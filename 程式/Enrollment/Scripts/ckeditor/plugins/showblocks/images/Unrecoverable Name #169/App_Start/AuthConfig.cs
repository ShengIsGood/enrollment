e including the properties. 
                C#: t => new { t.PropertyOne, t.PropertyTwo }
                VB.Net: Function(t) New With { t.PropertyOne, t.PropertyTwo }
            </param>
            <param name = "unique">A value indicating whether or not this is a unique index.</param>
            <param name = "anonymousArguments">
                Additional arguments that may be processed by providers. 
                Use anonymous type syntax to specify arguments e.g. 'new { SampleArgument = "MyValue" }'.
            </param>
            <returns>Itself, so that multiple calls can be chained.</returns>
        </member>
        <member name="M:System.Data.Entity.Migrations.Builders.TableBuilder`1.ForeignKey(System.String,System.Linq.Expressions.Expression{System.Func{`0,System.Object}},System.Boolean,System.String,System.Object)">
            <summary>
                Specifies a foreign key constraint to be created on the table.
            </summary>
