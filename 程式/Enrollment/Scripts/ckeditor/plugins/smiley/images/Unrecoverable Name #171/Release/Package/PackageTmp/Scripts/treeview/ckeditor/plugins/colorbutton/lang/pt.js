/param>
            <returns>An object that allows further configuration of the table creation operation.</returns>
        </member>
        <member name="M:System.Data.Entity.Migrations.DbMigration.AddForeignKey(System.String,System.String,System.String,System.String,System.Boolean,System.String,System.Object)">
            <summary>
                Adds an operation to create a new foreign key constraint.
            </summary>
            <param name = "dependentTable">
                The table that contains the foreign key column.
                Schema name is optional, if no schema is specified then dbo is assumed.
            </param>
            <param name = "dependentColumn">The foreign key column.</param>
            <param name = "principalTable">
                The table that contains the column this foreign key references.
                Schema name is op