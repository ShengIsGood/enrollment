   The context for EdmModel Validation
            </summary>
        </member>
        <member name="T:System.Data.Entity.Edm.Validation.Internal.EdmModel.EdmModelValidationVisitor">
            <summary>
                Visitor for EdmModel Validation
            </summary>
        </member>
        <member name="T:System.Data.Entity.Edm.Validation.Internal.EdmModel.EdmModelValidator">
            <summary>
                Edm Model Validator
            </summary>
        </member>
        <member name="M:System.Data.Entity.Edm.Validation.Internal.EdmModel.EdmModelValidator.Validate(System.Data.Entity.Edm.EdmModel,System.Data.Entity.Edm.Validation.Internal.EdmModel.EdmModelValidationContext)">
            <summary>
                validate the <see cref="T:System.Data.Entity.Edm.EdmModel"/> from the root with the context
            </summary>
            <param name="validateRoot"> The root to validate from </param>
            <param name="context"> The validation context </param>
        </member>
        <member name="T:System.Data.Entity.CreateDatabaseIfNotExists`1">
            <summary>
                An implementation of IDatabaseInitializer that will recreate and optionally re-seed the
                database only if the database does not exist.
                To seed the database, create a derived class and override the Seed method.
            </summary>
            <typeparam name = "TContext">The type of the context.</typeparam>
        </member>
        <member name="M:System.Data.Entity.CreateDatabaseIfNotExists`1.InitializeDatabase(`0)">
            <summary>
                Executes the strategy to initialize the database for the given context.
            </summary>
            <param name = "context">The context.</param>
        </member>
        <member name="M:System.Data.Entity.CreateDatabaseIfNotExists`1.Seed(`0)">
            <summary>
                A that should be overridden to actually add data to the context for seeding. 
                The default implementation does nothing.
            </summary>
            <param name = "context">The context to seed.</param>
        </member>
        <member name="T:System.Data.Entity.Database">
            <summary>
                An instances of this class is obtained from an <see cref="T:System.Data.Entity.DbContext"/> object and can be used
                to manage the actual database backing a DbContext or connection.
                This includes creating, deleting, and checking for the existence of a database.
                Note that deletion and checking for existence of a database can be performed using just a
                connection (i.e. without a full context) by using the static methods of this class.
            </summary>
        </member>
        <member name="M:System.Data.Entity.Database.#ctor(System.Data.Entity.Internal.InternalContext)">
            <summary>
                Creates a Database backed by the given context.  This object can be used to create a database,
                check for database existence, and delete a database.
            </summary>
            <param name = "context">The context that defines the database connection and model.</param>
        </member>
        <member name="M:System.Data.Entity.Database.SetInitializer``1(System.Data.Entity.IDatabaseInitializer{``0})">
            <summary>
                Gets or sets the database initialization strategy.  The database initialization strategy is called when <see cref="T:System.Data.Entity.DbContext"/> instance
                is initialized from a <see cref="T:System.Data.Entity.Infrastructure.DbCompiledModel"/>.  The strategy can optionally check for database existence, create a new database, and
                seed the database with data.
                The default strategy is an instance of <see cref="T:System.Data.Entity.CreateDatabaseIfNotExists`1"/>.
     