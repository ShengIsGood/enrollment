Schema.JsonSchemaException.#ctor(System.String,System.Exception)">
            <summary>
            Initializes a new instance of the <see cref="T:Newtonsoft.Json.Schema.JsonSchemaException"/> class
            with a specified error message and a reference to the inner exception that is the cause of this exception.
            </summary>
            <param name="message">The error message that explains the reason for the exception.</param>
            <param name="innerException">The exception that is the cause of the current exception, or a null reference (Nothing in Visual Basic) if no inner exception is specified.</param>
        </member>
        <member name="M:Newtonsoft.Json.Schema.JsonSchemaException.#ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)">
            <summary>
            Initializes a new instance of the <see cref="T:Newtonsoft.Json.Schema.JsonSchemaException"/> class.
            </summary>
            <param name="info">The <see cref="T:System.Runtime.Serialization.SerializationInfo"/> that holds the serialized object data about the exception being thrown.</param>
            <param name="context">The <see cref="T:System.Runtime.Serialization.StreamingContext"/> that contains contextual information about the source or destination.</param>
            <exception cref="T:System.ArgumentNullException">The <paramref name="info"/> parameter is null. </exception>
            <exception cref="T:System.Runtime.Serialization.SerializationException">The class name is null or <see cref="P:System.Exception.HResult"/> is zero (0). </exception>
        </member>
        <member name="T:Newtonsoft.Json.Schema.JsonSchemaGenerator">
            <summary>
            <para>
            Generates a <see cref="T:Newtonsoft.Json.Schema.JsonSchema"/> from a specified <see cref="T:System.Type"/>.
            </para>
            <note type="caution">
            JSON Schema validation has been moved to its own package. See <see href="http://www.newtonsoft.com/jsonschema">http://www.newtonsoft.com/jsonschema</see> for more details.
            </note>
            </summary>
        </member>
        <member name="P:Newtonsoft.Json.Schema.JsonSchemaGenerator.UndefinedSchemaIdHandling">
            <summary>
            Gets or sets how undefined schemas are handled by the serializer.
            </summary>
        </member>
        <member name="P:Newtonsoft.Json.Schema.JsonSchemaGenerator.ContractResolver">
            <summary>
            Gets or sets the contract resolver.
            </summary>
            <value>The contract resolver.</value>
        </member>
        <member name="M:Newtonsoft.Json.Schema.JsonSchemaGenerator.Generate(System.Type)">
            <summary>
            Generate a <see cref="T:Newtonsoft.Json.Schema.JsonSchema"/> from the specified type.
            </summary>
            <param name="type">The type to generate a <see cref="T:Newtonsoft.Json.Schema.JsonSchema"/> from.</param>
            <returns>A <see cref="T:Newtonsoft.Json.Schema.JsonSchema"/> generated from the specified type.</returns>
        </member>
        <member name="M:Newtonsoft.Json.Schema.JsonSchemaGenerator.Generate(System.Type,Newtonsoft.Json.Schema.JsonSchemaResolver)">
            <summary>
            Generate a <see cref="T:Newtonsoft.Json.Schema.JsonSchema"/> from the specified type.
            </summary>
            <param name="type">The type to generate a <see cref="T:Newtonsoft.Json.Schema.JsonSchema"/> from.</param>
            <param name="resolver">The <see cref="T:Newtonsoft.Json.Schema.JsonSchemaResolver"/> used to resolve schema references.</param>
            <returns>A <see cref="T:Newtonsoft.Json.Schema.JsonSchema"/> generated from the specified type.</returns>
        </member>
        <member name="M:Newtonsoft.Json.Schema.JsonSchemaGenerator.Generate(System.Type,System.Boolean)">
            <summary>
            Generate a <see cref="T:Newtonsoft.Json.Schema.JsonSchema"/> from the specified type.
            </summary>
            <param name="type">The type to generate a <see cref="T:Newtonsoft.Json.Schema.JsonSchema"/> from.</param>
            <param name="rootSchemaNullable">Specify whether the generated root <see cref="T:Newtonsoft.Json.Schema.JsonSchema"/> will be nullable.</param>
            <returns>A <see cref="T:Newtonsoft.Json.Schema.JsonSchema"/> generated from the specified type.</returns>
        