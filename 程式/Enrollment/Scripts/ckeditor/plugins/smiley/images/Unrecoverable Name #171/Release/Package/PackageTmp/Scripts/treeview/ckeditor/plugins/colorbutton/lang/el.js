ns to be converted.</param>
            <param name = "providerManifestToken">Token representing the version of SQL Server being targeted (i.e. "2005", "2008").</param>
            <returns>A list of SQL statements to be executed to perform the migration operations.</returns>
        </member>
        <member name="M:System.Data.Entity.Migrations.Sql.SqlServerMigrationSqlGenerator.CreateConnection">
            <summary>
                Creates an empty connection for the current provider.
                Allows derived providers to use connection other than <see cref="T:System.Data.SqlClient.SqlConnection"/>.
            </summary>
            <returns></returns>
        </member>
        <member name="M:System.Data.Entity.Migrations.Sql.SqlServerMigrationSqlGenerator.Generate(System.Data.Entity.Migrations.Model.CreateTableOperation)">
            <summary>
                Generates SQL for a <see cref="T:System.Data.Entity.Migrations.Model.CreateTableOperation"/>.
                Generated SQL should be added using the Statement method.
            </summary>
            <param name="createTableOperation">The operation to produce SQL for.</param>
        </member>
        <member name="M:System.Data.Entity.Migrations.Sql.SqlServerMigrationSqlGenera