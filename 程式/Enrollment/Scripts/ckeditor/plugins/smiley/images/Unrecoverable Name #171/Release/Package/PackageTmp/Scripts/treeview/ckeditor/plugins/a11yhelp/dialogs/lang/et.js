 configure the complex type. This method can be called multiple times for the same type to
                perform multiple lines of configuration.
            </summary>
            <typeparam name = "TComplexType">The type to be registered or configured.</typeparam>
            <returns>The configuration object for the specified complex type.</returns>
        </member>
        <member name="M:System.Data.Entity.DbModelBuilder.Build(System.Data.Common.DbConnection)">
            <summary>
                Creates a <see cref="T:System.Data.Entity.Infrastructure.DbModel"/> based on the configuration performed using this builder.
                The connection is used to determine the database provider being used as this
                affects the database layer of the generated model.
            </summary>
            <param name="providerConnection">Connection to use to determine provider information.</param>
            <returns>The model that was built.</returns>
        </member>
        <member name="M:System.Data.Entity.DbModelBuilder.Build(System.Data.Entity.Infrastructure.DbProviderInfo)">
            <summary>
                Creates a <see cref="T:System.Data.Entity.Infrastructure.DbModel"/> based on the configuration performed using this builder.
                Provider information must be specified because this affects the database layer of the generated model.
                For SqlClient the invariant name is 'System.Data.SqlClient' and the manifest token is the version year (i.e. '2005', '2008' etc.)
            </summary>
            <param name="providerInfo">The database provider that the model will be used with.</param>
            <returns>The model that was built.</returns>
        </member>
        <member name="P:System.Data.Entity.DbModelBuilder.Conventions">
            <summary>
                Provides access to the settings of this DbModelBuilder that deal with conventions.
            </summary>
        </member>
        <member name="P:System.Data.Entity.DbModelBuilder.Configurations">
            <summary>
                Gets the <see cref="T:System.Data.Entity.ModelConfiguration.Configuration.ConfigurationRegistrar"/> for this DbModelBuilder. 
                The registrar allows derived entity and complex type configurations to be registered with this builder.
            </summary>
        </member>
        <member name="T:System.Data.Entity.DbModelBuilderVersion">
            <summary>
                A value from this enumeration can be provided directly to the <see cref="T:System.Data.Entity.DbModelBuilder"/>
                class or can be used in the <see cref="T:System.Data.Entity.DbModelBuilderVersionAttribute"/> applied to
                a class derived from <see cref="T:System.Data.Entity.DbContext"/>. The value used defines which version of
                the DbContext and DbModelBuilder conventions should be used when building a model from
                code--also know as "Code First".
            </summary>
            <remarks>
                Using DbModelBuilderVersion.Latest ensures that all the latest functionality is available
                when upgrading to a new release of the Entity Framework. However, it may result in an
                application behaving differently with the new release than it did with a previous release.
                This can be avoided by using a specific version of the conventions, but if a version
                other than the latest is set then not all the latest functionality will be available.
            </remarks>
        </member>
        <member name="F:System.Data.Entity.DbModelBuilderVersion.Latest">
            <summary>
                Indicates that the latest version of the <see cref="T:System.Data.Entity.DbModelBuilder"/> and 
                <see cref="T:System.Data.Entity.DbContext"/> conventions should be used.
            </summary>
       