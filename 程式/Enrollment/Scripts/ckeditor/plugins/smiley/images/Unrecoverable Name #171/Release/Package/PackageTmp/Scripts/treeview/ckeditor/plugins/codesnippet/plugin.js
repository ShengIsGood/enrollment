ValidationItemFormat(System.Object,System.Object,System.Object)">
            <summary>
            A string like "\t{0}: {1}: {2}"
            </summary>
        </member>
        <member name="M:System.Data.Entity.Resources.Strings.KeyRegisteredOnDerivedType(System.Object,System.Object)">
            <summary>
            A string like "A key is registered for the derived type '{0}'. Keys can only be registered for the root type '{1}'."
            </summary>
        </member>
        <member name="M:System.Data.Entity.Resources.Strings.DuplicateEntryInUserDictionary(System.Object,System.Object)">
            <summary>
            A string like "The {0} value '{1}' already exists in the user-defined dictionary."
            </summary>
        </member>
        <member name="M:System.Data.Entity.Resources.Strings.InvalidTableMapping(System.Object,System.Object)">
            <summary>
            A string like "The type '{0}' has already been mapped to table '{1}'. Specify all mapping aspects of a table in a single Map call."
            </summary>
        </member>
        <member name="M:System.Data.Entity.Resources.Strings.InvalidTableMapping_NoTableName(System.Object)">
            <summary>
            A string like "Map was called more than once for type '{0}' and at least one of the calls didn't specify the target table name."
            </summary>
        </member>
        <member name="M:System.Data.Entity.Resources.Strings.InvalidChainedMappingSyntax(System.Object)">
            <summary>
            A string like "The derived type '{0}' has already been mapped using the chaining syntax. A derived type can only be mapped once using the chaining syntax."
            </summary>
        </member>
        <member name="M:System.Data.Entity.Resources.Strings.InvalidNotNullCondition(System.Object,System.Object)">
            <summary>
            A string like "An "is not null" condition cannot be specified on property '{0}' on type '{1}' because this property is not included in the model. Check that the property has not been explicitly excluded from the model by using the Ignore method or NotMappedAttribute data annotation."
            </summary>
        </member>
        <member name="M:System.Data.Entity.Resources.Strings.InvalidDiscriminatorType(System.Object)">
            <summary>
            A string like "Values of type '{0}' cannot be used as type discriminator values. Supported types include byte, signed byte, bool, int16, int32, int64, and string."
            </summary>
        </member>
        <member name="M:System.Data.Entity.Resources.Strings.ConventionNotFound(System.Object,System.Object)">
            <summary>
            A string like "Unable to add the convention '{0}'. Could not find an existing convention of type '{1}' in the current convention set."
            </summary>
        </member>
        <member name="M:System.Data.Entity.Resources.Strings.InvalidEntitySplittingProperties(System.Object)">
            <summary>
            A string like "Not all properties for type '{0}' have been mapped. Either map those properties or explicitly excluded them from the model."
            </summary>
        </member>
        <member name="M:System.Data.Entity.Resources.Strings.ModelBuilder_ProviderNameNotFound(System.Object)">
            <summary>
            A string like "Unable to determine the provider name for connection of type '{0}'."
            </summary>
        </member>
        <member name="M:System.Data.Entity.Resources.Strings.ToTable_InvalidSchemaName(System.Object)">
            <summary>
            A string like "The qualified table name '{0}' contains an invalid schema name. Schema names must have a non-zero length."
            </summary>
        </m