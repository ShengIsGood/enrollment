s.ReadAs(System.Net.Http.Formatting.FormDataCollection,System.Type,System.String,System.Net.Http.Formatting.IRequiredMemberSelector,System.Net.Http.Formatting.IFormatterLogger)">
      <summary>Reads the collection extensions with specified type and model name.</summary>
      <returns>The collection extensions.</returns>
      <param name="formData">The form data.</param>
      <param name="type">The type of the object.</param>
      <param name="modelName">The name of the model.</param>
      <param name="requiredMemberSelector">The required member selector.</param>
      <param name="formatterLogger">The formatter logger.</param>
    </member>
    <member name="T:System.Web.Http.ModelBinding.HttpBindingBehavior">
      <summary>Enumerates the behavior of the HTTP binding.</summary>
    </member>
    <member name="F:System.Web.Http.ModelBinding.HttpBindingBehavior.Optional">
      <summary>The optional binding behavior</summary>
    </member>
    <member name="F:System.Web.Http.ModelBinding.HttpBindingBehavior.Never">
      <summary>Never use HTTP binding.</summary>
    </member>
    <member name="F:System.Web.Http.ModelBinding.HttpBindingBehavior.Required">
      <summary>HTTP binding is required.</summary>
    </member>
    <member name="T:System.Web.Http.ModelBinding.HttpBindingBehaviorAttribute">
      <summary>Provides a base class for model-binding behavior attributes.</summary>
    </member>
    <member name="M:System.Web.Http.ModelBinding.HttpBindingBehaviorAttribute.#ctor(System.Web.Http.ModelBinding.HttpBindingBehavior)">
      <summary>Initializes a new instance of the <see cref="T:System.Web.Http.ModelBinding.HttpBindingBehaviorAttribute" /> class.</summary>
      <param name="behavior">The behavior.</param>
    </member>
    <member name="P:System.Web.Http.ModelBinding.HttpBindingBehaviorAttribute.Behavior">
      <summary>Gets or sets the behavior category.</summary>
      <returns>The behavior category.</returns>
    </member>
    <member name="P:System.Web.Http.ModelBinding.HttpBindingBehaviorAttribute.TypeId">
      <summary>Gets the unique identifier for this attribute.</summary>
      <returns>The id for this attribute.</returns>
    </member>
    <member name="T:System.Web.Http.ModelBinding.HttpRequestParameterBinding">
      <summary>Parameter binds to the request.</summary>
    </member>
    <member name="M:System.Web.Http.ModelBinding.HttpRequestParameterBinding.#ctor(System.Web.Http.Controllers.HttpParameterDescriptor)">
      <summary>Initializes a new instance of the <see cref="T:System.Web.Http.ModelBinding.HttpRequestParameterBinding" /> class.</summary>
      <param name="descriptor">The parameter descriptor.</param>
    </member>
    <member name="M:System.Web.Http.ModelBinding.HttpRequestParameterBinding.ExecuteBindingAsync(System.Web.Http.Metadata.ModelMetadataProvider,System.Web.Http.Controllers.HttpActionContext,System.Threading.CancellationToken)">
      <summary>Asynchronously executes parameter binding.</summary>
      <returns>The binded parameter.</returns>
      <param name="metadataProvider">The metadata provider.</param>
      <param name="actionContext">The action context.</param>
      <param name="cancellationToken">The cancellation token.</param>
    </member>
    <member name="T:System.Web.Http.ModelBinding.IModelBinder">
      <summary>Defines the methods that are required for a model binder.</summary>
    </member>
    <member name="M:System.Web.Http.ModelBinding.IModelBinder.BindModel(System.Web.Http.Controllers.HttpActionContext,System.Web.Http.ModelBinding.ModelBindingContext)">
      <summary>Binds the model to a value by using the specified controller context and binding context.</summary>
      <returns>The bound value.</returns>
      <param name="actionContext">The action context.</param>
      <param name="bindingContext">The binding context.</param>
    </member>
    <member name="T:System.Web.Http.ModelBinding.IValueProviderParameterBinding">
      <summary>Represents a value provider for parameter binding.</summary>
    </member>
    <member name="P:System.Web.Http.ModelBinding.IValueProviderParameterBinding.ValueProviderFactories">
      <summary>Gets the <see cref="T:System.Web.Http.ValueProviders.ValueProviderFactory" /> instances used by this parameter binding.</summary>
      <returns>The <see cref="T:System.Web.Http.ValueProviders.ValueProviderFactory" /> instances used by this parameter binding.</returns>
    </member>
    <member name="T:System.Web.Http.ModelBinding.JQueryMvcFormUrlEncodedFormatter">
      <summary>Represents the <see cref="T:System.Net.Http.Formatting.MediaTypeFormatter" /> class for handling HTML form URL-ended data, also known as appl