ty Data Model (EDM) item. See <see cref="P:System.Data.Entity.Edm.EdmProperty.PropertyType"/> for examples.
            </summary>
        </member>
        <member name="P:System.Data.Entity.Edm.EdmTypeReference.CollectionRank">
            <summary>
                Gets or sets a value indicating the collection rank of the type reference. A collection rank greater than zero indicates that the type reference represents a collection of its referenced <see cref="P:System.Data.Entity.Edm.EdmTypeReference.EdmType"/> .
            </summary>
        </member>
        <member name="P:System.Data.Entity.Edm.EdmTypeReference.EdmType">
            <summary>
                Gets or sets a value indicating the <see cref="T:System.Data.Entity.Edm.EdmDataModelType"/> referenced by this type reference.
            </summary>
        </member>
        <member name="P:System.Data.Entity.Edm.EdmTypeReference.IsNullable">
            <summary>
                Gets or sets an optional value indicating whether the referenced type should be considered nullable.
            </summary>
        </member>
        <member name="P:System.Data.Entity.Edm.EdmTypeReference.PrimitiveTypeFacets">
            <summary>
                Gets or sets an optional <see cref="T:System.Data.Entity.Edm.EdmPrimitiveTypeFacets"/> instance that applies additional constraints to a referenced primitive type.
            </summary>
            <remarks>
                Accessing this property forces the creation of an EdmPrimitiveTypeFacets value if no value has previously been set. Use <see cref="P:System.Data.Entity.Edm.EdmTypeReference.HasFacets"/> to determine whether or not this property currently has a value.
            </remarks>
        </member>
        <member name="P:System.Data.Entity.Edm.EdmTypeReference.HasFacets">
            <summary>
                Gets a value indicating whether the <see cref="P:System.Data.Entity.Edm.EdmTypeReference.PrimitiveTypeFacets"/> property of this type reference has been assigned an <see cref="T:System.Data.Entity.Edm.EdmPrimitiveTypeFacets"/> value with at least one facet value specified.
            </summary>
        </member>
        <member name="P:System.Data.Entity.Edm.EdmTypeReference.IsCollectionType">
            <summary>
                Indicates whether this type reference represents a collection of its referenced <see cref="P:System.Data.Entity.Edm.EdmTypeReference.EdmType"/> (when <see cref="P:System.Data.Entity.Edm.EdmTypeReference.CollectionRank"/> is greater than zero) or not.
            </summary>
        </member>
        <member name="P:System.Data.Entity.Edm.EdmTypeReference.IsComplexType">
            <summary>
                Indicates whether the <see cref="P:System.Data.Entity.Edm.EdmTypeReference.EdmType"/> property of this type reference currently refers to an <see cref="T:System.Data.Entity.Edm.EdmComplexType"/> , is not a collection type, and does not have primitive facet values specified.
            </summary>
        </member>
        <member name="P:System.Data.Entity.Edm.EdmTypeReference.ComplexType">
            <summary>
                Gets the <see cref="T:System.Data.Entity.Edm.EdmComplexType"/> currently referred to by this type reference, or <code>null</code> if the type reference is a collection type or does not refer to a complex type.
            </summary>
        </member>
        <member name="P:System.Data.Entity.Edm.EdmTypeReference.IsPrimitiveType">
            <summary>
                Indicates whether the <see cref="P:System.Data.Entity.