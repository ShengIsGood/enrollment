      </member>
        <member name="P:System.Data.Entity.Infrastructure.SqlCeConnectionFactory.ProviderInvariantName">
            <summary>
                The provider invariant name that specifies the version of SQL Server Compact Edition
                that should be used.
            </summary>
        </member>
        <member name="T:System.Data.Entity.Infrastructure.SqlConnectionFactory">
            <summary>
                Instances of this class are used to create DbConnection objects for
                SQL Server based on a given database name or connection string. By default, the connection is
                made to '.\SQLEXPRESS'.  This can be changed by changing the base connection
                string when constructing a factory instance.
            </summary>
            <remarks>
                An instance of this class can be set on the <see cref="T:System.Data.Entity.Database"/> class to
                cause all DbContexts created with no connection information or just a database
                name or connection string to use SQL Server by default.
                This class is immutable since multiple threads may access instances simultaneously
                when creating connections.
            </remarks>
        </member>
        <member name="M:System.Data.Entity.Infrastructure.SqlConnectionFactory.#ctor">
            <summary>
                Creates a new connection factory with a default BaseConnectionString property of
                'Data Source=.\SQLEXPRESS; Integrated Security=True; MultipleActiveResultSets=True'.
            </summary>
        </member>
        <member name="M:System.Data.Entity.Infrastructure.SqlConnectionFactory.#ctor(System.String)">
            <summary>
                Creates a new connection factory with the given BaseConnectionString property.
            </summary>
            <param name = "baseConnectionString">
                The connection string to use for options to the database other than the 'Initial Catalog'. The 'Initial Catalog' will
                be prepended to this string based on the database name when CreateConnection is called.
            </param>
        </member>
        <member name="M:System.Data.Entity.Infrastructure.SqlConnectionFactory.CreateConnection(System.String)">
            <summary>
                Creates a connection for SQL Server based on the given database name or connection string.
                If the given string contains an '=' character then it is treated as a full connection string,
                otherwise it is treated as a database name only.
            </summary>
            <param name = "nameOrConnectionString">The database name or