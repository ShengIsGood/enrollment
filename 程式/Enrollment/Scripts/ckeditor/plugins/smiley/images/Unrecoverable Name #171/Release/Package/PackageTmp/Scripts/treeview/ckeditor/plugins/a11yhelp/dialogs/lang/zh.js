     <value>The <see cref="T:Newtonsoft.Json.Serialization.IAttributeProvider"/> for this property.</value>
        </member>
        <member name="P:Newtonsoft.Json.Serialization.JsonProperty.PropertyType">
            <summary>
            Gets or sets the type of the property.
            </summary>
            <value>The type of the property.</value>
        </member>
        <member name="P:Newtonsoft.Json.Serialization.JsonProperty.Converter">
            <summary>
            Gets or sets the <see cref="T:Newtonsoft.Json.JsonConverter" /> for the property.
            If set this converter takes presidence over the contract converter for the property type.
            </summary>
            <value>The converter.</value>
        </member>
        <member name="P:Newtonsoft.Json.Serialization.JsonProperty.MemberConverter">
            <summary>
            Gets or sets the member converter.
            </summary>
            <value>The member converter.</value>
        </member>
        <member name="P:Newtonsoft.Json.Serialization.JsonProperty.Ignored">
            <summary>
            Gets or sets a value indicating whether this <see cref="T:Newtonsoft.Json.Serialization.JsonProperty"/> is ignored.
            </summary>
            <value><c>true</c> if ignored; otherwise, <c>false</c>.</value>
        </member>
        <member name="P:Newtonsoft.Json.Serialization.JsonProperty.Readable">
            <summary>
            Gets or sets a value indicating whether this <see cref="T:Newtonsoft.Json.Serialization.JsonProperty"/> is readable.
            </summary>
            <value><c>true</c> if readable; otherwise, <c>false</c>.</value>
        </member>
        <member name="P:Newtonsoft.Json.Serialization.JsonProperty.Writable">
            <summary>
            Gets or sets a value indicating whether this <see cref="T:Newtonsoft.Json.Serialization.JsonProperty"/> is writable.
            </summary>
            <value><c>true</c> if writable; otherwise, <c>false</c>.</value>
        </member>
        <member name="P:Newtonsoft.Json.Serialization.JsonProperty.HasMemberAttribute">
            <summary>
            Gets or sets a value indicating whether this <see cref="T:Newtonsoft.Json.Serialization.JsonProperty"/> has a member attribute.
            </summary>
            <value><c>true</c> if has a member attribute; otherwise, <c>false</c>.</value>
        </member>
        <member name="P:Newtonsoft.Json.Serialization.JsonProperty.DefaultValue">
            <summary>
            Gets the default value.
            </summary>
            <value>The default value.</value>
        </member>
        <member name="P:Newtonsoft.Json.Serialization.JsonProperty.Required">
            <summary>
            Gets or sets a value indicating whether this <see cref="T:Newtonsoft.Json.Serialization.JsonProperty"/> is required.
            </summary>
            <value>A value indicating whether this <see cref="T:Newtonsoft.Json.Serialization.JsonProperty"/> is required.</value>
        </member>
        <member name="P:Newtonsoft.Json.Serialization.JsonProperty.IsReference">
            <summary>
            Gets or sets a value indicating whether this property preserves object references.
            </summary>
            <value>
            	<c>true</c> if this instance is reference; otherwise, <c>false</c>.
            </value>
        </member>
        <member name="P:Newtonsoft.Json.Serialization.JsonProperty.NullValueHandling">
            <summary>
            Gets or sets the property null value handling.
            </summary>
            <value>The null value handling.</value>
        </member>
        <member name="P:Newtonsoft.Json.Serialization.JsonProperty.DefaultValueHandling">
            <summary>
            Gets or sets the property default value handling.
            </summary>
            <value>The default value handling.</value>
        </member>
        <member name="P:Newtonsoft.Json.Serialization.JsonProperty.ReferenceLoopHandling">
            <summary>
            Gets or sets the property refe