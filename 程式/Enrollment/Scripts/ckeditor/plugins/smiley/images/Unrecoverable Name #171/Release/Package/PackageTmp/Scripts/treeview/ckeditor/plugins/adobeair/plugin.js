   </member>
        <member name="F:System.Data.Entity.Edm.Parsing.Xml.Internal.XmlErrorCode.EdmModel_NameMustNotBeEmptyOrWhiteSpace">
            <summary>
                The name of NamedEdmItem must not be empty or white space only
            </summary>
        </member>
        <member name="F:System.Data.Entity.Edm.Parsing.Xml.Internal.XmlErrorCode.EdmAssociationType_AssocationEndMustNotBeNull">
            <summary>
                EdmTypeReference is empty
            </summary>
            Unused 199;
        </member>
        <member name="T:System.Data.Entity.Edm.Serialization.CsdlSerializer">
            <summary>
                Serializes an <see cref="T:System.Data.Entity.Edm.EdmModel"/> that conforms to the restrictions of a single CSDL schema file to an XML writer. The model to be serialized must contain a single <see cref="T:System.Data.Entity.Edm.EdmNamespace"/> and a single <see cref="T:System.Data.Entity.Edm.EdmEntityContainer"/> .
            </summary>
        </member>
        <member name="M:System.Data.Entity.Edm.Serialization.CsdlSerializer.Serialize(System.Data.Entity.Edm.EdmModel,System.Xml.XmlWriter)">
            <summary>
                Serialize the <see cref="T:System.Data.Entity.Edm.EdmModel"/> to the XmlWriter.
            </summary>
            <param name="model"> The EdmModel to serialize, mut have only one <see cref="T:System.Data.Entity.Edm.EdmNamespace"/> and one <see cref="T:System.Data.Entity.Edm.EdmEntityContainer"/> </param>
            <param name="xmlWriter"> The XmlWriter to serialize to </param>
        </member>
        <member name="M:System.Data.Entity.Edm.Serialization.MslSerializer.Serialize(System.Data.Entity.Edm.Db.Mapping.DbDatabaseMapping,System.Xml.XmlWriter)">
            <summary>
                Serialize the <see cref="T:System.Data.Entity.Infrastructure.DbModel"/> to the XmlWriter
            </summary>
            <param name="databaseMapping"> The DbModel to serialize </param>
            <param name="xmlWriter"> The XmlWriter to serialize to </param>
        </member>
        <member name="M:System.Data.Entity.Edm.Serialization.SsdlSerializer.Serialize(System.Data.Entity.Edm.Db.DbDatabaseMetadata,System.String,System.String,System.Xml.XmlWriter)">
            <summary>
                Serialize the <see cref="T:System.Data.Entity.Edm.Db.DbDatabaseMetadata"/> to the <see cref="T:System.Xml.XmlWriter"/>
            </summary>
            <p