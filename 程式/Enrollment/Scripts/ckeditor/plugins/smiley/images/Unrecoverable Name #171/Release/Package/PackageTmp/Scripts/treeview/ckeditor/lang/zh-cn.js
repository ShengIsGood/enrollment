ntrollers.HttpActionContext" />. </summary>
      <param name="adapterType">The type of the adapter.</param>
    </member>
    <member name="M:System.Web.Http.Validation.Providers.DataAnnotationsModelValidatorProvider.RegisterDefaultValidatableObjectAdapterFactory(System.Web.Http.Validation.Providers.DataAnnotationsValidatableObjectAdapterFactory)">
      <summary>Registers the default adapter factory for objects which implement <see cref="T:System.ComponentModel.DataAnnotations.IValidatableObject" />. </summary>
      <param name="factory">The factory.</param>
    </member>
    <member name="M:System.Web.Http.Validation.Providers.DataAnnotationsModelValidatorProvider.RegisterValidatableObjectAdapter(System.Type,System.Type)">
      <summary>Registers an adapter type for the given modelType, which must implement <see cref="T:System.ComponentModel.DataAnnotations.IValidatableObject" />. The adapter type must derive from <see cref="T:System.Web.Http.Validation.ModelValidator" /> and it must contain a public constructor which takes two parameters of types <see cref="T:System.Web.Http.Metadata.ModelMetadata" /> and <see cref="T:System.Web.Http.Controllers.HttpActionContext" />. </summary>
      <param name="modelType">The model type.</param>
      <param name="adapterType">The type of the adapter.</param>
    </member>
    <member name="M:System.Web.Http.Validation.Providers.DataAnnotationsModelValidatorProvider.RegisterValidatableObjectAdapterFactory(System.Type,System.Web.Http.Validation.Providers.DataAnnotationsValidatableObjectAdapterFactory)">
      <summary>Registers an adapter factory for the given modelType, which must implement <see cref="T:System.ComponentModel.DataAnnotations.IValidatableObject" />. </summary>
      <param name="modelType">The model type.</param>
      <param name="factory">The factory.</param>
    </member>
    <member name="T:System.Web.Http.Validation.Providers.DataAnnotationsValidatableObjectAdapterFactory">
      <summary>Provides a factory for validators that are based on <see cref="T:System.ComponentModel.DataAnnotations.IValidatableObject" />.</summary>
    </member>
    <member name="T:System.Web.Http.Validation.Providers.DataMemberModelValidatorProvider">
      <summary>Represents a validator provider for data member model.</summary>
    </member>
    <member name="M:System.Web.Http.Validation.Providers.DataMemberModelValidatorProvider.#ctor">
      <summary>Initializes a new instance of the <see cref="T:System.Web.Http.Validation.Providers.DataMemberModelValidatorProvider" /> class.</summary>
    </member>
    <member name="M:System.Web.Http.Validation.Providers.DataMemberModelValidatorProvider.GetValidators(System.Web.Http.Metadata.ModelMetadata,System.Collections.Generic.IEnumerable{System.Web.Http.Validation.ModelValidatorProvider},System.Collections.Generic.IEnumerable{System.Attribute})">
      <summary>Gets the validators for the model.</summary>
      <returns>The validators for the model.</returns>
      <param name="metadata">The metadata.</param>
      <param name="validatorProviders">An enumerator of validator providers.</param>
      <param name="attributes">A list of attributes.</param>
    </member>
    <member name="T:System.Web.Http.Validation.Providers.InvalidModelValidatorProvider">
      <summary>An implementation of <see cref="T:System.Web.Http.Validation.ModelValidatorProvider" /> which provides validators that throw exceptions when the model is invalid.</summary>
    </member>
    <member name="M:System.Web.Http.Validation.Providers.InvalidModelValidatorProvider.#ctor">
      <summary>Initializes a new instance of the <see cref="T:System.Web.Http.Validation.Providers.InvalidModelValidatorProvider" /> class.</summary>
    </member>
    <member name="M:System.Web.Http.Validation.Providers.InvalidModelValidatorProvider.GetValidators(System.Web.Http.Metadata.ModelMetadata,System.Collections.Generic.IEnumerable{System.Web.Http.Validation.ModelValidatorProvider},System.Collections.Generic.IEnumerable{System.Attribute})">
      <summary>Gets a list of validators associated with this <see cref="T:System.Web.Http.Validation.Providers.InvalidModelValidatorProvider" />.</summary>
      <returns>The list of validators.</returns>
      <param name="metadata">The metadata.</param>
      <param name="validatorProviders">The validator providers.</param>
      <param name="attributes">The list of attributes.</param>
    </member>
    <member name="T:System.Web.Http.Validation.Providers.RequiredMemberModelValidatorProvider">
      <summary>Represents the provider for the required member model validator.</summary>
    </member>
    <member name="M:System.Web.Http.Validation.Providers.RequiredMemberModelValidatorProvider.#ctor(System.Net.Http.Formatting.IRequiredMemberSelector)">
      <summary>Initializes a new instance of the <see cref="T:System.Web.Http.Validation.Providers.RequiredMemberModelValidatorProvider" /> class.</summary>
      <param name="requiredMemberSelector">The required member selector.</param>
    </member>
    <member name="M:System.Web.Http.Validation.Providers.RequiredMemberModelValidatorProvider.GetValidators(System.Web.Http.Metadata.ModelMetadata,System.Collections.Generic.IEnumerable{System.Web.Http.Validation.ModelValidatorProvider})">
      <summary>Gets the validator for the member model.</summary>
      <returns>The validator for the member model.</returns>
      <param name="metadata">The metadata.</param>
      <param name="validatorProviders">The validator providers</param>
    </member>
    <member name="T:System.Web.Http.Validation.Validators.DataAnnotationsModelValidator">
      <summary>Provides a model validator.</summary>
    </member>
    <member name="M:System.Web.Http.Validation.Validators.DataAnnotationsModelValidator.#ctor(System.Collections.Generic.IEnumerable{System.Web.Http.Validation.ModelValidatorProvider},System.ComponentModel.DataAnnotations.ValidationAttribute)">
      <summary>Initializes a new instance of the <see cref="T:System.Web.Http.Validation.Validators.DataAnnotationsModelValidator" /> class.</summary>
      <param name="validatorProviders">The validator providers.</param>
      <param name="attribute">The validation attribute for the model.</param>
    </member>
    <member name="P:System.Web.Http.Validation.Validators.DataAnnotationsModelValidator.Attribute">
      <summary>Gets or sets the validation attribute for the model validator.</summary>
      <returns>The validation attribute for the model validator.</returns>
    </member>
    <member name="P:System.Web.Http.Validation.Validators.DataAnnotationsModelValidator.IsRequired">
      <summary>Gets a value that indicates whether model validation is required.</summary>
      <returns>true if model validation is required; otherwise, false.</returns>
    </member>
    <member name="M:System.Web.Http.Validation.Validators.DataAnnotationsModelValidator.Validate(System.Web.Http.Metadata.ModelMetadata,System.Object)">
      <summary>Validates the model and returns the validation errors if any.</summary>
      <returns>A list of validation error messages for the model, or an empty list if no errors have occurred.</returns>
      <param name="metadata">The model metadata.</param>
      <param name="container">The container for the model.</param>
    </member>
    <member name="T:System.Web.Http.Validation.Validators.ErrorModelValidator">
      <summary>A <see cref="T:System.Web.Http.Validation.ModelValidator" /> to represent an error. This validator will always throw an exception regardless of the actual model value.</summary>
    </member>
    <member name="M:System.Web.Http.Validation.Validators.ErrorModelValidator.#ctor(System.Collections.Generic.IEnumerable{System.Web.Http.Validation.ModelValidatorProvider},System.String)">
      <summary>Initializes a new instance of the <see cref="T:System.Web.Http.Validation.Validators.ErrorModelValidator" /> class.</summary>
      <param name="validatorProviders">The list of  model validator providers.</param>
      <param name="errorMessage">The error message for the exception.</param>
    </member>
    <member name="M:System.Web.Http.Validation.Validators.ErrorModelValidator.Validate(System.Web.Http.Metadata.ModelMetadata,System.Object)">
      <summary>Validates a specified object.</summary>
      <returns>A list of validation results.</returns>
      <param name="metadata">The metadata.</param>
      <param name="container">The container.</param>
    </member>
    <member name="T:System.Web.Http.Validation.Validators.RequiredMemberModelValidator">
      <summary>Represents the <see cref="T:System.Web.Http.Validation.ModelValidator" /> for required members. </summary>
    </member>
    <member name="M:System.Web.Http.Validation.Validators.RequiredMemberModelValidator.#ctor(System.Collections.Generic.IEnumerable{System.Web.Http.Validation.ModelValidatorProvider})">
      <summary>Initializes a new instance of the <see cref="T:System.Web.Http.Validation.Validators.RequiredMemberModelValidator" /> class.</summary>
      <param name="validatorProviders">The validator providers.</param>
    </member>
    <member name="P:System.Web.Http.Validation.Validators.RequiredMemberModelValidator.IsRequired">
      <summary>Gets or sets a value that instructs the serialization engine that the member must be presents when validating.</summary>
      <returns>true if the member is required; otherwise, false.</returns>
    </member>
    <member name="M:System.Web.Http.Validation.Validators.RequiredMemberModelValidator.Validate(System.Web.Http.Metadata.ModelMetadata,System.Object)">
      <summary>Validates the object.</summary>
      <returns>A list of validation results.</returns>
      <param name="metadata">The metadata.</param>
      <param name="container">The container.</param>
    </member>
    <member name="T:System.Web.Http.Validation.Validators.ValidatableObjectAdapter">
      <summary>Provides an object adapter that can be validated.</summary>
    </member>
    <member name="M:System.Web.Http.Validation.Validators.ValidatableObjectAdapter.#ctor(System.Collections.Generic.IEnumerable{System.Web.Http.Validation.ModelValidatorProvider})">
      <summary>Initializes a new instance of the <see cref="T:System.Web.Http.Validation.Validators.ValidatableObjectAdapter" /> class.</summary>
      <param name="validatorProviders">The validation provider.</param>
    </member>
    <member name="M:System.Web.Http.Validation.Validators.ValidatableObjectAdapter.Validate(System.Web.Http.Metadata.ModelMetadata,System.Object)">
     