ds an operation to drop an existing primary key that was created with the default name.
            </summary>
            <param name = "table">
                The table that contains the primary key column.
                Schema name is optional, if no schema is specified then dbo is assumed.
            </param>
            <param name = "anonymousArguments">
                Additional arguments that may be processed by providers. 
                Use anonymous type syntax to specify arguments e.g. 'new { SampleArgument = "MyValue" }'.
            </param>
        </member>
        <member name="M:System.Data.Entity.Migrations.DbMigration.CreateIndex(System.String,System.String,System.Boolean,System.String,System.Object)">
            <summary>
                Adds an operation to create an index on a single column.
            </summary>
            <param name = "table">
        