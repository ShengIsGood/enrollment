sonSerializer)">
            <summary>
            Reads the JSON representation of the object.
            </summary>
            <param name="reader">The <see cref="T:Newtonsoft.Json.JsonReader"/> to read from.</param>
            <param name="objectType">Type of the object.</param>
            <param name="existingValue">The existing value of object being read.</param>
            <param name="serializer">The calling serializer.</param>
            <returns>The object value.</returns>
        </member>
        <member name="M:Newtonsoft.Json.Converters.DiscriminatedUnionConverter.CanConvert(System.Type)">
            <summary>
            Determines whether this instance can convert the specified object type.
            </summary>
            <param name="objectType">Type of the object.</param>
            <returns>
            	<c>true</c> if this instance can convert the specified object type; otherwise, <c>false</c>.
            </returns>
        </member>
        <member name="T:Newtonsoft.Json.Converters.EntityKeyMemberConverter">
            <summary>
            Converts an Entity Framework EntityKey to and from JSON.
            </summary>
        </member>
        <member name="M:Newtonsoft.Json.Converters.EntityKeyMemberConverter.WriteJson(Newtonsoft.Json.JsonWriter,System.Object,Newtonsoft.Json.JsonSerializer)">
            <summary>
            Writes the JSON representation of the object.
            </summary>
            <param name="writer">The <see cref="T:Newtonsoft.Json.JsonWriter"/> to write to.</param>
            <param name="value">The value.</param>
            <param name="serializer">The calling serializer.</param>
        </member>
        <member name="M:Newtonsoft.Json.Converters.EntityKeyMemberConverter.ReadJson(Newtonsoft.Json.JsonReader,System.Type,System.Object,Newtonsoft.Json.JsonSerializer)">
            <summary>
            Reads the JSON representation of the object.
            </summary>
            <param name="reader">The <see cref="T:Newtonsoft.Json.JsonReader"/> to read from.</param>
            <param name="objectType">Type of the object.</param>
            <param name="existingValue">The existing value of object being read.</param>
            <param name="serializer">The calling serializer.</param>
            <returns>The object value.</returns>
        </member>
        <member name="M:Newtonsoft.Json.Converters.EntityKeyMemberConverter.CanConvert(System.Type)">
         