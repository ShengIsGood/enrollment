.JTokenReader.CurrentToken">
            <summary>
            Gets the <see cref="T:Newtonsoft.Json.Linq.JToken"/> at the reader's current position.
            </summary>
        </member>
        <member name="M:Newtonsoft.Json.Linq.JTokenReader.#ctor(Newtonsoft.Json.Linq.JToken)">
            <summary>
            Initializes a new instance of the <see cref="T:Newtonsoft.Json.Linq.JTokenReader"/> class.
            </summary>
            <param name="token">The token to read from.</param>
        </member>
        <member name="M:Newtonsoft.Json.Linq.JTokenReader.Read">
            <summary>
            Reads the next JSON token from the stream.
            </summary>
            <returns>
            true if the next token was read successfully; false if there are no more tokens to read.
            </returns>
        </member>
        <member name="P:Newtonsoft.Json.Linq.JTokenReader.Path">
            <summary>
            Gets the path of the current JSON token. 
            </summary>
        </member>
        <member name="T:Newtonsoft.Json.Linq.JTokenType">
            <summary>
            Specifies the type of token.
            </summary>
        </member>
        <member name="F:Newtonsoft.Json.Linq.JTokenType.None">
            <summary>
            No token type has been set.
            </summary>
        </member>
        <member name="F:Newtonsoft.Json.Linq.JTokenType.Object">
            <summary>
            A JSON object.
            </summary>
        </member>
        <member name="F:Newtonsoft.Json.Linq.JTokenType.Array">
            <summary>
            A JSON array.
            </summary>
        </member>
        <member name="F:Newtonsoft.Json.Linq.JTokenType.Constructor">
            <summary>
            A JSON constructor.
            </summary>
        </member>
        <member name="F:Newtonsoft.Json.Linq.JTokenType.Property">
            <summary>
            A JSON object property.
            </summary>
        </member>
        <member name="F:Newtonsoft.Json.Linq.JTokenType.Comment">
            <summary>
            A comment.
            </summary>
        </member>
        <member name="F:Newtonsoft.Json.Linq.JTokenType.Integer">
            <summary>
            An integer value.
            </summary>
        </member>
        <member name="F:Newtonsoft.Json.Linq.JTokenType.Float">
            <summary>
            A float value.
            </summary>
        </member>
        <member name="F:Newtonsoft.Json.Linq.JTokenType.String">
            <summary>
            A string value.
            </summary>
        </member>
        <member name="F:Newtonsoft.Json.Linq.JTokenType.Boolean">
            <summary>
            A boolean value.
            </summary>
        </member>
        <member name="F:Newtonsoft.Json.Linq.JTokenType.Null">
            <summary>
            A null value.
            </summary>
        </member>
        <member name="F:Newtonsoft.Json.Linq.JTokenType.Undefined">
            <summary>
            An undefined value.
            </summary>
        </member>
        <member name="F:Newtonsoft.Json.Linq.JTokenType.Date">
            <summary>
            A date value.
            </summary>
        </member>
        <member name="F:Newtonsoft.Json.Linq.JTokenType.Raw">
            <summary>
            A raw JSON value.
            </summary>
        </member>
        <member name="F:Newtonsoft.Json.Linq.JTokenType.Bytes">
            <summary>
            A collection of bytes value.
            </summary>
        </member>
        <member name="F:Newtonsoft.Json.Linq.JTokenType.Guid">
            <summary>
            A Guid value.
            </summary>
        </member>
        <member name="F:Newtonsoft.Json.Linq.JTokenType.Uri">
            <summary>
            A Uri value.
            </summary>
        </member>
        <member name="F:Newtonsoft.Json.Linq.JTokenType.TimeSpan">
            <summary>
            A TimeSpan value.
            </summary>
        </member>
        <member name="T:Newtonsoft.Json.Linq.JTokenWriter">
            <summary>
            Represents a writer that provides a fast, non-cached, forward-only way of generating JSON data.
            </summary>
        </member>
   