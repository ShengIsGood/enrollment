ternally.  The method cannot be used for contexts created using Database
                First or Model First, for contexts created using a pre-existing <see cref="T:System.Data.Objects.ObjectContext"/>, or
                for contexts created using a pre-existing <see cref="T:System.Data.Entity.Infrastructure.DbCompiledModel"/>.
            </summary>
            <param name="context">The context.</param>
            <param name="writer">The writer.</param>
        </member>
        <member name="M:System.Data.Entity.Infrastructure.EdmxWriter.WriteEdmx(System.Data.Entity.Infrastructure.DbModel,System.Xml.XmlWriter)">
            <summary>
                Writes the Entity Data Model represented by the given <see cref="T:System.Data.Entity.Infrastructure.DbModel"/> to the
                given writer in EDMX form.
            </summary>
            <param name="modelaseMapping">An object representing the EDM.</param>
            <param name="writer">The writer.</param>
        </member>
        <member name="T:System.Data.Entity.Infrastructure.IDbContextFactory`1">
            <summary>
                A factory for creating derived <see cref="T:System.Data.Entity.DbContext"/> instances. Implement this 
                interface to enable design-time services for context types that do not have a 
                public default constructor.
                
                At design-time, derived <see cref="T:System.Data.Entity.DbContext"/> instances can be created in order to enable specific
                design-time experiences such as model rendering, DDL generation etc. To enable design-time instantiation
                for derived <see cref="T:System.Data.Entity.DbContext"/> types that do not have a public, default constructor, implement 
                this interface. Design-time services will auto-discover implementations of this interface that are in the
                same assembly as the derived <see cref="T:System.Data.Entity.DbContext"/> type.
            </summary>
            <typeparam name="TContext"></typeparam>
        </member>
        <member name="M:System.Data.Entity.Infrastructure.IDbContextFactory`1.Create">
            <summary>
                Creates a new instance of a derived <see cref="T:System.Data.Entity.DbContext"/> type.
            </summary>
            <returns>An instance of TContext</returns>
        </member>
        <member name="T:System.Data.Entity.Infrastructure.IncludeMetadataConvention">
            <summary>
                This <see cref="T:System.Data.Entity.DbModelBuilder"/> convention causes DbModelBuilder to include metadata about the model 
                when it builds the model. When <see cref="T:System.Data.Entity.DbContext"/> creates a model by convention it will
                add this convention to the list of those used by the DbModelBuilder.  This will then result in
                model 