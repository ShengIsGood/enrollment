ed in <see cref="P:System.Net.Http.HttpRequestMessage.Properties" />.</summary>
    </member>
    <member name="F:System.Web.Http.Hosting.HttpPropertyKeys.RetrieveClientCertificateDelegateKey">
      <summary>Provides a key for a delegate which can retrieve the client certificate for this request.</summary>
    </member>
    <member name="F:System.Web.Http.Hosting.HttpPropertyKeys.SynchronizationContextKey">
      <summary> Provides a key for the current <see cref="T:System.Threading.SynchronizationContext" /> stored in <see cref="M:HttpRequestMessage.Properties" />. If <see cref="M:SynchronizationContext.Current" /> is null then no context is stored. </summary>
    </member>
    <member name="T:System.Web.Http.Hosting.IHostBufferPolicySelector">
      <summary> Interface for controlling the use of buffering requests and responses in the host. If a host provides support for buffering requests and/or responses then it can use this interface to determine the policy for when buffering is to be used.</summary>
    </member>
    <member name="M:System.Web.Http.Hosting.IHostBufferPolicySelector.UseBufferedInputStream(System.Object)">
      <summary>Determines whether the host should buffer the <see cref="T:System.Net.Http.HttpRequestMessage" /> entity body.</summary>
      <returns>true if buffering should be used; otherwise a streamed request should be used.</returns>
      <param name="hostContext">The host context.</param>
    </member>
    <member name="M:System.Web.Http.Hosting.IHostBufferPolicySelector.UseBufferedOutputStream(System.Net.Http.HttpResponseMessage)">
      <summary>Determines whether the host should buffer the <see cref="T.System.Net.Http.HttpResponseMessage" /> entity body.</summary>
      <returns>true if buffering should be used; otherwise a streamed response should be used.</returns>
      <param name="response">The HTTP response message.</param>
    </member>
    <member name="T:System.Web.Http.Metadata.ModelMetadata">
      <summary>No content here will be updated; please do not add material here.</summary>
    </member>
    <member name="M:System.Web.Http.Metadata.ModelMetadata.#ctor(System.Web.Http.Metadata.ModelMetadataProvider,System.Type,System.Func{System.Object},System.Type,System.String)">
      <summary>Initializes a new instance of the <see cref="T:System.Web.Http.Metadata.ModelMetadata" /> class.</summary>
      <param name="provider">The provider.</param>
      <param name="containerType">The type of the container.</param>
      <param name="modelAccessor">The model accessor.</param>
      <param name="modelType">The type of the model.</param>
      <param name="propertyName">The name of the property.</param>
    </member>
    <member name="P:System.Web.Http.Metadata.ModelMetadata.AdditionalValues">
      <summary>Gets a dictionary that contains additional metadata about the model.</summary>
      <returns>A dictionary that contains additional metadata about the model.</returns>
    </member>
    <member name="P:System.Web.Http.Metadata.ModelMetadata.ContainerType">
      <summary>Gets or sets the type of the container for the model.</summary>
      <returns>The type of the container for the model.</returns>
    </member>
    <member name="P:System.Web.Http.Metadata.ModelMetadata.ConvertEmptyStringToNull">
      <summary>Gets or sets a value that indicates whether empty strings that are posted back in forms should be converted to null.</summary>
      <returns>true if empty strings that are posted back in forms should be converted to null; otherwise, false. The default value is true.</returns>
    </member>
    <member name="P:System.Web.Http.Metadata.ModelMetadata.Description">
      <summary>Gets or sets the description of the model.</summary>
      <returns>The description of the model. The default value is null.</returns>
    </member>
    <member name="M:System.Web.Http.Metadata.ModelMetadata.GetDisplayName">
      <summary>Gets the display name for the model.</summary>
      <returns>The display name for the model.</returns>
    </member>
    <member name="M:System.Web.Http.Metadata.ModelMetadata.GetValidators(System.Collections.Generic.IEnumerable{System.Web.Http.Validation.ModelValidatorProvider})">
      <summary>Gets a list of validators for the model.</summary>
      <returns>A list of validators for the model.</returns>
      <param name="validatorProviders">The validator providers for the model.</param>
    </member>
    <member name="P:System.Web.Http.Metadata.ModelMetadata.IsComplexType">
      <summary>Gets or sets a value that indicates whether the model is a complex type.</summary>
      <returns>A value that indicates whether the model is considered a complex.</returns>
    </member>
    <member name="P:System.We