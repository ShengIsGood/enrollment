
        <member name="M:System.Data.Entity.Edm.Serialization.Xml.Internal.Ssdl.DbModelSsdlHelper.GetRoleNamePair(System.Data.Entity.Edm.Db.DbTableMetadata,System.Data.Entity.Edm.Db.DbTableMetadata)">
            <summary>
                Return role name pair
            </summary>
            <param name = "firstTable"> </param>
            <param name = "secondTable"> </param>
            <returns> </returns>
        </member>
        <member name="T:System.Data.Entity.Edm.Validation.Internal.DataModelValidationContext">
            <summary>
                The context for DataModel Validation
            </summary>
        </member>
        <member name="M:System.Data.Entity.Edm.Validation.Internal.DataModelValidationHelper.AreRelationshipEndsEqual(System.Collections.Generic.KeyValuePair{System.Data.Entity.Edm.EdmAssociationSet,System.Dat