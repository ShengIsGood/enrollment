tpResponseMessage}})">
      <summary>Executes the filter action asynchronously.</summary>
      <returns>The newly created task for this operation.</returns>
      <param name="actionContext">The action context.</param>
      <param name="cancellationToken">The cancellation token assigned for this task.</param>
      <param name="continuation">The delegate function to continue after the action method is invoked.</param>
    </member>
    <member name="T:System.Web.Http.Filters.AuthorizationFilterAttribute">
      <summary>No content here will be updated; please do not add material here.</summary>
    </member>
    <member name="M:System.Web.Http.Filters.AuthorizationFilterAttribute.#ctor">
      <summary>Initializes a new instance of the <see cref="T:System.Web.Http.Filters.AuthorizationFilterAttribute" /> class.</summary>
    </member>
    <member name="M:System.Web.Http.Filters.AuthorizationFilterAttribute.OnAuthorization(System.Web.Http.Controllers.HttpActionContext)">
      <summary>Calls when a process requests authorization.</summary>
      <param name="actionContext">The action context, which encapsulates information for using <see cref="T:System.Web.Http.Filters.AuthorizationFilterAttribute" />.</param>
    </member>
    <member name="M:System.Web.Http.Filters.AuthorizationFilterAttribute.System#Web#Http#Filters#IAuthorizationFilter#ExecuteAuthorizationFilterAsync(System.Web.Http.Controllers.HttpActionContext,System.Threading.CancellationToken,System.Func{System.Threading.Tasks.Task{System.Net.Http.HttpResponseMessage}})">
      <summary>Executes the authorization filter during synchronization.</summary>
      <returns>The authorization filter during synchronization.</returns>
      <param name="actionContext">The action context, which encapsulates information for using <see cref="T:System.Web.Http.Filters.AuthorizationFilterAttribute" />.</param>
      <param name="cancellationToken">The cancellation token that cancels the operation.</param>
      <param name="continuation">A continuation of the operation.</param>
    </member>
    <member name="T:System.Web.Http.Filters.ConfigurationFilterProvider">
      <summary>Represents the configuration filter provider.</summary>
    </member>
    <member name="M:System.Web.Http.Filters.ConfigurationFilterProvider.#ctor">
      <summary>Initializes a new instance of the <see cref="T:System.Web.Http.Filters.ConfigurationFilterProvider" /> class.</summary>
    </member>
    <member name="M:System.Web.Http.Filters.ConfigurationFilterProvider.GetFilters(System.Web.Http.HttpConfiguration,System.Web.Http.Controllers.HttpActionDescriptor)">
      <summary>Returns the filters that are associated with this configuration method.</summary>
      <returns>The filters that are associated with this configuration method.</returns>
      <param name="configuration">The configuration.</param>
      <param name="actionDescriptor">The action descriptor.</param>
    </member>
    <member name="T:System.Web.Http.Filters.ExceptionFilterAttribute">
      <summary>Represents the attributes for the exception filter.</summary>
    </member>
    <member name="M:System.Web.Http.Filters.ExceptionFilterAttribute.#ctor">
      <summary>Initializes a new instance of the <see cref="T:System.Web.Http.Filters.ExceptionFilterAttribute" /> class.</summary>
    </member>
    <member name="M:System.Web.Http.Filters.ExceptionFilterAttribute.OnException(System.Web.Http.Filters.HttpActionExecutedContext)">
      <summary>Raises the exception event.</summary>
      <param name="actionExecutedContext">The context for the action.</param>
    </member>
    <member name="M:System.Web.Http.Filters.ExceptionFilterAttribute.System#Web#Http#Filters#IExceptionFilter#ExecuteExceptionFilterAsync(System.Web.Http.Filters.HttpActionExecutedContext,System.Threading.CancellationToken)">
      <summary>Asynchronously executes the exception filter.</summary>
      <returns>The result of the execution.</returns>
      <param name="actionExecutedContext">The context for the action.</param>
      <param name="cancellationToken">The cancellation context.</param>
    </member>
    <member name="T:System.Web.Http.Filters.FilterAttribute">
      <summary>Represents the base class for action-filter attributes.</summary>
    </member>
    <member name="M:System.Web.Http.Filters.FilterAttribute.#ctor">
      <summary>Initializes a new instance of the <see cref="T:System.Web.Http.Filters.FilterAttribute" /> class.</summary>
    </member>
    <member name="P:System.Web.Http.Filters.FilterAttribute.AllowMultiple">
      <summary>Gets a value that indicates whether multiple filters are allowed.<