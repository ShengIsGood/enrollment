        <member name="F:Newtonsoft.Json.FloatFormatHandling.DefaultValue">
            <summary>
            Write special floating point values as the property's default value in JSON, e.g. 0.0 for a <see cref="T:System.Double"/> property, null for a <see cref="T:System.Nullable`1"/> property.
            </summary>
        </member>
        <member name="T:Newtonsoft.Json.Formatting">
            <summary>
            Specifies formatting options for the <see cref="T:Newtonsoft.Json.JsonTextWriter"/>.
            </summary>
        </member>
        <member name="F:Newtonsoft.Json.Formatting.None">
            <summary>
            No special formatting is applied. This is the default.
            </summary>
        </member>
        <member name="F:Newtonsoft.Json.Formatting.Indented">
            <summary>
            Causes child objects to be indented according to the <see cref="P:Newtonsoft.Json.JsonTextWriter.Indentation"/> and <see cref="P:Newtonsoft.Json.JsonTextWriter.IndentChar"/> settings.
            </summary>
        </member>
        <member name="T:Newtonsoft.Json.IArrayPool`1">
            <summary>
            Provides an interface for using pooled arrays.
            </summary>
            <typeparam name="T">The array type content.</typeparam>
        </member>
        <member name="M:Newtonsoft.Json.IArrayPool`1.Rent(System.Int32)">
            <summary>
            Rent a array from the pool. This array must be returned when it is no longer needed.
            </summary>
            <param name="minimumLength">The minimum required length of the array. The returned array may be longer.</param>
            <returns>The rented array from the pool. This array must be returned when it is no longer needed.</returns>
        </member>
        <member name="M:Newtonsoft.Json.IArrayPool`1.Return(`0[])">
            <summary>
            Return an array to the pool.
            </summary>
            <param name="array">The array that is being returned.</param>
        </member>
        <member name="T:Newtonsoft.Json.IJsonLineInfo">
            <summary>
            Provides an interface to enable a class to return line and position information.
            </summary>
        </member>
        <member name="M:Newtonsoft.Json.IJsonLineInfo.HasLineInfo">
            <summary>
            Gets a value indicating whether the class can return line information.
            </summary>
            <returns>
            	<c>true</c> if LineNumber and LinePosition can be provided; otherwise, <c>false</c>.
            </returns>
        </member>
        <member name="P:Newtonsoft.Json.IJsonLineInfo.LineNumber">
            <summary>
            Gets the current line number.
            </summary>
            <value>The current line number or 0 if no line information is available (for example, HasLineInfo returns false).</value>
        </member>
        <member name="P:Newtonsoft.Json.IJsonLineInfo.LinePosition">
            <summary>
            Gets the current line position.
            </summary>
            <value>The current line position or 0 if no line information is available (for example, HasLineInfo returns false).</value>
        </member>
        <member name="T:Newtonsoft.Json.JsonArrayAttribute">
            <summary>
            Instructs the <see cref="T:Newtonsoft.Json.JsonSerializer"/> how to serialize the collection.
            </summary>
        </member>
        <member name="P:Newtonsoft.Json.JsonArrayAttribute.AllowNullItems">
            <summary>
            Gets or sets a value indicating whether null items are allowed in the collection.
            </summary>
            <value><c>true</c> if null items are allowed in the collection; otherwise, <c>false</c>.</value>
        </member>
        <member name="M:Newtonsoft.Json.JsonArrayAttribute.#ctor">
            <summary>
            Initializes a new instance of the <see cref="T:Newtonsoft.Json.JsonArrayAttribute"/> class.
            </summary>
        </member>
        <member name="M:Newtonsoft.Json.JsonArrayAttribute.#ctor(System.Boolean)">
            <summary>
            Initializes a new instance of the <see cref="T:Newtonsoft.Json.JsonObjectAttribute"/> class with a flag indicating whether the array can contain null items
            </summary>
            <param name="allowNullItems">A flag indicating whether the array can contain null items.</param>
        </member>
        <member name="M:Newtonsoft.Json.JsonArrayAttribute.#ctor(System.String)">
            <summary>
            Initializes a new instance of the <see cref="T:Newtonsoft.Json.JsonArrayAttribute"/> cl