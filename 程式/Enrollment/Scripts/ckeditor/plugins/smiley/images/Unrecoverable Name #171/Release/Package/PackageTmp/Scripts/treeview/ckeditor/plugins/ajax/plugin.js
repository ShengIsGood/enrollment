n SSDL artifacts
            </summary>
        </member>
        <member name="F:System.Data.Entity.Edm.Parsing.Xml.Internal.XmlErrorCode.InconsistentProviderManifestToken">
            <summary>
                Inconsistent Provider Manifest token values in SSDL artifacts
            </summary>
        </member>
        <member name="F:System.Data.Entity.Edm.Parsing.Xml.Internal.XmlErrorCode.DuplicatedFunctionoverloads">
            <summary>
                Duplicated Function overloads
            </summary>
        </member>
        <member name="F:System.Data.Entity.Edm.Parsing.Xml.Internal.XmlErrorCode.InvalidProvider">
            <summary>
                InvalidProvider
            </summary>
        </member>
        <member name="F:System.Data.Entity.Edm.Parsing.Xml.Internal.XmlErrorCode.FunctionWithNonEdmTypeNotSupported">
            <summary>
                FunctionWithNonEdmTypeNotSupported
            </summary>
        </member>
        <member name="F:System.Data.Entity.Edm.Parsing.Xml.Internal.XmlErrorCode.ComplexTypeAsReturnTypeAndDefinedEntitySet">
            <summary>
                ComplexTypeAsReturnTypeAndDefinedEntitySet
            </summary>
        </member>
        <member name="F:System.Data.Ent