tilities.ConfigurationFileUpdater">
            <summary>
                Utility class to prep the user's config file to run in an AppDomain
            </summary>
        </member>
        <member name="M:System.Data.Entity.Migrations.Utilities.ConfigurationFileUpdater.Update(System.String)">
            <summary>
                Updates a config file by adding binding redirects for EntityFramework.dll.
                This ensures that the user's code can be ran in an AppDomain and the exact
                same version of the assembly will be used for both domains.
            </summary>
            <param name = "configurationFile">That path of the user's config file. Can also be null or a path to an non-existent file.</param>
            <returns>The path of the updated config file. It is the caller's responsibility to delete this.</returns>
        </member>
        <member name="T