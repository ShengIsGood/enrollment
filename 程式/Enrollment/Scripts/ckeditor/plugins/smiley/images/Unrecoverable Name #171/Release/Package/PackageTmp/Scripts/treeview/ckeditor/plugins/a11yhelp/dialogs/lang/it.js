summary>
      <returns>A <see cref="T:System.Threading.Tasks.Task`1" /> representing the ongoing operation.</returns>
      <param name="request">The request to dispatch</param>
      <param name="cancellationToken">The cancellation token.</param>
    </member>
    <member name="T:System.Web.Http.Dispatcher.HttpRoutingDispatcher">
      <summary> This class is the default endpoint message handler which examines the <see cref="T:System.Web.Http.Routing.IHttpRoute" /> of the matched route, and chooses which message handler to call. If <see cref="P:System.Web.Http.Routing.IHttpRoute.Handler" /> is null, then it delegates to <see cref="T:System.Web.Http.Dispatcher.HttpControllerDispatcher" />. </summary>
    </member>
    <member name="M:System.Web.Http.Dispatcher.HttpRoutingDispatcher.#ctor(System.Web.Http.HttpConfiguration)">
      <summary> Initializes a new instance of the <see cref="T:System.Web.Http.Dispatcher.HttpRoutingDispatcher" /> class, using the provided <see cref="T:System.Web.Http.HttpConfiguration" /> and <see cref="T:System.Web.Http.Dispatcher.HttpControllerDispatcher" /> as the default handler. </summary>
      <param name="configuration">The server configuration.</param>
    </member>
    <member name="M:System.Web.Http.Dispatcher.HttpRoutingDispatcher.#ctor(System.Web.Http.HttpConfiguration,System.Net.Http.HttpMessageHandler)">
      <summary> Initializes a new instance of the <see cref="T:System.Web.Http.Dispatcher.HttpRoutingDispatcher" /> class, using the provided <see cref="T:System.Web.Http.HttpConfiguration" /> and <see cref="T:System.Net.Http.HttpMessageHandler" />. </summary>
      <param name="configuration">The server configuration.</param>
      <param name="defaultHandler">The default handler to use when the <see cref="T:System.Web.Http.Routing.IHttpRoute" /> has no <see cref="P:System.Web.Http.Routing.IHttpRoute.Handler" />.</param>
    </member>
    <member name="M:System.Web.Http.Dispatcher.HttpRoutingDispatcher.SendAsync(System.Net.Http.HttpRequestMessage,System.Threading.CancellationToken)">
      <summary>Sends an HTTP request as an asynchronous operation.</summary>
      <returns>The task object representing the asynchronous operation.</returns>
      <param name="request">The HTTP request message to send.</param>
      <param name="cancellationToken">The cancellation token to cancel operation.</param>
    </member>
    <member name="T:System.Web.Http.Dispatcher.IAssembliesResolver">
      <summary> Provides an abstraction for managing the assemblies of an application. A different implementation can be registered via the <see cref="T:System.Web.Http.Services.DependencyResolver" />. </summary>
    </member>
    <member name="M:System.Web.Http.Dispatcher.IAssembliesResolver.GetAssemblies">
      <summary> Returns a list of assemblies available for the application. </summary>
      <returns>An &lt;see cref="T:System.Collections.Generic.ICollection`1" /&gt; of assemblies.</returns>
    </member>
    <member name="T:System.Web.Http.Dispatcher.IHttpControllerActivator">
      <summary>Defines the methods that are required for an <see cref="T:System.Web.Http.Dispatcher.IHttpControllerActivator" />.</summary>
    </member>
    <member name="M:System.Web.Http.Dispatcher.IHttpControllerActivator.Create(System.Net.Http.HttpRequestMessage,System.Web.Http.Controllers.HttpControllerDescriptor,System.Type)">
      <summary>Creates an <see cref="T:System.Web.Http.Controllers.IHttpController" /> object.</summary>
      <returns>An <see cref="T:System.Web.Http.Controllers.IHttpController" /> object.</returns>
      <param name="request">The message request.</param>
      <param name="controllerDescriptor">The HTTP controller descriptor.</param>
      <param name="controllerType">The type of the controller.</param>
    </member>
    <member name="T:System.Web.Http.Dispatcher.IHttpControllerSelector">
      <summary> Defines the methods that are required for an <see cref="T:System.Web.Http.Controllers.IHttpController" /> factory. </summary>
    </member>
    <member name="M:System.Web.Http.Dispatcher.IHttpControllerSelector.GetControllerMapping">
      <summary> Returns a map, keyed by controller string, of all <see cref="T:System.Web.Http.Controllers.HttpControllerDescriptor" /> that the selector can select.  This is primarily called by <see cref="T:System.Web.Http.Description.IApiExplorer" /> to discover all the possible controllers in the system. </summary>
      <returns>A map of all <see cref="T:System.Web.Http.Controllers.HttpControllerDescriptor" /> that the selector can select, or null if the selector does not have a well-defined mapping of <see cref="T:System.Web.Http.Controllers.HttpControllerDescriptor" />.</returns>
    </member>
    <member name="M:System.Web.Http.Dispatcher.IHttpControllerSelector.SelectController(System.Net.Http.HttpRequestMessage)">