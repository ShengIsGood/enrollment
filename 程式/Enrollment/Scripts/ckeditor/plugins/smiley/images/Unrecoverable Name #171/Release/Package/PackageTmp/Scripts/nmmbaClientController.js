e">
            <summary>
                Gets the property name.
            </summary>
            <value>The property name.</value>
        </member>
        <member name="P:System.Data.Entity.Infrastructure.DbCollectionEntry`2.CurrentValue">
            <summary>
                Gets or sets the current value of the navigation property.  The current value is
                the entity that the navigation property references.
            </summary>
            <value>The current value.</value>
        </member>
        <member name="P:System.Data.Entity.Infrastructure.DbCollectionEntry`2.IsLoaded">
            <summary>
                Gets a value indicating whether the collection of entities has been loaded from the database.
            </summary>
            <value><c>true</c> if the collection is loaded; otherwise, <c>false</c>.</value>
        </member>
        <member name="P:System.Data.Entity.Infrastructure.DbCollectionEntry`2.InternalMemberEntry">
            <summary>
                Gets the underlying <see cref="T:System.Data.Entity.Internal.InternalCollectionEntry"/> as an <see cref="P:System.Data.Entity.Infrastructure.DbCollectionEntry`2.InternalMemberEntry"/>.
            </summary>
            <value>The internal member entry.</value>
        </member>
        <member name="P:System.Data.Entity.Infrastructure.DbCollectionEntry`2.EntityEntry">
            <summary>
                The <see cref="T:System.Data.Entity.Infrastructure.DbEntityEntry`1"/> to which this navigation property belongs.
            </summary>
            <value>An entry for the entity that owns this navigation property.</value>
        </member>
        <member name="T:System.Data.Entity.Infrastructure.DbCompiledModel">
            <summary>
                An immutable representation of an Entity Data Model (EDM) model that can be used to create an 
                <see cref="T:System.Data.Objects.ObjectContext"/> or can be passed to the constructor of a <see cref="T:System.Data.Entity.DbContext"/>. 
                For increased performance, instances of this type should be cached and re-used to construct contexts.
            </summary>
        </member>
        <member name="M:System.Data.Entity.Infrastructure.DbCompiledModel.#ctor">
            <summary>
            For mocking.
            </summary>
        </member>
        <member name="M:System.Data.Entity.Infrastructure.DbCompiledModel.#ctor(System.Data.Entity.Infrastructure.DbModel)">
        