:System.Net.Http.HttpRequestMessage" />.</summary>
      <returns>The created controller instance.</returns>
      <param name="request">The request message</param>
    </member>
    <member name="M:System.Web.Http.Controllers.HttpControllerDescriptor.GetCustomAttributes``1">
      <summary>Retrieves a collection of custom attributes of the controller.</summary>
      <returns>A collection of custom attributes</returns>
      <typeparam name="T">The type of the object.</typeparam>
    </member>
    <member name="M:System.Web.Http.Controllers.HttpControllerDescriptor.GetFilters">
      <summary>Returns a collection of filters associated with the controller.</summary>
      <returns>A collection of filters associated with the controller.</returns>
    </member>
    <member name="P:System.Web.Http.Controllers.HttpControllerDescriptor.Properties">
      <summary>Gets the properties associated with this instance.</summary>
      <returns>The properties associated with this instance.</returns>
    </member>
    <member name="T:System.Web.Http.Controllers.HttpControllerSettings">
      <summary>Contains settings for an HTTP controller.</summary>
    </member>
    <member name="M:System.Web.Http.Controllers.HttpControllerSettings.#ctor(System.Web.Http.HttpConfiguration)">
      <summary>Initializes a new instance of the <see cref="T:System.Web.Http.Controllers.HttpControllerSettings" /> class.</summary>
      <param name="configuration">A configuration object that is used to initialize the instance.</param>
    </member>
    <member name="P:System.Web.Http.Controllers.HttpControllerSettings.Formatters">
      <summary>Gets the collection of <see cref="T:System.Net.Http.Formatting.MediaTypeFormatter" /> instances for the controller.</summary>
      <returns>The collection of <see cref="T:System.Net.Http.Formatting.MediaTypeFormatter" /> instances.</returns>
    </member>
    <member name="P:System.Web.Http.Controllers.HttpControllerSettings.ParameterBindingRules">
      <summary>Gets the collection of parameter bindingfunctions for for the controller.</summary>
      <returns>The collection of parameter binding functions.</returns>
    </member>
    <member name="P:System.Web.Http.Controllers.HttpControllerSettings.Services">
      <summary>Gets the collection of service instances for the controller.</summary>
      <returns>The collection of service instances.</returns>
    </member>
    <member name="T:System.Web.Http.Controllers.HttpParameterBinding">
      <summary> Describes how a parameter is bound. The binding should be static (based purely on the descriptor) and  can be shared across requests.  </summary>
    </member>
    <member name="M:System.Web.Http.Controllers.HttpParameterBinding.#ctor(System.Web.Http.Controllers.HttpParameterDescriptor)">
      <summary>Initializes a new instance of the <see cref="T:System.Web.Http.Controllers.HttpParameterBinding" /> class.</summary>
      <param name="descriptor">An <see cref="T:System.Web.Http.Controllers.HttpParameterDescriptor" /> that describes the parameters.</param>
    </member>
    <member name="P:System.Web.Http.Controllers.HttpParameterBinding.Descriptor">
      <summary>Gets the <see cref="T:System.Web.Http.Controllers.HttpParameterDescriptor" /> that was used to initialize this instance.</summary>
      <returns>The <see cref="T:System.Web.Http.Controllers.HttpParameterDescriptor" /> instance.</returns>
    </member>
    <member name="P:System.Web.Http.Controllers.HttpParameterBinding.ErrorMessage">
      <summary>If the binding is invalid, gets an error message that describes the binding error.</summary>
      <returns>An error message. If the binding was successful, the value is null.</returns>
    </member>
    <member name="M:System.Web.Http.Controllers.HttpParameterBinding.ExecuteBindingAsync(System.Web.Http.Metadata.ModelMetadataProvider,System.Web.Http.Controllers.HttpActionContext,System.Threading.CancellationToken)">
      <summary>Asynchronously executes the binding for the given request.</summary>
      <returns>A task object representing the asynchronous operation.</returns>
      <param name="metadataProvider">Metadata provider to use for validation.</param>
      <param name="actionContext">The action context for the binding. The action context contains the parameter dictionary that will get populated with the parameter.</param>
   