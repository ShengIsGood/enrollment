ed at the token to read into this <see cref="T:Newtonsoft.Json.Linq.JToken"/>.</param>
            <param name="settings">The <see cref="T:Newtonsoft.Json.Linq.JsonLoadSettings"/> used to load the JSON.
            If this is null, default load settings will be used.</param>
            <returns>
            An <see cref="T:Newtonsoft.Json.Linq.JToken"/> that contains the token and its descendant tokens
            that were read from the reader. The runtime type of the token is determined
            by the token type of the first token encountered in the reader.
            </returns>
        </member>
        <member name="M:Newtonsoft.Json.Linq.JToken.Parse(System.String)">
            <summary>
            Load a <see cref="T:Newtonsoft.Json.Linq.JToken"/> from a string that contains JSON.
            </summary>
            <param name="json">A <see cref="T:System.String"/> that contains JSON.</param>
            <returns>A <see cref="T:Newtonsoft.Json.Linq.JToken"/> populated from the string that contains JSON.</returns>
        </member>
        <member name="M:Newtonsoft.Json.Linq.JToken.Parse(System.String,Newtonsoft.Json.Linq.JsonLoadSettings)">
            <summary>
            Load a <see cref="T:Newtonsoft.Json.Linq.JToken"/> from a string that contains JSON.
            </summary>
            <param name="json">A <see cref="T:System.String"/> that contains JSON.</param>
            <param name="settings">The <see cref="T:Newtonsoft.Json.Linq.JsonLoadSettings"/> used to load the JSON.
            If this is null, default load settings will be used.</param>
            <returns>A <see cref="T:Newtonsoft.Json.Linq.JToken"/> populated from the string that contains JSON.</returns>
        </member>
        <member name="M:Newtonsoft.Json.Linq.JToken.Load(Newtonsoft.Json.JsonReader,Newtonsoft.Json.Linq.JsonLoadSettings)">
            <summary>
            Creates a <see cref="T:Newtonsoft.Json.Linq.JToken"/> from a <see cref="T:Newtonsoft.Json.JsonReader"/>.
            </summary>
            <param name="reader">An <see cref="T:Newtonsoft.Json.JsonReader"/> positioned at the token to read into this <see cref="T:Newtonsoft.Json.Linq.JToken"/>.</param>
            <param name="settings">The <see cref="T:Newtonsoft.Json.Linq.JsonLoadSettings"/> used to load the JSON.
            If this is null, default load settings will be used.</param>
            <returns>
            An <see cref="T:Newtonsoft.Json.Linq.JToken"/> that contains the token and its descendant tokens
            that were read from the reader. The runtime type of the token is determined
            by the token type of the first token encountered in the reader.
            </returns>
        </member>
        <member name="M:Newtonsoft.Json.Linq.JToken.Load(Newtonsoft.Json.JsonReader)">
            <summary>
            Creates a <see cref="T:Newtonsoft.Json.Linq.JToken"/> from a <see cref="T:Newtonsoft.Json.JsonReader"/>.
            </summary>
            <param name="reader">An <see cref="T:Newtonsoft.Json.JsonReader"/> positioned at the token to read into this <see cref="T:Newtonsoft.Json.Linq.JToken"/>.</param>
            <returns>
            An <see cref="T:Newtonsoft.Json.Linq.JToken"/> that contains the token and its descendant tokens
            that were read from the reader. The runtime type of the token is determined
            by the token type of the first token encountered in the reader.
            </returns>
        </member>
        <member name="M:Newtonsoft.Json.Linq.JToken.SelectToken(System.String)">
            <summary>
            Selects a <see cref="T:Newtonsoft.Json.Linq.JToken"/> using a JPath expression. Selects the token that matches the object path.
            </summary>
            <param name="path">
            A <see cref="T:System.String"/> that contains a JPath expression.
            </param>
            <returns>A <see cref="T:Newtonsoft.Json.Linq.JToken"/>, or null.</returns>
        </member>
        <member name="M:Newtonsoft.Json.Linq.JToken.SelectToken(System.String,System.Boolean)">
            <summary>
            Selects a <see cref="T:Newtonsoft.Json.Linq.JToken"/> using a JPath expression. Selects the token that matches the object path.
            </summary>
            <param name="path">
            A <see cref="T:System.String"/> that contains a JPath expression.
            </param>
            <param name="errorWhenNoMatch">A flag to indicate whether an error should be thrown if no tokens are found when evaluating part of the expression.</param>
            <returns>A <see cref="T:Newtonsoft.Json.Linq.JToken"/>.</returns>
        </member>
        <member name="M:Newtonsoft.Json.Linq.JToken.SelectTokens(System.String)">
            <summary>
            Selects a collection of elements using a JPath expression.
            </summary>
            <param name="path">
            A <see cref="T:System.String"/> that contains a JPath expression.
            </param>
            <returns>An <see cref="T:System.Collections.Generic.IEnumerable`1"/> that contains the selected elements.</returns>
        </member>
        <member name="M:Newtonsoft.Json.Linq.JToken.SelectTokens(System.String,System.Boolean)">
            <summary>
            Selects a collection of elements using a JPath expression.
            </summary>
            <param name="path">
            A <see cref="T:System.String"/> that contains a JPath expression.
            </param>
            <param name="errorWhenNoMatch">A flag to indicate whether an error should be thrown if no tokens are found when evaluating part of the expression.</param>
            <returns>An <see cref="T:System.Collections.Generic.IEnumerable`1"/> that contains the selected elements.</returns>
        </member>
        <member name="M:Newtonsoft.Json.Linq.JToken.GetMetaObject(System.Linq.Expressions.Expression)">
            <summary>
            Returns the <see cref="T:System.Dynamic.DynamicMetaObject"/> responsible for binding operations performed on this object.
            </summary>
            <param name="parameter">The expression tree representation of the runtime value.</param>
            <returns>
            The <see cref="T:System.Dynamic.DynamicMetaObject"/> to bind this object.
            </returns>
        </member>
        <member name="M:Newtonsoft.Json.Linq.JToken.System#Dynamic#IDynamicMetaObjectProvider#GetMetaObject(System.Linq.Expressions.Expression)">
            <summary>
            Returns the <see cref="T:System.Dynamic.DynamicMetaObject"/> responsible for binding operations performed on this object.
            </summary>
            <param name="parameter">The expression tree representation of the runtime value.</param>
            <returns>
            The <see cref="T:System.Dynamic.DynamicMetaObject"/> to bind this object.
            </returns>
        </member>
        <member name="M:Newtonsoft.Json.Linq.JToken.DeepClone">
            <summary>
            Creates a new instance of the <see cref="T:Newtonsoft.Json.Linq.JToken"/>. All child tokens are recursively cloned.
            </summary>
            <returns>A new instance of the <see cref="T:Newtonsoft.Json.Linq.JToken"/>.</returns>
        </member>
        <member name="M:Newtonsoft.Json.Linq.JToken.AddAnnotation(System.Object)">
            <summary>
            Adds an object to the annotation list of this <see cref="T:Newtonsoft.Json.Linq.JToken"/>.
            </summary>
            <param name="annotation">The annotation to add.</param>
        </member>
        <member name="M:Newtonsoft.Json.Linq.JToken.Annotation``1">
            <summary>
            Get the first annotation object of the specified type from this <see cref="T:Newtonsoft.Json.Linq.JToken"/>.
            </summary>
            <typeparam name="T">The type of the annotation to retrieve.</typeparam>
            <returns>The first annotation object that matches the specified type, or <c>null</c> if no annotation is of the specified type.</returns>
        </member>
        <member name="M:Newtonsoft.Json.Linq.JToken.Annotation(System.Type)">
            <summary>
            Gets the first annotation object of the specified type from this <see cref="T:Newtonsoft.Json.Linq.JToken"/>.
            </summary>
            <param name="type">The <see cref="P:Newtonsoft.Json.Linq.JToken.Type"/> of the annotation to retrieve.</param>
            <returns>The first annotation object that matches the specified type, or <c>null</c> if no annotation is of the specified type.</returns>
        </member>
        <member name="M:Newtonsoft.Json.Linq.JToken.Annotations``1">
            <summary>
            Gets a collection of annotations of the specified type for this <see cref="T:Newtonsoft.Json.Linq.JToken"/>.
            </summary>
            <typeparam name="T">The type of the annotations to retrieve.</typeparam>
            <returns>An <see cref="T:System.Collections.Generic.IEnumerable`1"/>  that contains the annotations for this <see cref="T:Newtonsoft.Json.Linq.JToken"/>.</returns>
        </member>
        <member name="M:Newtonsoft.Json.Linq.JToken.Annotations(System.Type)">
            <summary>
            Gets a collection of annotations of the specified type for this <see cref="T:Newtonsoft.Json.Linq.JToken"/>.
            </summary>
            <param name="type">The <see cref="P:Newtonsoft.Json.Linq.JToken.Type"/> of the annotations to retrieve.</param>
            <returns>An <see cref="T:System.Collections.Generic.IEnumerable`1"/> of <see cref="T:System.Object"/> that contains the annotations that match the specified type for this <see cref="T:Newtonsoft.Json.Linq.JToken"/>.</returns>
        </member>
        <member name="M:Newtonsoft.Json.Linq.JToken.RemoveAnnotations``1">
            <summary>
            Removes the annotations of the specified type from this <see cref="T:Newtonsoft.Json.Linq.JToken"/>.
            </summary>
            <typeparam name="T">The type of annotations to remove.</typeparam>
        </member>
        <member name="M:Newtonsoft.Json.Linq.JToken.RemoveAnnotations(System.Type)">
            <summary>
            Removes the annotations of the specified type from this <see cref="T:Ne