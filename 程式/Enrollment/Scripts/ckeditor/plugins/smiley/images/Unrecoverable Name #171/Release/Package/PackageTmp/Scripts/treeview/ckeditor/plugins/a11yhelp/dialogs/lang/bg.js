     is called.
            </summary>
            <param name = "entity">The entity to remove.</param>
            <returns>The entity.</returns>
            <remarks>
                Note that if the entity exists in the context in the Added state, then this method
                will cause it to be detached from the context.  This is because an Added entity is assumed not to
                exist in the database such that trying to delete it does not make sense.
            </remarks>
        </member>
        <member name="M:System.Data.Entity.DbSet.Create">
            <summary>
                Creates a new instance of an entity for the type of this set.
                Note that this instance is NOT added or attached to the set.
                The instance returned will be a proxy if the underlying context is configured to create
                proxies and the entity type meets the requirements for creating a proxy.
            </summary>
            <returns>The entity instance, which may be a proxy.</returns>
        </member>
        <member name="M:System.Data.Entity.DbSet.Create(System.Type)">
            <summary>
                Creates a new instance of an entity for the type of this set or for a type derived
                from the type of this set.
                Note that this instance is NOT added or attached to the set.
                The instance returned will be a proxy if the underlying context is configured to create
                proxies and the entity type meets the requirements for creating a proxy.
            </summary>
            <returns> The entity instance, which may be a proxy. </returns>
        </member>
        <member name="M:System.Data.Entity.DbSet.Cast``1">
            <summary>
                Returns the equivalent generic <see cref="T:System.Data.Entity.DbSet`1"/> object.
            </summary>
            <typeparam name="TEntity">The type of entity for which the set was created.</typeparam>
            <returns>The generic set object.</returns>
        </member>
        <member name="M:System.Data.Entity.DbSet.SqlQuery(System.String,System.Object[])">
            <summary>
                Creates a raw SQL query that will return entities in this set.  By default, the
                entities returned are tracked by the context; this can be changed by calling
                AsNoTracking on the <see cref="T:System.Data.Entity.Infrastructure.DbSqlQuery"/> returned.
                Note that the entities returned are always of the type for this set and never of
                a derived type.  If the table or tables queried may contain data for other entity
                types, then the SQL query must be written appropriately to ensure that only entities of
                the correct type are returned.
            </summary>
            <param name="sql">The SQL query string.</param>
            <param name="parameters">The parameters to apply to the SQL query string.</param>
            <returns>A <see cref="T:System.Data.Entity.Infrastructure.DbSqlQuery"/> object that will execute the query when it is enumerated.</returns>
        </member>
        <member name="P:System.Data.Entity.DbSet.Local">
            <summary>
                Gets an <see cref="T:System.Collections.ObjectModel.ObservableCollection`1"/> that represents a local view of all Added, Unchanged,
                and Modified entities in this set.  This local view will stay in sync as entities are added or
                removed from the context.  Likewise, entities added to or removed from the local view will automatically
                be added to or removed from the context.
            </summary>
            <remarks>
                This property can be used for data binding by populating the set with data, for example by using the Load
                extension method, and then binding to the local data through