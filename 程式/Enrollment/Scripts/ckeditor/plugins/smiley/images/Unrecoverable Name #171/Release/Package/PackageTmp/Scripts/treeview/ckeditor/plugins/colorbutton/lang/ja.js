            Scaffolds the initial code-based migration corresponding to a previously run database initializer.
            </summary>
            <returns>The scaffolded migration.</returns>
        </member>
        <member name="P:System.Data.Entity.Migrations.Design.MigrationScaffolder.Namespace">
            <summary>
            Gets or sets the namespace used in the migration's generated code.
            
            By default, this is the same as MigrationsNamespace on the migrations
            configuration object passed into the constructor. For VB.NET projects, this
            will need to be updated to take into account the project's root namespace.
            </summary>
        </member>
        <member name="T:System.Data.Entity.Migrations.Design.ScaffoldedMigration">
            <summary>
                Represents a code-based migration that 