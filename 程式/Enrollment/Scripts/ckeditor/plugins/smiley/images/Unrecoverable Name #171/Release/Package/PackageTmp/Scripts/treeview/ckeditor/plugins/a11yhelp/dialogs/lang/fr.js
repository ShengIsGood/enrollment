>
      <returns>A reference to an object that implements the <see cref="T:System.Web.Http.ModelBinding.IModelBinder" /> interface.</returns>
    </member>
    <member name="T:System.Web.Http.ModelBinding.DefaultActionValueBinder">
      <summary>No content here will be updated; please do not add material here.</summary>
    </member>
    <member name="M:System.Web.Http.ModelBinding.DefaultActionValueBinder.#ctor">
      <summary>Initializes a new instance of the <see cref="T:System.Web.Http.ModelBinding.DefaultActionValueBinder" /> class.</summary>
    </member>
    <member name="M:System.Web.Http.ModelBinding.DefaultActionValueBinder.GetBinding(System.Web.Http.Controllers.HttpActionDescriptor)">
      <summary>Default implementation of the <see cref="T:System.Web.Http.Controllers.IActionValueBinder" /> interface. This interface is the primary entry point for binding action parameters.</summary>
      <returns>The <see cref="T:System.Web.Http.Controllers.HttpActionBinding" /> associated with the <see cref="T:System.Web.Http.ModelBinding.DefaultActionValueBinder" />.</returns>
      <param name="actionDescriptor">The action descriptor.</param>
    </member>
    <member name="M:System.Web.Http.ModelBinding.DefaultActionValueBinder.GetParameterBinding(System.Web.Http.Controllers.HttpParameterDescriptor)">
      <summary>Gets the <see cref="T:System.Web.Http.Controllers.HttpParameterBinding" /> associated with the <see cref="T:System.Web.Http.ModelBinding.DefaultActionValueBinder" />.</summary>
      <returns>The <see cref="T:System.Web.Http.Controllers.HttpParameterBinding" /> associated with the <see cref="T:System.Web.Http.ModelBinding.DefaultActionValueBinder" />.</returns>
      <param name="parameter">The parameter descriptor.</param>
    </member>
    <member name="T:System.Web.Http.ModelBinding.ErrorParameterBinding">
      <summary>Defines a binding error.</summary>
    </member>
    <member name="M:System.Web.Http.ModelBinding.ErrorParameterBinding.#ctor(System.Web.Http.Controllers.HttpParameterDescriptor,System.String)">
      <summary>Initializes a new instance of the <see cref="T:System.Web.Http.ModelBinding.ErrorParameterBinding" /> class.</summary>
      <param name="descriptor">The error descriptor.</param>
      <param name="message">The message.</param>
    </member>
    <member name="P:System.Web.Http.ModelBinding.ErrorParameterBinding.ErrorMessage">
      <summary>Gets the error message.</summary>
      <returns>The error message.</returns>
    </member>
    <member name="M:System.Web.Http.ModelBinding.ErrorParameterBinding.ExecuteBindingAsync(System.Web.Http.Metadata.ModelMetadataProvider,System.Web.Http.Controllers.HttpActionContext,System.Threading.CancellationToken)">
      <summary>Executes the binding method during synchronization.</summary>
      <param name="metadataProvider">The metadata provider.</param>
      <param name="actionContext">The action context.</param>
      <param name="cancellationToken">The cancellation Token value.</param>
    </member>
    <member name="T:System.Web.Http.ModelBinding.FormatterParameterBinding">
      <summary>Represents parameter binding that will read from the body and invoke the formatters.</summary>
    </member>
    <member name="M:System.Web.Http.ModelBinding.FormatterParameterBinding.#ctor(System.Web.Http.Controllers.HttpParameterDescriptor,System.Collections.Generic.IEnumerable{System.Net.Http.Formatting.MediaTypeFormatter},System.Web.Http.Validation.IBodyModelValidator)">
      <summary>Initializes a new instance of the <see cref="T:System.Web.Http.ModelBinding.FormatterParameterBinding" /> class.</summary>
      <param name="descriptor">The descriptor.</param>
      <param name="formatters">The formatter.</param>
      <param name="bodyModelValidator">The body model validator.</param>
    </member>
    <member name="P:System.Web.Http.ModelBinding.FormatterParameterBinding.BodyModelValidator">
      <summary>Gets or sets an interface for the body model validator.</summary>
      <returns>An interface for the body model validator.</returns>
    </member>
    <member name="P:System.Web.Http.ModelBinding.FormatterParameterBinding.ErrorMessage">
      <summary>Gets the error message.</summary>
      <returns>The error message.</returns>
    </member>
    <member name="M:System.Web.Http.ModelBinding.FormatterParameterBinding.ExecuteBindingAsync(System.Web.Http.Metadata.ModelMetadataProvider,System.Web.Http.Controllers.HttpActionContext,System.Threading.CancellationToken)">
      <summary>Asynchronously execute the binding of <see cref="T:System.Web.Http.ModelBinding.FormatterParameterBinding" />.</summary>
      <returns>The result of the action.</returns>
      <param name="metadataProvider">The metadata provider.</param>
      <param name="actionContext">The context associated with the action.</param>
      <param name="cancellationToken">The cancellation token.</param>
    </member>
    <member name="P:System.Web.Http.ModelBinding.FormatterParameterBinding.Formatters">
      <summary>Gets or sets an enumerable object that represents the formatter for the parameter binding.</summary>
      <returns>An en