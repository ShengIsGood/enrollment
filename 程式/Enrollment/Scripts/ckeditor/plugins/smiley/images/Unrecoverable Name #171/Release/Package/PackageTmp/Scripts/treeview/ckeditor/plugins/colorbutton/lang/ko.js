ty.Migrations.Model.ColumnModel,System.Data.Entity.Migrations.Utilities.IndentedTextWriter,System.Boolean)">
            <summary>
                Generates code to specify the definition for a <see cref="T:System.Data.Entity.Migrations.Model.ColumnModel"/>.
            </summary>
            <param name="column">The column definition to generate code for.</param>
            <param name="writer">Text writer to add the generated code to.</param>
            <param name="emitName">A value indicating whether to include the column name in the definition.</param>
        </member>
        <member name="M:System.Data.Entity.Migrations.Design.CSharpMigrationCodeGenerator.Generate(System.Byte[])">
            <summary>
                Generates code to specify the default value for a <see cref = "T:byte[]" /> column.
            </summary>
            <param name = "defaultValue">The value to be used as the default.</param>
            <returns>Code representing the default value.</re