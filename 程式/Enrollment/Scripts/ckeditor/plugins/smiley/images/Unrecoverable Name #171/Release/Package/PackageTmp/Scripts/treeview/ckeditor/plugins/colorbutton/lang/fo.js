"P:System.Data.Entity.Migrations.Model.ForeignKeyOperation.PrincipalTable">
            <summary>
                Gets or sets the name of the table that the foreign key constraint targets.
            </summary>
        </member>
        <member name="P:System.Data.Entity.Migrations.Model.ForeignKeyOperation.DependentTable">
            <summary>
                Gets or sets the name of the table that the foreign key columns exist in.
            </summary>
        </member>
        <member name="P:System.Data.Entity.Migrations.Model.ForeignKeyOperation.DependentColumns">
            <summary>
                The names of the foreign key column(s).
            </summary>
        </member>
        <member name="P:System.Data.Entity.Migrations.Model.ForeignKeyOperation.HasDefaultName">
            <summary>
                Gets a value indicating if a specific name has been supplied for this foreign key constraint.