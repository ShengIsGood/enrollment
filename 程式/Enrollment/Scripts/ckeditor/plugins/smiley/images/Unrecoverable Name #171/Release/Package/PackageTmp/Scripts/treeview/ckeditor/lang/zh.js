     <param name="actionContext">The action context.</param>
    </member>
    <member name="M:System.Web.Http.Validation.ModelValidationNode.Validate(System.Web.Http.Controllers.HttpActionContext,System.Web.Http.Validation.ModelValidationNode)">
      <summary>Validates the model using the specified execution context and parent node.</summary>
      <param name="actionContext">The action context.</param>
      <param name="parentNode">The parent node.</param>
    </member>
    <member name="P:System.Web.Http.Validation.ModelValidationNode.ValidateAllProperties">
      <summary>Gets or sets a value that indicates whether all properties of the model should be validated.</summary>
      <returns>true if all properties of the model should be validated, or false if validation should be skipped.</returns>
    </member>
    <member name="E:System.Web.Http.Validation.ModelValidationNode.Validated">
      <summary>Occurs when the model has been validated.</summary>
    </member>
    <member name="E:System.Web.Http.Validation.ModelValidationNode.Validating">
      <summary>Occurs when the model is being validated.</summary>
    </member>
    <member name="T:System.Web.Http.Validation.ModelValidationRequiredMemberSelector">
      <summary>Represents the selection of required members by checking for any required ModelValidators associated with the member.</summary>
    </member>
    <member name="M:System.Web.Http.Validation.ModelValidationRequiredMemberSelector.#ctor(System.Web.Http.Metadata.ModelMetadataProvider,System.Collections.Generic.IEnumerable{System.Web.Http.Validation.ModelValidatorProvider})">
      <summary>Initializes a new instance of the <see cref="T:System.Web.Http.Validation.ModelValidationRequiredMemberSelector" /> class.</summary>
      <param name="metadataProvider">The metadata provider.</param>
      <param name="validatorProviders">The validator providers.</param>
    </member>
    <member name="M:System.Web.Http.Validation.ModelValidationRequiredMemberSelector.IsRequiredMember(System.Reflection.MemberInfo)">
      <summary>Indicates whether the member is required for validation.</summary>
      <returns>true if the member is required for validation; otherwise, false.</returns>
      <param name="member">The member.</param>
    </member>
    <member name="T:System.Web.Http.Validation.ModelValidationResult">
      <summary>Provides a container for a validation result.</summary>
    </member>
    <member name="M:System.Web.Http.Validation.ModelValidationResult.#ctor">
      <summary>Initializes a new instance of the <see cref="T:System.Web.Http.Validation.ModelValidationResult" /> class.</summary>
    </member>
    <member name="P:System.Web.Http.Validation.ModelValidationResult.MemberName">
      <summary>Gets or sets the name of the member.</summary>
      <returns>The name of the member.</returns>
    </member>
    <member name="P:System.Web.Http.Validation.ModelValidationResult.Message">
      <summary>Gets or sets the validation result message.</summary>
      <returns>The validation result message.</returns>
    </member>
    <member name="T:System.Web.Http.Validation.ModelValidator">
      <summary>Provides a base class for implementing validation logic.</summary>
    </member>
    <member name="M:System.Web.Http.Validation.ModelValidator.#ctor(System.Collections.Generic.IEnumerable{System.Web.Http.Validation.ModelValidatorProvider})">
      <summary>Initializes a new instance of the <see cref="T:System.Web.Http.Validation.ModelValidator" /> class.</summary>
      <param name="validatorProviders">The validator providers.</param>
    </member>
    <member name="M:System.Web.Http.Validation.ModelValidator.GetModelValidator(System.Collections.Generic.IEnumerable{System.Web.Http.Validation.ModelValidatorProvider})">
      <summary>Returns a composite model validator for the model.</summary>
      <returns>A composite model validator for the model.</returns>
      <param name="validatorProviders">An enumeration of validator providers.</param>
    </member>
    <member name="P:System.Web.Http.Validation.ModelValidator.IsRequired">
      <summary>Gets a value that indicates whether a model property is required.</summary>
      <returns>true if the model property is required; otherwise, false.</returns>
    </member>
    <member name="M:System.Web.Http.Validation.ModelValidator.Validate(System.Web.Http.Metadata.ModelMetadata,System.Object)">
      <summary>Validates a specified object.</summary>
      <returns>A list of validation results.</returns>
      <param name="metadata">The metadata.</param>
      <param name="container">The container.</param>
    </member>
    <member name="P:System.Web.Http.Validation.ModelValidator.ValidatorProviders">
      <summary>Gets or sets an enumeration of validator providers.</summary>
      <returns>An enumeration of validator providers.</returns>
    </member>
    <member name="T:System.Web.Http.Validation.ModelValidatorProvider">
      <summary>Provides a list of validators for a model.</summary>
    </member>
    <member name="M:System.Web.Http.Validation.ModelValidatorProvider.#ctor">
      <summary>Initializes a new instance of the <see cref="T:System.Web.Http.Validation.ModelValidatorProvider" /> class.</summary>
    </member>
    <member name="M:System.Web.Http.Validation.ModelValidatorProvider.GetValidators(System.Web.Http.Metadata.ModelMetadata,System.Collections.Generic.IEnumerable{System.Web.Http.Validation.ModelValidatorProvider})">
      <summary>Gets a list of validators associated with this <see cref="T:System.Web.Http.Validation.ModelValidatorProvider" />.</summary>
      <returns>The list of validators.</returns>
      <param name="metadata">The metadata.</param>
      <param name="validatorProviders">The validator providers.</param>
    </member>
    <member name="T:System.Web.Http.Validation.Providers.AssociatedValidatorProvider">
      <summary>Provides an abstract class for classes that implement a validation provider.</summary>
    </member>
    <member name="M:System.Web.Http.Validation.Providers.AssociatedValidatorProvider.#ctor">
      <summary>Initializes a new instance of the <see cref="T:System.Web.Http.Validation.Providers.AssociatedValidatorProvider" /> class.</summary>
    </member>
    <member name="M:System.Web.Http.Validation.Providers.AssociatedValidatorProvider.GetTypeDescriptor(System.Type)">
      <summary>Gets a type descriptor for the specified type.</summary>
      <returns>A type descriptor for the specified type.</returns>
      <param name="type">The type of the validation provider.</param>
    </member>
    <member name="M:System.Web.Http.Validation.Providers.AssociatedValidatorProvider.GetValidators(System.Web.Http.Metadata.ModelMetadata,System.Collections.Generic.IEnumerable{System.Web.Http.Validation.ModelValidatorProvider})">
      <summary>Gets the validators for the model using the metadata and validator providers.</summary>
      <returns>The validators for the model.</returns>
      <param name="metadata">The metadata.</param>
      <param name="validatorProviders">An enumeration of validator providers.</param>
    </member>
    <member name="M:System.Web.Http.Validation.Providers.AssociatedValidatorProvider.GetValidators(System.Web.Http.Metadata.ModelMetadata,System.Collections.Generic.IEnumerable{System.Web.Http.Validation.ModelValidatorProvider},System.Collections.Generic.IEnumerable{System.Attribute})">
      <summary>Gets the validators for the model using the metadata, the validator providers, and a list of attributes.</summary>
      <returns>The validators for the model.</returns>
      <param name="metadata">The metadata.</param>
      <param name="validatorProviders">An enumeration of validator providers.</param>
      <param name="attributes">The list of attributes.</param>
    </member>
    <member name="T:System.Web.Http.Validation.Providers.DataAnnotationsModelValidationFactory">
      <summary>Represents the method that creates a <see cref="T:System.Web.Http.Validation.Providers.DataAnnotationsModelValidatorProvider" /> instance.</summary>
    </member>
    <member name="T:System.Web.Http.Validation.Providers.DataAnnotationsModelValidatorProvider">
      <summary>Represents an implementation of <see cref="T:System.Web.Http.Validation.ModelValidatorProvider" /> which providers validators for attributes which derive from <see cref="T:System.ComponentModel.DataAnnotations.ValidationAttribute" />. It also provides a validator for types which implement <see cref="T:System.ComponentModel.DataAnnotations.IValidatableObject" />. To support client side validation, you can either register adapters through the static methods on this class, or by having your validation attributes implement <see cref="T:System.Web.Http.Validation.IClientValidatable" />. The logic to support IClientValidatable is implemented in <see cref="T:System.Web.Http.Validation.Validators.DataAnnotationsModelValidator" />. </summary>
    </member>
    <member name="M:System.Web.Http.Validation.Providers.DataAnnotationsModelValidatorProvider.#ctor">
      <summary>Initializes a new instance of the <see cref="T:System.Web.Http.Validation.Providers.DataAnnotationsModelValidatorProvider" /> class.</summary>
    </member>
    <member name="M:System.Web.Http.Validation.Providers.DataAnnotationsModelValidatorProvider.GetValidators(System.Web.Http.Metadata.ModelMetadata,System.Collections.Generic.IEnumerable{System.Web.Http.Validation.ModelValidatorProvider},System.Collections.Generic.IEnumerable{System.Attribute})">
      <summary>Gets the validators for the model using the specified metadata, validator provider and attributes.</summary>
      <returns>The validators for the model.</returns>
      <param name="metadata">The metadata.</param>
      <param name="validatorProviders">The validator providers.</param>
      <param name="attributes">The attributes.</param>
    </member>
    <member name="M:System.Web.Http.Validation.Providers.DataAnnotationsModelValidatorProvider.RegisterAdapter(System.Type,System.Type)">
      <summary>Registers an adapter to provide client-side validation.</summary>
      <param name="attributeType">The type of the validation attribute.</param>
      <param name="adapterType">The type of the adapter.</param>
    </member>
    <member name="M:System.Web.Http.Validation.Providers.DataAnnotationsModelValidatorProvider.RegisterAdapterFactory(System.Type