 of the nested property.</param>
            <returns>An object representing the nested property.</returns>
        </member>
        <member name="M:System.Data.Entity.Infrastructure.DbComplexPropertyEntry`2.Property``1(System.Linq.Expressions.Expression{System.Func{`1,``0}})">
            <summary>
                Gets an object that represents a nested property of this property.
                This method can be used for both scalar or complex properties.
            </summary>
            <typeparam name = "TNestedProperty">The type of the nested property.</typeparam>
            <param name = "navigationProperty">An expression representing the nested property.</param>
            <returns>An object representing the nested property.</returns>
        </member>
        <member name="M:System.Data.Entity.Infrastructure.DbComplexPropertyEntry`2.ComplexProperty(System.String)">
            <summary>
                Gets an object that represents a nested complex property of this property.
            </summary>
            <param name = "propertyName">The name of the nested property.</param>
            <returns>An object representing the nested property.</returns>
        </member>
        <member name="M:System.Data.Entity.Infrastructure.DbComplexPropertyEntry`2.ComplexProperty``1(System.String)">
            <summary>
                Gets an object that represents a nested complex property of this property.
            </summary>
            <typeparam name = "TNestedComplexProperty">The type of the nested property.</typeparam>
            <param name = "propertyName">The name of the nested property.</param>
            <returns>An object representing the nested property.</returns>
        </member>
        <member name="M:System.Data.Entity.Infrastructure.DbComplexPropertyEntry`2.ComplexProperty``1(System.Linq.Expressions.Expression{System.Func{`1,``0}})">
         