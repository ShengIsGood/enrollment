ts the internal query.
            </summary>
            <value>The internal query.</value>
        </member>
        <member name="P:System.Data.Entity.Infrastructure.DbSqlQuery`1.System#ComponentModel#IListSource#ContainsListCollection">
            <summary>
                Returns <c>false</c>.
            </summary>
            <returns><c>false</c>.</returns>
        </member>
        <member name="T:System.Data.Entity.Infrastructure.DbUpdateConcurrencyException">
            <summary>
                Exception thrown by <see cref="T:System.Data.Entity.DbContext"/> when it was expected that SaveChanges for an entity would
                result in a database update but in fact no rows in the database were affected.  This usually indicates
                that the database has been concurrently updated such that a concurrency token that was expected to match
                did not actually match.
                Note that state entries referenced by this exception are not serialized due to security and accesses to
                the state entries after serialization will return null.
            </summary>
        </member>
        <!-- Badly formed XML comment ignored for member "T:System.Data.Entity.Infrastructure.DbUpdateException" -->
        <member name="M:System.Data.Entity.Infrastructure.DbUpdateException.#ctor(System.Data.Entity.Internal.InternalContext,System.Data.UpdateException,System.Boolean)">
            <summary>
                Initializes a new instance of the <see cref="T:System.Data.Entity.Infrastructure.DbUpdateException"/> class.
            </summary>
            <param name="internalContext">The internal context.</param>
            <param name="innerException">The inner exception.</param>
        </member>
        <member name="M:System.Data.Entity.Infrastructure.DbUpdateException.#ctor">
            <summary>
                In