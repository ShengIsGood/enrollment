columns it targets.
            </summary>
            <param name = "table">
                The name of the table to drop the index from.
                Schema name is optional, if no schema is specified then dbo is assumed.
            </param>
            <param name = "columns">The name of the column(s) the index targets.</param>
            <param name = "anonymousArguments">
                Additional arguments that may be processed by providers. 
                Use anonymous type syntax to specify arguments e.g. 'new { SampleArgument = "MyValue" }'.
            </param>
        </member>
        <member name="M:System.Data.Entity.Migrations.DbMigration.Sql(System.String,System.Boolean,System.Object)">
            <summary>
                Adds an operation to execute a SQL command.
            </summary>
            <param name = "sql">The SQL to be executed.</param>
    