         Gets a value indicating whether the entity has been loaded from the database.
            </summary>
            <value><c>true</c> if the entity is loaded; otherwise, <c>false</c>.</value>
        </member>
        <member name="P:System.Data.Entity.Infrastructure.DbReferenceEntry.EntityEntry">
            <summary>
                The <see cref="T:System.Data.Entity.Infrastructure.DbEntityEntry"/> to which this navigation property belongs.
            </summary>
            <value>An entry for the entity that owns this navigation property.</value>
        </member>
        <member name="P:System.Data.Entity.Infrastructure.DbReferenceEntry.InternalMemberEntry">
            <summary>
                Gets the <see cref="T:System.Data.Entity.Internal.InternalReferenceEntry"/> backing this object as an <see cref="P:System.Data.Entity.Infrastructure.DbReferenceEntry.InternalMemberEntry"/>.
            </summary>
            <value>The internal member entry.</value>
        </member>
        <member name="T:System.Data.Entity.Infrastructure.DbReferenceEntry`2">
            <summary>
                Instances of this class are returned from the Reference method of
                <see cref="T:System.Data.Entity.Infrastructure.DbEntityEntry`1"/> and allow operations such as loading to
                be performed on the an entity's reference navigation properties.
            </summary>
            <typeparam name="TEntity">The type of the entity to which this property belongs.</typeparam>
            <typeparam name="TProperty">The type of the property.</typeparam>
        </member>
        <member name="M:System.Data.Entity.Infrastructure.DbReferenceEntry`2.Create(System.Data.Entity.Internal.InternalReferenceEntry)">
            <summary>
                Creates a <see cref="T:System.Data.Entity.Infrastructure.DbReferenceEntry`2"/> from information in the given <see cref="T:System.Data.Entity.Internal.InternalReferenceEntry"/>.
                Use this method in preference to the constructor since it may potentially create a subclass depending on
                the type of member represented by the InternalCollectionEntry instance.
            </summary>
            <param name="internalReferenceEntry">The internal reference entry.</param>
            <returns>The new entry.</returns>
        </member>
        <member name="M:System.Data.Entity.Infrastructure.DbReferenceEntry`2.#ctor(System.Data.Entity.Internal.InternalReferenceEntry)">
            <summary>
                Initializes a new instance of the <see cref="T:System.Data.Entity.Infrastructure.DbReferenceEntry`2"/> class.
            </summary>
            <param name="internalReferenceEntry">The internal entry.</param>
        </member>
        <member name="M:System.Data.Entity.Infrastructure.DbReferenceEntry`2.Load">
            <summary>
                Loads the entity from the database.
                Note that if the entity already exists in the context, then it will not overwritten with values from the database.
            </summary>
        </member>
        <member name="M:System.Data.Entity.Infrastr