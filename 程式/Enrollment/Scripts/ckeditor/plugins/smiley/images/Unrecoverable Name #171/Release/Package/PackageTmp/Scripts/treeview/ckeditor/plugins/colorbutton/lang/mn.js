ntity.Migrations.Infrastructure.MigratorBase.Update">
            <summary>
                Updates the target database to the latest migration.
            </summary>
        </member>
        <member name="M:System.Data.Entity.Migrations.Infrastructure.MigratorBase.Update(System.String)">
            <summary>
                Updates the target database to a given migration.
            </summary>
            <param name = "targetMigration">The migration to upgrade/downgrade to.</param>
        </member>
        <member name="M:System.Data.Entity.Migrations.Infrastructure.MigratorBase.GetLocalMigrations">
            <summary>
                Gets a list of the migrations that are defined in the assembly.
            </summary>
            <returns>List of migration Ids</returns>
        </member>
        <member name="M:System.Data.Entity.Migrations.Infrastructure.MigratorBase.GetDatabaseMigrations">
            <summary>
                Gets a list of the migrations that have been applied to the data