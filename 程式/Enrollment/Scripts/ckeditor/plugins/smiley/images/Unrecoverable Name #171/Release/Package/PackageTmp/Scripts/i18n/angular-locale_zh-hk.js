       The entity will be in the Unchanged state after calling this method.
            </summary>
        </member>
        <member name="M:System.Data.Entity.Infrastructure.DbEntityEntry.Reference(System.String)">
            <summary>
                Gets an object that represents the reference (i.e. non-collection) navigation property from this
                entity to another entity.
            </summary>
            <param name = "navigationProperty">The name of the navigation property.</param>
            <returns>An object representing the navigation property.</returns>
        </member>
        <member name="M:System.Data.Entity.Infrastructure.DbEntityEntry.Collection(System.String)">
            <summary>
                Gets an object that represents the collection navigation property from this
                entity to a collection of related entities.
            </summary>
            <param name = "navigationProperty">The name of the navigation property.</param>
            <returns>An object representing the navigation property.</returns>
        </member>
        <member name="M:System.Data.Entity.Infrastructure.DbEntityEntry.Property(System.String)">
            <summary>
                Gets an object that represents a scalar or complex property of this entity.
            </summary>
            <param name = "propertyName">The name of the property.</param>
            <returns>An object representing the property.</returns>
        </member>
        <member name="M:System.Data.Entity.Infrastructure.DbEntityEntry.ComplexProperty(System.String)">
            <summary>
                Gets an object that represents a complex property of this entity.
            </summary>
            <param name = "propertyName">The name of the complex property.</param>
            <returns>An object representing the complex property.</returns>
        </member>
        <member name="M:System.Data.Entity.Infrastructure.DbEntityEntry.Member(System.String)">
            <summary>
                Gets an object that represents a member of the entity.  The runtime type of the returned object will
 