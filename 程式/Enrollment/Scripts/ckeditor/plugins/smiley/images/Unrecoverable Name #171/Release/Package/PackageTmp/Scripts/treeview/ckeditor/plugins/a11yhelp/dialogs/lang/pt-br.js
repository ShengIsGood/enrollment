    <param name="query">The original query results.</param>
    </member>
    <member name="M:System.Web.Http.QueryableAttribute.OnActionExecuted(System.Web.Http.Filters.HttpActionExecutedContext)">
      <summary>Called by the Web API framework after the action method executes.</summary>
      <param name="actionExecutedContext">The filter context.</param>
    </member>
    <member name="M:System.Web.Http.QueryableAttribute.OnActionExecuting(System.Web.Http.Controllers.HttpActionContext)">
      <summary>Called by the Web API framework before the action method executes.</summary>
      <param name="actionContext">The filter context.</param>
    </member>
    <member name="P:System.Web.Http.QueryableAttribute.ResultLimit">
      <summary>The maximum number of results that should be returned from this query regardless of query-specified limits.</summary>
      <returns>The maximum number of results that should be returned. A value of zero indicates no limit.</returns>
    </member>
    <member name="P:System.Web.Http.QueryableAttribute.StructuredQueryBuilder">
      <summary> The <see cref="T:System.Web.Http.Query.IStructuredQueryBuilder" /> to use. Derived classes can use this to have a per-attribute query builder  instead of the one on <see cref="T:System.Web.Http.HttpConfiguration" /></summary>
    </member>
    <member name="T:System.Web.Http.RouteParameter">
      <summary>The <see cref="T:System.Web.Http.RouteParameter" /> class can be used to indicate properties about a route parameter (the literals and placeholders  located within segments of a <see cref="M:IHttpRoute.RouteTemplate" />).  It can for example be used to indicate that a route parameter is optional. </summary>
    </member>
    <member name="F:System.Web.Http.RouteParameter.Optional">
      <summary>An optional parameter.</summary>
    </member>
    <member name="M:System.Web.Http.RouteParameter.ToString">
      <summary>Returns a <see cref="T:System.String" /> that represents this instance.</summary>
      <returns>A <see cref="T:System.String" /> that represents this instance.</returns>
    </member>
    <member name="T:System.Web.Http.ServicesExtensions">
      <summary>Provides type-safe accessors for services obtained from a <see cref="T:System.Web.Http.Controllers.ServicesContainer" />object.</summary>
    </member>
    <member name="M:System.Web.Http.ServicesExtensions.GetActionInvoker(System.Web.Http.Controllers.ServicesContainer)">
      <summary>Gets the <see cref="T:System.Web.Http.Controllers.IHttpActionInvoker" /> service.</summary>
      <returns>Returns an <see cref="T:System.Web.Http.Controllers.IHttpActionInvoker" /> instance.</returns>
      <param name="services">The services container.</param>
    </member>
    <member name="M:System.Web.Http.ServicesExtensions.GetActionSelector(System.Web.Http.Controllers.ServicesContainer)">
      <summary>Gets the <see cref="T:System.Web.Http.Controllers.IHttpActionSelector" /> service.</summary>
      <returns>Returns an<see cref="T:System.Web.Http.Controllers.IHttpActionSelector" />instance.</returns>
      <param name="services">The services container.</param>
    </member>
    <member name="M:System.Web.Http.ServicesExtensions.GetActionValueBinder(System.Web.Http.Controllers.ServicesContainer)">
      <summary>Gets the <see cref="T:System.Web.Http.Controllers.IActionValueBinder" /> service.</summary>
      <returns>Returns an<see cref="T:System.Web.Http.Controllers.IActionValueBinder" />instance.</returns>
      <param name="services">The services container.</param>
    </member>
    <member name="M:System.Web.Http.ServicesExtensions.GetApiExplorer(System.Web.Http.Controllers.ServicesContainer)">
      <summary>Gets the <see cref="T:System.Web.Http.Description.IApiExplorer" /> service.</summary>
      <returns>Returns an <see cref="T:System.Web.Http.Description.IApiExplorer" />instance.</returns>
      <param name="services">The services container.</param>
    </member>
    <member name="M:System.Web.Http.ServicesExtensions.GetAssembliesResolver(System.Web.Http.Controllers.ServicesContainer)">
      <summary>Gets the <see cref="T:System.Web.Http.Dispatcher.IAssembliesResolver" /> service.</summary>
      <returns>Returns an<see cref="T:System.Web.Http.Dispatcher.IAssembliesResolver" />instance.</returns>
      <param name="services">The services container.</param>
    </member>
    <member name="M:System.Web.Http.ServicesExtensions.GetBodyModelValidator(System.Web.Http.Controllers.ServicesContainer)">
      <summary>Gets the <see cref="T:System.Web.Http.Validation.IBodyModelValidator" /> ser