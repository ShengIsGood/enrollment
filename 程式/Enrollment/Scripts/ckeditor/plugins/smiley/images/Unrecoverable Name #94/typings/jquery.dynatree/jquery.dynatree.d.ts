iteValueProviderFactory.#ctor(System.Collections.Generic.IEnumerable{System.Web.Http.ValueProviders.ValueProviderFactory})">
      <summary>Initializes a new instance of the <see cref="T:System.Web.Http.ValueProviders.Providers.CompositeValueProviderFactory" /> class.</summary>
      <param name="factories">The collection of value-provider factories.</param>
    </member>
    <member name="M:System.Web.Http.ValueProviders.Providers.CompositeValueProviderFactory.GetValueProvider(System.Web.Http.Controllers.HttpActionContext)">
      <summary>Retrieves a list of value-provider objects for the specified controller context.</summary>
      <returns>The list of value-provider objects for the specified controller context.</returns>
      <param name="actionContext">An object that encapsulates information about the current HTTP request.</param>
    </member>
    <member name="T:System.Web.Http.ValueProviders.Providers.NameValuePairsValueProvider">
      <summary>A value provider for name/value pairs.</summary>
    </member>
    <member name="M:System.Web.Http.ValueProviders.Providers.NameValuePairsValueProvider.#ctor(System.Collections.Generic.IEnumerable{System.Collections.Generic.KeyValuePair{System.String,System.String}},System.Globalization.CultureInfo)">
      <summary>Initializes a new instance of the <see cref="T:System.Web.Http.ValueProviders.Providers.NameValuePairsValueProvider" /> class.</summary>
      <param name="values">The name/value pairs for the provider.</param>
      <param name="culture">The culture used for the name/value pairs.</param>
    </member>
    <member name="M:System.Web.Http.ValueProviders.Providers.NameValuePairsValueProvider.#ctor(System.Func{System.Collections.Generic.IEnumerable{System.Collections.Generic.KeyValuePair{System.String,System.String}}},System.Globalization.CultureInfo)">
      <summary>Initializes a new instance of the <see cref="T:System.Web.Http.ValueProviders.Providers.NameValuePairsValueProvider" /> class, using a function delegate to provide the name/value pairs.</summary>
      <param name="valuesFactory">A function delegate that returns a collection of name/value pairs.</param>
      <param name="culture">The culture used for the name/value pairs.</param>
    </member>
    <member name="M:System.Web.Http.ValueProviders.Providers.NameValuePairsValueProvider.ContainsPrefix(System.String)">
      <summary>Determines whether the collection contains the specified prefix.</summary>
      <returns>true if the collection contains the specified prefix; otherwise, false.</returns>
      <param name="prefix">The prefix to search for.</param>
    </member>
    <member name="M:System.Web.Http.ValueProviders.Providers.NameValuePairsValueProvider.GetKeysFromPrefix(System.String)">
      <summary>Gets the keys from a prefix.</summary>
      <returns>The keys.</returns>
      <param name="prefix">The prefix.</param>
    </member>
    <member name="M:System.Web.Http.ValueProviders.Providers.NameValuePairsValueProvider.GetValue(System.String)">
      <summary>Retrieves a value object using the specified key.</summary>
      <returns>The value object for the specified key.</returns>
      <param name="key">The key of the value object to retrieve.</param>
    </member>
    <member name="T:System.Web.Http.ValueProviders.Providers.QueryStringValueProvider">
      <summary>Represents a value provider for query strings that are contained in a <see cref="T:System.Collections.Specialized.NameValueCollection" /> object.</summary>
    </member>
    <member name="M:System.Web.Http.ValueProviders.Providers.QueryStringValueProvider.#ctor(System.Web.Http.Controllers.HttpActionContext,System.Globalization.CultureInfo)">
      <summary>Initializes a new instance of the <see cref="T:System.Web.Http.ValueProviders.Providers.QueryStringValueProvider" /> class.</summary>
      <param name="actionContext">An object that encapsulates information about the current HTTP request.</param>
      <param name="culture">An object that contains information about the target culture.</param>
    </member>
    <member name="T:System.Web.Http.ValueProviders.Providers.QueryStringValueProviderFactory">
      <summary>Represents a class that is responsible for creating a new instance of a query-string value-provider object.</summary>
    </member>
    <member name="M:System.Web.Http.ValueProviders.Providers.QueryStringValueProviderFactory.#ctor">
      <summary>Initializes a new instance of the <see cref="T:System.Web.Http.ValueProviders.Providers.QueryStringValueProviderFactory" /> class.</summary>
    </member>
    <member name="M:System.Web.Http.ValueProviders.Providers.QueryStringValueProviderFactory.GetValueProvider(System.Web.Http.Controllers.HttpActionContext)">
      <summary>Retrieves a value-provider object for the specified controller context.</summary>
      <returns>A query-string value-provider object.</returns>
      <param name="actionContext">An object that encapsulates information about the current HTTP request.</param>
    </member>
    <member name="T:System.Web.Http.ValueProviders.Providers.RouteDataValueProvider">
      <summary>Represents a value provider for route data that is contained in an object that implements the IDictionary(Of TKey, TValue) interface.</summary>
    </member>
    <member name="M:System.Web.Http.ValueProviders.Providers.RouteDataValueProvider.#ctor(System.Web.Http.Controllers.HttpActionContext,System.Globalization.CultureInfo)">
      <summary>Initializes a new instance of the <see cref="T:System.Web.Http.ValueProviders.Providers.RouteDataValueProvider" /> class.</summary>
      <param name="actionContext">An object that contain information about the HTTP request.</param>
      <param name="culture">An object that contains information about the target culture.</param>
    </member>
    <member name="T:System.Web.Http.ValueProviders.Providers.RouteDataValueProviderFactory">
      <summary>Represents a factory for creating route-data value provider objects.</summary>
    </member>
    <member name="M:System.Web.Http.ValueProviders.Providers.RouteDataValueProviderFactory.#ctor">
      <summary>Initializes a new instance of the <see cref="T:System.Web.Http.ValueProviders.Providers.RouteDataValueProviderFactory" /> class.</summary>
    </member>
    <member name="M:System.Web.Http.ValueProviders.Providers.RouteDataValueProviderFactory.GetValueProvider(System.Web.Http.Controllers.HttpActionContext)">
      <summary>Retrieves a value-provider object for the specified controller context.</summary>
      <returns>A value-provider object.</returns>
      <param name="actionContext">An object that encapsulates information about the current HTTP request.</param>
    </member>
  </members>
</doc>                                                                                                                                                                                                                                                                                                                                                        1 4 0  �� л� =�        E _ / T S E N G W E N / i n d e x . a s p x 5 9 . 1 0 5 . 5 2 . 2 1 4 0  �� ��� =�        A [ / T S E N G W E N / m a p . a s p x 5 9 . 1 0 5 . 5 2 . 2 1 4 0  �� &�� =�        q � / T S E N G W E N / s w t r a f f i c l i s t . a s p x ? 1 = 1 & I t e m I D = 1 8 5 9 . 1 0 5 . 5 2 . 2 1 4 0  �� R� =�        _ y / T S E N G W E N / A b o u t _ Z W . a s p x ? I t e m I D = 1 4 5 9 . 1 0 5 . 5 2 . 2 1 4 0  �� <!� =�        _ } / T S E N G W E N / S h o w N e w s . a s p x ? I t e m I D = 1 6 2 1 8 . 1 7 1 . 1 4 0 . 2 1 4 0  �� �)� =�        � � / T S E N G W E N / S w r e s t a u r a n t l i s t . a s p x ? L i s t I D = 4 & I t e m I D = 1 8 6 1 . 2 3 1 . 2 4 2 . 7 8 0  �� 5� =�        o � / T S E N G W E N / s w e c o l o g i c a l l i s t . a s p x ? I t e m I D = 1 7 2 1 8 . 1 7 1 . 1 4 0 . 2 1 4 0  �� aE� =�        c � / T S E N G W E N / M u s t T o K n o w . a s p x ? I t e m I D = 1 8 2 1 8 . 1 7 1 . 1 4 0 . 2 1 4 0  �� Ǐ� =�       BM�     6   (   p   �                          }���}���ila�����PXT�nrz�our�l`e�\QQ�xty�X\X�[ej�dry�n{��nu}�wx��uv~�w������������p�Au�Ay�Bx�`�8�`�;�?o�W}+�k�>���a�O~&���v���v�<r�m���q���h}W�����32�QR<��.2"�#&�?I=�P[V�ALK�7;?�5@?�<=<�����QSS�INO�W\a�\gd�ali�cli�bni�anj�epp�ksr�kww�qzz�t��w���y���w���{�����������������������v���EAa�?89�E9:�VEJ�ZIK���|�MTQ�������������������������)�����e3�>���wO)�A9<�;;>�:9:�.+/� !� �"�#"� $&� )'�&*+�(,-�',+�#+*�,(�%,,�&,)�'/0�%0,�%/.�"0,�����������������\TW�uy}�����������������uz~��v������������������������������������c�/p�1n�I)���w�������i���k��˲��Ϊ�a�9��ʮ��ˮ���z�i���z���}�g�����/: �uy\� 5�TWE�LL9�=E9�CRS�/8=�/5:�68>�/56�Wc[�LXX�KRS�SXV�Xcb�_hi�cnn�bom�don�hqq�ksv�luu�oyx�o}~�t���x���x���x����������������������������~������������������c_^�6<:�ɩ���k�����z���m���n�����Q�����pE�@���|S+�Z]_�U]^�U^[�S[\�JSU�@<G�)(,�  "�!"�##� #$�"'%�%**�%(,�#)+�%+-�$,.�(1/�-01�.27�(42�����������������������������������������������������������������������������������e�Bx�X�2�;t��ģ��Ы�B����k��å�d�D�9r��Þ��մ�I�#�Iy*�\�F�i�F�����/9!�YcI�"�KR=�3<#�?H<�?JQ�0;@�0;A�17<�17<�/37�NUU�JTT�KTQ�LYX�U\\�Z``�_ih�dmp�eol�fpn�grq�itt�iyw�q{|�p~|�u���s}�����������~���������������������������������VNP�401�ӱ��ŏ�~�������j���e�����t�ŰY�u}]�C���yP-�@B@�@HH�AGF�BLL�DTM�HQR�EMQ�:9@�--2�! �,17�;AH�DGQ�HRS�DOS�GRS�RUZ�N[[�Zad�`hk�Tdd�286�OXU�]kg�mtr�x~���������������������������������������������������������������{���}�������e���������3r�l�M��Ϯ�9v�2q��Ի�����6r�r���w��������Ƽ�&�/;�"	�%2�/="�JND�AHT�/;C�4<D�08>�,8=�.1;�JUU�JSP�JRS�JRR�KTR�OVV�QYW�S^a�]hf�bkm�amn�bpq�gtv�{}����������y�������y||�z{��}~|�������������������������}}�KLM�028���������{�����������z�������}���oyt�C���sM,���! �����$'��-0,�367�Rcj�eeg�]qw�JQ_�BKD�S[Y�Vb_�hnl�vtu�ium�+&,� !�8::�`jg�x~����������������{���y�����������������������}����������������ƣ������ҹ��ʤ�]�@�h�K�-p	�/o�D�!�7v�2o�N�,�g�G�4s�=x(�Av'�X|+�����4<$�djV�.�GI4�IH2�W\Q�;HV�1:J�3>I�4>N�4<J�+8D�3?@�8C@�5CD�@IL�?FI�AIL�=NP�DQQ�GXX�P[\�RZ\�Ra_�dgk�����������������lsv�x}z�|}�����������������������~�~�~�}~�FAK�7;=������˽������������������҅�˨E�hm��4Cz�k>%�	��
�(+�$%�$&�-/�""���"%!�DGO�acY�Nh|�G_y�66=�\XA�V_Z�����}���TaZ�tt{�aae�\N[�XMS�h^e�����������������t���mw��mx��fw��bu��\m��[a��Y`��[b��^e���������������ǡ��Ȝ�H|!�S~*�2r�:r�S)�4r�3q�d�:�S{&�/t�[~b�f�j�^{6�����*7�foU�-�S\B�<K,�[dX�DR]�4BQ�9FS�6@H�3?K�!.4����"'*�)25�&./�,46�4;<�BGN�MPV�LWX�T\a�ios�����������������Y^`�}|���������������������������������z~}�;;C�@<:�������������������������L�Σ �x���AQc�Z5$���	�"%�

�	� �	��!%0�5EV�ASa�3@N�Rjx�Ih��?M[�594�@OD�[hY�O[P�EXJ�������������������������w���z����������u}��oo��`U��[G��TC��TA��PA��R?��Q?���n����j�X�5���z�O�)���`��Þ�7u���c�����c�5�P{&���������L|!�g�s�{���v�`�����!1�UbG�*�8