﻿$.extend({
    ComboBoxDefault: function (f) {
        var Select2Init = function () {
            $(document).find('.select2').each(function (i, obj) {
                if (!$(obj).data("select2")) {
                    //$(obj).css('width', '100%').select2({ allowClear: true });
                    $(obj).select2({ allowClear: true });
                }
            });
            if ($.isFunction(f)) {
                f();
            }
        };

        var Tasks = [];
        var url = '/ApiComboBox/GetData';
        $.each($('select'), function (i) {
            if ($(this).attr('DefaultType') == null) {
                return;
            }

            if ($(this).attr('Action') != null) {
                return;
            }

            var DefalutSelectVal = true;

            var BindValue = $(this).attr('BindValue');
            if ($(this).attr('DefalutSelectVal') != null && $(this).attr('DefalutSelectVal') == 'T') {
                DefalutSelectVal = false;
            }

            var $ddl = $(this);
            if ($(this).attr('PrevLink') == null) {
                var PrevLinkDom = $(this).attr('PrevLink');

                var _m = {
                    DefaultType: $(this).attr('DefaultType'),
                    CodeID: $(this).attr('CodeID')
                };
                var f = function (callback) {

                    $.ajax({
                        type: 'POST',
                        url: url,
                        contentType: 'application/json; charset=utf-8',
                        data: JSON.stringify(_m),
                        dataType: 'json',
                        success: function (data) {

                            $ddl.find('option').remove();


                            if (data.length > 0) {

                                if (DefalutSelectVal) {
                                    $ddl.append($('<option>請選擇</option>'));
                                }
                                $.each(data, function (i, item) {

                                    BindValue = BindValue == null ? "" : BindValue;
                                    var selected = item.ID.toString().toLowerCase() == BindValue.toLowerCase() ? 'selected' : '';
                                    $ddl.append($('<option ' + selected + '></option>').val(item.ID).text(item.Name));
                                });
                            }
                            callback(null, '');
                        }
                    });
                };
                Tasks.push(f);
            } else {
                var PrevLinkDom = $(this).attr('PrevLink');
                if ($(PrevLinkDom).is('select')) {
                    $(PrevLinkDom).on('change', function () {
                        if ($(PrevLinkDom).val() == null || $(PrevLinkDom).val() == '-1') {
                            return;
                        }
                        var _m = {
                            DefaultType: $ddl.attr('DefaultType'),
                            CodeID: $(PrevLinkDom).val()
                        };
                        
                        //var f = function (callback) {

                            $.ajax({
                                type: 'POST',
                                url: url,
                                contentType: 'application/json; charset=utf-8',
                                data: JSON.stringify(_m),
                                dataType: 'json',
                                success: function (data) {

                                    $ddl.find('option').remove();


                                    if (data.length > 0) {
                                        if (DefalutSelectVal) {
                                            $ddl.append($('<option>請選擇</option>'));
                                        }
                                        $.each(data, function (i, item) {

                                            BindValue = BindValue == null ? "" : BindValue;
                                            var selected = item.ID.toString().toLowerCase() == BindValue.toLowerCase() ? 'selected' : '';
                                            $ddl.append($('<option ' + selected + '></option>').val(item.ID).text(item.Name));
                                        });
                                        $ddl.trigger("change");
                                    } else {
                                        debugger
                                        $ddl.select2({ allowClear: true });
                                    }
                                }
                            });
                        //};
                        //Tasks.push(f);
                    });
                }
            }
        });

        async.series(Tasks, function (error, result) {
            Select2Init();
            //console.log(Tasks);
        });
    }
});

$.extend({
    ComboBoxModal: function (ele, f) {
        var Select2Init = function () {
            $(ele).find('.select2').each(function (i, obj) {
                if (!$(obj).data("select2")) {
                    $(obj).css('width', '100%').select2({ allowClear: true });
                }
            });
            if ($.isFunction(f)) {
                f($(ele));
            }
        };

        var Tasks = [];
        var url = '/ApiComboBox/GetData';
        $.each($(ele).find('.select2'), function (i) {
            if ($(this).attr('DefaultType') == null) {
                return;
            }

            if ($(this).attr('Action') != null) {
                return;
            }

            var DefalutSelectVal = true;

            var BindValue = $(this).attr('BindValue');

            if ($(this).attr('DefalutSelectVal') != null && $(this).attr('DefalutSelectVal') == 'T') {
                DefalutSelectVal = false;
            }

            var $ddl = $(this);

            var _m = {
                DefaultType: $(this).attr('DefaultType'),
                CodeID: $(this).attr('CodeID')
            };
            var f = function (callback) {
                $.ajax({
                    type: 'POST',
                    url: url,
                    contentType: 'application/json; charset=utf-8',
                    data: JSON.stringify(_m),
                    dataType: 'json',
                    success: function (data) {
                        $ddl.find('option').remove();
                        if (data.length > 0) {
                            if (DefalutSelectVal) {
                                $ddl.append($('<option></option>'));
                            }
                            $.each(data, function (i, item) {
                                BindValue = BindValue == null ? "" : BindValue;
                                var selected = item.ID.toString().toLowerCase() == BindValue.toLowerCase() ? 'selected' : '';
                                $ddl.append($('<option ' + selected + '></option>').val(item.ID).text(item.Name));
                            });
                        }
                        callback(null, '');
                    }
                });
            };
            Tasks.push(f);
        });

        async.series(Tasks, function (error, result) {
            Select2Init();
        });
    }
});