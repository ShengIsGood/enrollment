﻿$.extend({
    DatePickerDefault: function () {
        $.each($('.datepicker'), function (a, b) {
            var IsSingle = $(b).attr('mode') == "range" ? false : true;
            $(b).daterangepicker({
                singleDatePicker: IsSingle,
                showDropdowns: true,
                autoUpdateInput: false,
                locale: {
                    format: 'YYYY/MM/DD',
                    daysOfWeek: [
                        "日",
                        "一",
                        "二",
                        "三",
                        "四",
                        "五",
                        "六"
                    ],
                    monthNames: [
                        "一月",
                        "二月",
                        "三月",
                        "四月",
                        "五月",
                        "六月",
                        "七月",
                        "八月",
                        "九月",
                        "十月",
                        "十一月",
                        "十二月"
                    ]
                }
            });

            $(b).on('apply.daterangepicker', function (ev, picker) {
               
                if (IsSingle) {
                    $(this).val(picker.startDate.format('YYYY/MM/DD'));
                }
                else {
                    $(this).val(picker.startDate.format('YYYY/MM/DD') + ' - ' + picker.endDate.format('YYYY/MM/DD'));
                }
            });

            $(b).on('cancel.daterangepicker', function (ev, picker) {
                $(this).val('');
            });
        });
        $.each($('.datetimepicker'), function (a, b) {
            $(b).daterangepicker({
                singleDatePicker: true,
                showDropdowns: true,
                autoUpdateInput: false,
                timePicker: true,
                timePickerIncrement: 1, //時間的增量，單位為分鐘  
                timePicker12Hour: false, //是否使用12小時制來顯示時間  
                locale: {
                    format: 'YYYY/MM/DD HH:mm',
                    daysOfWeek: [
                        "日",
                        "一",
                        "二",
                        "三",
                        "四",
                        "五",
                        "六"
                    ],
                    monthNames: [
                        "一月",
                        "二月",
                        "三月",
                        "四月",
                        "五月",
                        "六月",
                        "七月",
                        "八月",
                        "九月",
                        "十月",
                        "十一月",
                        "十二月"
                    ]
                }
            });

            $(b).on('apply.daterangepicker', function (ev, picker) {


                $(this).val(picker.startDate.format('YYYY/MM/DD HH:mm'));

            });

            $(b).on('cancel.daterangepicker', function (ev, picker) {
                $(this).val('');
            });
        });
    }
});

$.extend({
    DatePickerModal: function (f) {
        $.each($(f).find('.datepicker'), function (a, b) {
            var IsSingle = $(b).attr('mode') == "range" ? false : true;
            $(b).daterangepicker({
                singleDatePicker: IsSingle,
                showDropdowns: true,
                autoUpdateInput: false,
                locale: {
                    format: 'YYYY/MM/DD',
                    daysOfWeek: [
                        "日",
                        "一",
                        "二",
                        "三",
                        "四",
                        "五",
                        "六"
                    ],
                    monthNames: [
                        "一月",
                        "二月",
                        "三月",
                        "四月",
                        "五月",
                        "六月",
                        "七月",
                        "八月",
                        "九月",
                        "十月",
                        "十一月",
                        "十二月"
                    ]
                }
            });

            $(b).on('apply.daterangepicker', function (ev, picker) {
                if (IsSingle) {
                    $(this).val(picker.startDate.format('YYYY/MM/DD'));
                }
                else {
                    $(this).val(picker.startDate.format('YYYY/MM/DD') + ' - ' + picker.endDate.format('YYYY/MM/DD'));
                }
            });

            $(b).on('cancel.daterangepicker', function (ev, picker) {
                $(this).val('');
            });
        });
    }
});


