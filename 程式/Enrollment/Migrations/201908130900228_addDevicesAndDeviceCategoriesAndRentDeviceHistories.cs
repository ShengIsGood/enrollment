namespace Enrollment.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addDevicesAndDeviceCategoriesAndRentDeviceHistories : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.RentDeviceHistories",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DeviceId = c.Int(),
                        ClassHistoryId = c.Int(),
                        RentName = c.String(maxLength: 10),
                        RentMobile = c.String(maxLength: 20),
                        IsReturn = c.Int(nullable: false),
                        ReturnDate = c.DateTime(),
                        Poster = c.String(maxLength: 20),
                        InitDate = c.DateTime(),
                        UpdateID = c.String(maxLength: 20),
                        UpdateDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Devices", t => t.DeviceId)
                .ForeignKey("dbo.ClassHistories", t => t.ClassHistoryId)
                .Index(t => t.DeviceId)
                .Index(t => t.ClassHistoryId);
            
            CreateTable(
                "dbo.Devices",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DeviceId = c.String(nullable: false, maxLength: 20),
                        DeviceCategoryId = c.Int(),
                        Subject = c.String(nullable: false, maxLength: 50),
                        DeviceModel = c.String(maxLength: 100),
                        DeviceBrand = c.String(maxLength: 100),
                        Price = c.Int(nullable: false),
                        DeviceStatus = c.Int(nullable: false),
                        DeviceRentStatus = c.Int(nullable: false),
                        DeviceInfo = c.String(),
                        WarrantyDate = c.DateTime(),
                        BuyDate = c.DateTime(nullable: false),
                        DecommissioningDate = c.DateTime(),
                        Memo = c.String(maxLength: 200),
                        Poster = c.String(maxLength: 20),
                        InitDate = c.DateTime(),
                        UpdateID = c.String(maxLength: 20),
                        UpdateDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.DeviceCategories", t => t.DeviceCategoryId)
                .Index(t => t.DeviceCategoryId);
            
            CreateTable(
                "dbo.DeviceCategories",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ParentId = c.Int(),
                        Subject = c.String(nullable: false, maxLength: 200),
                        SortNum = c.Int(nullable: false),
                        Poster = c.String(maxLength: 20),
                        InitDate = c.DateTime(),
                        UpdateID = c.String(maxLength: 20),
                        UpdateDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.DeviceCategories", t => t.ParentId)
                .Index(t => t.ParentId);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.DeviceCategories", new[] { "ParentId" });
            DropIndex("dbo.Devices", new[] { "DeviceCategoryId" });
            DropIndex("dbo.RentDeviceHistories", new[] { "ClassHistoryId" });
            DropIndex("dbo.RentDeviceHistories", new[] { "DeviceId" });
            DropForeignKey("dbo.DeviceCategories", "ParentId", "dbo.DeviceCategories");
            DropForeignKey("dbo.Devices", "DeviceCategoryId", "dbo.DeviceCategories");
            DropForeignKey("dbo.RentDeviceHistories", "ClassHistoryId", "dbo.ClassHistories");
            DropForeignKey("dbo.RentDeviceHistories", "DeviceId", "dbo.Devices");
            DropTable("dbo.DeviceCategories");
            DropTable("dbo.Devices");
            DropTable("dbo.RentDeviceHistories");
        }
    }
}
