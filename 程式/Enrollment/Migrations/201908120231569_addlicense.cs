namespace Enrollment.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addlicense : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Licenses",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        LicenseId = c.String(maxLength: 50),
                        LicenseCategory = c.String(maxLength: 50),
                        LicenseUnit = c.String(maxLength: 50),
                        ChineseName = c.String(maxLength: 50),
                        EnglishName = c.String(maxLength: 50),
                        IssueDate = c.DateTime(),
                        EffectiveDate = c.DateTime(),
                        TeacherFK = c.Int(),
                        StudentFK = c.Int(),
                        Poster = c.String(maxLength: 20),
                        InitDate = c.DateTime(),
                        UpdateID = c.String(maxLength: 20),
                        UpdateDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Teachers", t => t.TeacherFK)
                .ForeignKey("dbo.Students", t => t.StudentFK)
                .Index(t => t.TeacherFK)
                .Index(t => t.StudentFK);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.Licenses", new[] { "StudentFK" });
            DropIndex("dbo.Licenses", new[] { "TeacherFK" });
            DropForeignKey("dbo.Licenses", "StudentFK", "dbo.Students");
            DropForeignKey("dbo.Licenses", "TeacherFK", "dbo.Teachers");
            DropTable("dbo.Licenses");
        }
    }
}
