namespace Enrollment.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addLenderInRentDeviceHistory : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.RentDeviceHistories", "Lender", c => c.String(maxLength: 10));
        }
        
        public override void Down()
        {
            DropColumn("dbo.RentDeviceHistories", "Lender");
        }
    }
}
