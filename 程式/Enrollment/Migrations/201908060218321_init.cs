namespace Enrollment.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Units",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Subject = c.String(nullable: false, maxLength: 100),
                        Alias = c.String(maxLength: 50),
                        ListNum = c.Int(nullable: false),
                        Poster = c.String(maxLength: 20),
                        InitDate = c.DateTime(),
                        UpdateID = c.String(maxLength: 20),
                        UpdateDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Members",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Account = c.String(nullable: false, maxLength: 50),
                        Password = c.String(nullable: false, maxLength: 100),
                        PasswordSalt = c.String(maxLength: 100),
                        Name = c.String(nullable: false, maxLength: 50),
                        IDNum = c.String(nullable: false, maxLength: 10),
                        Mobile = c.String(maxLength: 50),
                        Memo = c.String(),
                        Gender = c.Int(nullable: false),
                        Email = c.String(nullable: false, maxLength: 200),
                        MyPic = c.String(maxLength: 50),
                        JobTitle = c.String(maxLength: 50),
                        Address = c.String(maxLength: 200),
                        UnitId = c.Int(nullable: false),
                        Permission = c.String(maxLength: 500),
                        Poster = c.String(maxLength: 20),
                        InitDate = c.DateTime(),
                        UpdateID = c.String(maxLength: 20),
                        UpdateDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Units", t => t.UnitId, cascadeDelete: true)
                .Index(t => t.UnitId);
            
            CreateTable(
                "dbo.Roles",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Subject = c.String(nullable: false, maxLength: 100),
                        Permission = c.String(),
                        Alias = c.String(maxLength: 50),
                        Poster = c.String(maxLength: 20),
                        InitDate = c.DateTime(),
                        UpdateID = c.String(maxLength: 20),
                        UpdateDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.RoleMenus",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        RoleId = c.Int(nullable: false),
                        MenuId = c.Int(nullable: false),
                        Poster = c.String(maxLength: 20),
                        InitDate = c.DateTime(),
                        UpdateID = c.String(maxLength: 20),
                        UpdateDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Roles", t => t.RoleId, cascadeDelete: true)
                .ForeignKey("dbo.Menus", t => t.MenuId, cascadeDelete: true)
                .Index(t => t.RoleId)
                .Index(t => t.MenuId);
            
            CreateTable(
                "dbo.Menus",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Subject = c.String(nullable: false, maxLength: 100),
                        PrivilegeCode = c.String(nullable: false, maxLength: 10),
                        HaveDetails = c.Int(nullable: false),
                        TopMenuId = c.Int(),
                        IsPlugIn = c.Int(nullable: false),
                        ControllerName = c.String(maxLength: 100),
                        ActionName = c.String(maxLength: 100),
                        Url = c.String(maxLength: 200),
                        LoginUrl = c.String(maxLength: 200),
                        IsWork = c.Int(nullable: false),
                        SortNum = c.Int(nullable: false),
                        Poster = c.String(maxLength: 20),
                        InitDate = c.DateTime(),
                        UpdateID = c.String(maxLength: 20),
                        UpdateDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Menus", t => t.TopMenuId)
                .Index(t => t.TopMenuId);
            
            CreateTable(
                "dbo.ComGroups",
                c => new
                    {
                        GroupID = c.String(nullable: false, maxLength: 50, unicode: false),
                        GroupName = c.String(maxLength: 100),
                        GroupType = c.String(maxLength: 2, unicode: false),
                        SysType = c.String(maxLength: 2, unicode: false),
                        Poster = c.String(maxLength: 20),
                        InitDate = c.DateTime(),
                        UpdateID = c.String(maxLength: 20),
                        UpdateDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.GroupID);
            
            CreateTable(
                "dbo.ComCodes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CodeID = c.String(nullable: false, maxLength: 50, unicode: false),
                        GroupID = c.String(nullable: false, maxLength: 50, unicode: false),
                        CodeName1 = c.String(maxLength: 100, unicode: false),
                        CodeName2 = c.String(maxLength: 100, unicode: false),
                        CodeName3 = c.String(maxLength: 100, unicode: false),
                        SortIndex = c.Int(nullable: false),
                        Enabled = c.Int(nullable: false),
                        Memo = c.String(unicode: false, storeType: "text"),
                        Poster = c.String(maxLength: 20),
                        InitDate = c.DateTime(),
                        UpdateID = c.String(maxLength: 20),
                        UpdateDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ComGroups", t => t.GroupID, cascadeDelete: true)
                .Index(t => t.GroupID);
            
            CreateTable(
                "dbo.courses",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CourseNumber = c.String(maxLength: 20),
                        CourseName = c.String(maxLength: 20),
                        CourseSummary = c.String(maxLength: 200),
                        TrainingProgram = c.String(maxLength: 20),
                        TrainingContent = c.String(),
                        UploadMaterials = c.String(maxLength: 50),
                        Memo = c.String(maxLength: 100),
                        Poster = c.String(maxLength: 20),
                        InitDate = c.DateTime(),
                        UpdateID = c.String(maxLength: 20),
                        UpdateDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.CourseAssociations",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CourseFK = c.Int(nullable: false),
                        FieldFK = c.Int(nullable: false),
                        Poster = c.String(maxLength: 20),
                        InitDate = c.DateTime(),
                        UpdateID = c.String(maxLength: 20),
                        UpdateDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.courses", t => t.CourseFK, cascadeDelete: true)
                .ForeignKey("dbo.Fields", t => t.FieldFK, cascadeDelete: true)
                .Index(t => t.CourseFK)
                .Index(t => t.FieldFK);
            
            CreateTable(
                "dbo.Fields",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FieldName = c.String(maxLength: 50),
                        Poster = c.String(maxLength: 20),
                        InitDate = c.DateTime(),
                        UpdateID = c.String(maxLength: 20),
                        UpdateDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.TeacherAssociations",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TeacherFK = c.Int(nullable: false),
                        FieldFK = c.Int(nullable: false),
                        Poster = c.String(maxLength: 20),
                        InitDate = c.DateTime(),
                        UpdateID = c.String(maxLength: 20),
                        UpdateDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Teachers", t => t.TeacherFK, cascadeDelete: true)
                .ForeignKey("dbo.Fields", t => t.FieldFK, cascadeDelete: true)
                .Index(t => t.TeacherFK)
                .Index(t => t.FieldFK);
            
            CreateTable(
                "dbo.Teachers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 50),
                        Photo = c.String(maxLength: 50),
                        Education = c.String(maxLength: 50),
                        Email = c.String(nullable: false, maxLength: 200),
                        Experience = c.String(maxLength: 200),
                        Resume = c.String(maxLength: 50),
                        Poster = c.String(maxLength: 20),
                        InitDate = c.DateTime(),
                        UpdateID = c.String(maxLength: 20),
                        UpdateDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ClassTeachers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CourseId = c.Int(nullable: false),
                        TeacherId = c.Int(nullable: false),
                        PlaceId = c.Int(nullable: false),
                        MinStudents = c.Int(nullable: false),
                        MaxStudents = c.Int(nullable: false),
                        ProvideDormitory = c.Int(nullable: false),
                        DormRoom = c.String(maxLength: 100),
                        ClassCycle = c.Int(nullable: false),
                        StarDateTime = c.DateTime(nullable: false),
                        EndDateTime = c.DateTime(nullable: false),
                        AnnouncementStarDateTime = c.DateTime(nullable: false),
                        AnnouncementEndDateTime = c.DateTime(nullable: false),
                        ClassStatus = c.Int(nullable: false),
                        Poster = c.String(maxLength: 20),
                        InitDate = c.DateTime(),
                        UpdateID = c.String(maxLength: 20),
                        UpdateDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.courses", t => t.CourseId, cascadeDelete: true)
                .ForeignKey("dbo.Teachers", t => t.TeacherId, cascadeDelete: true)
                .ForeignKey("dbo.Places", t => t.PlaceId, cascadeDelete: true)
                .Index(t => t.CourseId)
                .Index(t => t.TeacherId)
                .Index(t => t.PlaceId);
            
            CreateTable(
                "dbo.Places",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PlaceNumber = c.String(nullable: false, maxLength: 50),
                        category = c.Int(nullable: false),
                        PlaceName = c.String(nullable: false, maxLength: 50),
                        Location = c.String(maxLength: 50),
                        Numberlimit = c.Int(nullable: false),
                        Enable = c.Int(nullable: false),
                        Poster = c.String(maxLength: 20),
                        InitDate = c.DateTime(),
                        UpdateID = c.String(maxLength: 20),
                        UpdateDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ClassTimes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ClassTeacherId = c.Int(nullable: false),
                        Date = c.DateTime(),
                        Day = c.String(maxLength: 1),
                        StarTime = c.String(maxLength: 5),
                        EndTime = c.String(maxLength: 5),
                        Poster = c.String(maxLength: 20),
                        InitDate = c.DateTime(),
                        UpdateID = c.String(maxLength: 20),
                        UpdateDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ClassTeachers", t => t.ClassTeacherId, cascadeDelete: true)
                .Index(t => t.ClassTeacherId);
            
            CreateTable(
                "dbo.ClassStudents",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ClassTeacherId = c.Int(nullable: false),
                        StudentId = c.Int(nullable: false),
                        ClassStatus = c.Int(nullable: false),
                        PaymentStatus = c.Int(nullable: false),
                        NeedRoom = c.Int(nullable: false),
                        Poster = c.String(maxLength: 20),
                        InitDate = c.DateTime(),
                        UpdateID = c.String(maxLength: 20),
                        UpdateDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ClassTeachers", t => t.ClassTeacherId, cascadeDelete: true)
                .ForeignKey("dbo.Students", t => t.StudentId, cascadeDelete: true)
                .Index(t => t.ClassTeacherId)
                .Index(t => t.StudentId);
            
            CreateTable(
                "dbo.Students",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Account = c.String(nullable: false, maxLength: 50),
                        Password = c.String(nullable: false, maxLength: 100),
                        PasswordSalt = c.String(maxLength: 100),
                        Name = c.String(nullable: false, maxLength: 50),
                        IDNum = c.String(nullable: false, maxLength: 10),
                        Birthday = c.DateTime(),
                        Gender = c.Int(nullable: false),
                        Email = c.String(nullable: false, maxLength: 200),
                        Address = c.String(maxLength: 200),
                        WINDAID = c.String(maxLength: 100),
                        FileUrl = c.String(maxLength: 100),
                        Poster = c.String(maxLength: 20),
                        InitDate = c.DateTime(),
                        UpdateID = c.String(maxLength: 20),
                        UpdateDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Attendances",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ClassTeacherId = c.Int(nullable: false),
                        StudentId = c.Int(nullable: false),
                        ClassDate = c.DateTime(nullable: false),
                        Day = c.String(maxLength: 1),
                        StarTime = c.String(maxLength: 5),
                        EndTime = c.String(maxLength: 5),
                        AttendStatus = c.Int(),
                        IsLate = c.Int(nullable: false),
                        RealTime = c.String(maxLength: 5),
                        IsExcused = c.Int(nullable: false),
                        LeaveTime = c.String(maxLength: 5),
                        AskForLeaveStartTime = c.String(maxLength: 5),
                        AskForLeaveEndTime = c.String(maxLength: 5),
                        Memo = c.String(maxLength: 200),
                        Poster = c.String(maxLength: 20),
                        InitDate = c.DateTime(),
                        UpdateID = c.String(maxLength: 20),
                        UpdateDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ClassTeachers", t => t.ClassTeacherId, cascadeDelete: true)
                .ForeignKey("dbo.Students", t => t.StudentId, cascadeDelete: true)
                .Index(t => t.ClassTeacherId)
                .Index(t => t.StudentId);
            
            CreateTable(
                "dbo.Scores",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ScoreId = c.Int(),
                        ClassHistoryId = c.Int(),
                        StudentId = c.Int(),
                        ScoreSubject = c.String(maxLength: 50),
                        Fraction = c.Double(),
                        Poster = c.String(maxLength: 20),
                        InitDate = c.DateTime(),
                        UpdateID = c.String(maxLength: 20),
                        UpdateDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Scores", t => t.ScoreId)
                .ForeignKey("dbo.ClassHistories", t => t.ClassHistoryId)
                .ForeignKey("dbo.Students", t => t.StudentId)
                .Index(t => t.ScoreId)
                .Index(t => t.ClassHistoryId)
                .Index(t => t.StudentId);
            
            CreateTable(
                "dbo.ClassHistories",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ClassTeacherId = c.Int(nullable: false),
                        ClassDate = c.DateTime(nullable: false),
                        Day = c.String(maxLength: 1),
                        StarTime = c.String(maxLength: 5),
                        EndTime = c.String(maxLength: 5),
                        CourseContent = c.String(maxLength: 1000),
                        ClassStartTimes = c.String(maxLength: 5),
                        ClassEndTimes = c.String(maxLength: 5),
                        Memo = c.String(maxLength: 1000),
                        Poster = c.String(maxLength: 20),
                        InitDate = c.DateTime(),
                        UpdateID = c.String(maxLength: 20),
                        UpdateDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ClassTeachers", t => t.ClassTeacherId, cascadeDelete: true)
                .Index(t => t.ClassTeacherId);
            
            CreateTable(
                "dbo.RoleMembers",
                c => new
                    {
                        Role_Id = c.Int(nullable: false),
                        Member_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Role_Id, t.Member_Id })
                .ForeignKey("dbo.Roles", t => t.Role_Id, cascadeDelete: true)
                .ForeignKey("dbo.Members", t => t.Member_Id, cascadeDelete: true)
                .Index(t => t.Role_Id)
                .Index(t => t.Member_Id);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.RoleMembers", new[] { "Member_Id" });
            DropIndex("dbo.RoleMembers", new[] { "Role_Id" });
            DropIndex("dbo.ClassHistories", new[] { "ClassTeacherId" });
            DropIndex("dbo.Scores", new[] { "StudentId" });
            DropIndex("dbo.Scores", new[] { "ClassHistoryId" });
            DropIndex("dbo.Scores", new[] { "ScoreId" });
            DropIndex("dbo.Attendances", new[] { "StudentId" });
            DropIndex("dbo.Attendances", new[] { "ClassTeacherId" });
            DropIndex("dbo.ClassStudents", new[] { "StudentId" });
            DropIndex("dbo.ClassStudents", new[] { "ClassTeacherId" });
            DropIndex("dbo.ClassTimes", new[] { "ClassTeacherId" });
            DropIndex("dbo.ClassTeachers", new[] { "PlaceId" });
            DropIndex("dbo.ClassTeachers", new[] { "TeacherId" });
            DropIndex("dbo.ClassTeachers", new[] { "CourseId" });
            DropIndex("dbo.TeacherAssociations", new[] { "FieldFK" });
            DropIndex("dbo.TeacherAssociations", new[] { "TeacherFK" });
            DropIndex("dbo.CourseAssociations", new[] { "FieldFK" });
            DropIndex("dbo.CourseAssociations", new[] { "CourseFK" });
            DropIndex("dbo.ComCodes", new[] { "GroupID" });
            DropIndex("dbo.Menus", new[] { "TopMenuId" });
            DropIndex("dbo.RoleMenus", new[] { "MenuId" });
            DropIndex("dbo.RoleMenus", new[] { "RoleId" });
            DropIndex("dbo.Members", new[] { "UnitId" });
            DropForeignKey("dbo.RoleMembers", "Member_Id", "dbo.Members");
            DropForeignKey("dbo.RoleMembers", "Role_Id", "dbo.Roles");
            DropForeignKey("dbo.ClassHistories", "ClassTeacherId", "dbo.ClassTeachers");
            DropForeignKey("dbo.Scores", "StudentId", "dbo.Students");
            DropForeignKey("dbo.Scores", "ClassHistoryId", "dbo.ClassHistories");
            DropForeignKey("dbo.Scores", "ScoreId", "dbo.Scores");
            DropForeignKey("dbo.Attendances", "StudentId", "dbo.Students");
            DropForeignKey("dbo.Attendances", "ClassTeacherId", "dbo.ClassTeachers");
            DropForeignKey("dbo.ClassStudents", "StudentId", "dbo.Students");
            DropForeignKey("dbo.ClassStudents", "ClassTeacherId", "dbo.ClassTeachers");
            DropForeignKey("dbo.ClassTimes", "ClassTeacherId", "dbo.ClassTeachers");
            DropForeignKey("dbo.ClassTeachers", "PlaceId", "dbo.Places");
            DropForeignKey("dbo.ClassTeachers", "TeacherId", "dbo.Teachers");
            DropForeignKey("dbo.ClassTeachers", "CourseId", "dbo.courses");
            DropForeignKey("dbo.TeacherAssociations", "FieldFK", "dbo.Fields");
            DropForeignKey("dbo.TeacherAssociations", "TeacherFK", "dbo.Teachers");
            DropForeignKey("dbo.CourseAssociations", "FieldFK", "dbo.Fields");
            DropForeignKey("dbo.CourseAssociations", "CourseFK", "dbo.courses");
            DropForeignKey("dbo.ComCodes", "GroupID", "dbo.ComGroups");
            DropForeignKey("dbo.Menus", "TopMenuId", "dbo.Menus");
            DropForeignKey("dbo.RoleMenus", "MenuId", "dbo.Menus");
            DropForeignKey("dbo.RoleMenus", "RoleId", "dbo.Roles");
            DropForeignKey("dbo.Members", "UnitId", "dbo.Units");
            DropTable("dbo.RoleMembers");
            DropTable("dbo.ClassHistories");
            DropTable("dbo.Scores");
            DropTable("dbo.Attendances");
            DropTable("dbo.Students");
            DropTable("dbo.ClassStudents");
            DropTable("dbo.ClassTimes");
            DropTable("dbo.Places");
            DropTable("dbo.ClassTeachers");
            DropTable("dbo.Teachers");
            DropTable("dbo.TeacherAssociations");
            DropTable("dbo.Fields");
            DropTable("dbo.CourseAssociations");
            DropTable("dbo.courses");
            DropTable("dbo.ComCodes");
            DropTable("dbo.ComGroups");
            DropTable("dbo.Menus");
            DropTable("dbo.RoleMenus");
            DropTable("dbo.Roles");
            DropTable("dbo.Members");
            DropTable("dbo.Units");
        }
    }
}
