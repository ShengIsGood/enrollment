namespace Enrollment.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addDeviceUseAndDeviceUseContentInRentDeviceHistory : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.RentDeviceHistories", "DeviceUse", c => c.Int(nullable: false));
            AddColumn("dbo.RentDeviceHistories", "DeviceUseContent", c => c.String(maxLength: 200));
        }
        
        public override void Down()
        {
            DropColumn("dbo.RentDeviceHistories", "DeviceUseContent");
            DropColumn("dbo.RentDeviceHistories", "DeviceUse");
        }
    }
}
