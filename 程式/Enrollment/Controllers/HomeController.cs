﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Enrollment.Filters;
using Enrollment.Models;


namespace Enrollment.Controllers
{
    public class HomeController : Controller
    {
        private BackendContext db = new BackendContext();
        //
        // GET: /Admin/Home/

        [PermissionFilters]
        [Authorize]
        public ActionResult Index(string ReturnUrl)
        {
            if (Session["login"] == null)
            {
                Session["login"] = false;
            }

            if ((bool) Session["login"])
            {
                var member = db.Members.FirstOrDefault(x => x.Account == User.Identity.Name);
                string Permission = "," + member.Permission + ",";
                var menu = db.Menu.Where(x =>
                    !string.IsNullOrEmpty(x.LoginUrl) && Permission.Contains("," + x.PrivilegeCode + ",")).ToList();
                ViewBag.menuss = menu;
            }
            Session["login"] = false;



            if (!string.IsNullOrEmpty(ReturnUrl))
            {

                ViewBag.ReturnUrl = ReturnUrl;
               
            }
      
            return View();
        }
       
        [PermissionFilters]
        [Authorize]
        public ActionResult MyIndex()
        {
            
            return View();
        }

        /// <summary>
        /// 由內網管理後台登入的入口
        /// carl
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        [AllowAnonymous]
        public ActionResult Login(string code)
        {
            BackendContext db = new BackendContext();
            if (!string.IsNullOrEmpty(code))
            {

                //carl 改成md5的編碼方式
                string userAccout = Utility.DecodingByMD5(code);

                Member member = db.Members.FirstOrDefault(d => d.Account == userAccout);
                if (member != null)
                {
                    Utility.GetPerssionGlobal(member);
                    string userData = JsonConvert.SerializeObject(member);
                    Utility.SetAuthenTicket(userData, member.Account);
                    return RedirectToAction("Index", "Home");
                }
                ViewBag.message = "登入失敗!";
                return View();
            }
            ViewBag.message = "登入失敗!";
            return View();

        }

        private Member ValidateUser(string userName, string password)
        {
            Member member = db.Members.SingleOrDefault(o => o.Account == userName);
            if (member == null)
            {
                return null;
            }
            string saltPassword = Utility.GenerateHashWithSalt(password, member.PasswordSalt);
            return saltPassword == member.Password ? member : null;
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            FormsAuthentication.SignOut();
            Session.Abandon();

            return Redirect(FormsAuthentication.LoginUrl);


        }

      
        public ActionResult Error()
        {
            return View();
        }




    }
}
