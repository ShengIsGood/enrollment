﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using MvcPaging;
using System.Web.Mvc;
using Enrollment.Filters;
using Enrollment.Models;
using Newtonsoft.Json;

namespace Enrollment.Controllers
{
	[PermissionFilters]
    [Authorize]
    public class PlaceController : _BaseController
    {
        private BackendContext _db = new BackendContext();
        private const int DefaultPageSize = 15;
        //




        public ActionResult Index(int? page, FormCollection fc)
        {
            //記住搜尋條件
            GetCatcheRoutes(page, fc);

            //取得正確的頁面
            int currentPageIndex = getCurrentPage(page, fc);


            var place = _db.Place.Where(x => x.category == Category.訓練場地).OrderByDescending(p => p.InitDate).AsQueryable();
            if (hasViewData("SearchBycategory"))
            {
                string category = getViewDateStr("SearchBycategory");
                Enrollment.Models.Category searchBycategory = (Enrollment.Models.Category)Enum.Parse(typeof(Enrollment.Models.Category), category, false);

                place = place.Where(w => w.category == searchBycategory);
            }

            if (hasViewData("SearchByEnable"))
            {
                string Enable = getViewDateStr("SearchByEnable");
                Enrollment.Models.EnableType searchByEnable = (Enrollment.Models.EnableType)Enum.Parse(typeof(Enrollment.Models.EnableType), Enable, false);

                place = place.Where(w => w.Enable == searchByEnable);
            }



            return View(place.OrderByDescending(p => p.InitDate).ToPagedList(currentPageIndex, DefaultPageSize));

        }





        //
        // GET: /Place/Details/5

        public ActionResult Details(int id = 0)
        {
            Place place = _db.Place.Find(id);
            if (place == null)
            {
                //return HttpNotFound();
                return View();
            }
            return View(place);
        }

        //
        // GET: /Place/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Place/Create

        [HttpPost]
        [ValidateAntiForgeryToken]

        public ActionResult Create(Place place)
        {
            if (ModelState.IsValid)
            {

                _db.Place.Add(place);
                place.Create(_db, _db.Place);
                return RedirectToAction("Index");
            }

            return View(place);
        }

        //
        // GET: /Place/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Place place = _db.Place.Find(id);
            if (place == null)
            {
                return HttpNotFound();
            }
            return View(place);
        }

        //
        // POST: /Place/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]

        public ActionResult Edit(Place place)
        {
            if (ModelState.IsValid)
            {

                //_db.Entry(place).State = EntityState.Modified;
                place.Update();
                return RedirectToAction("Index", new { Page = -1 });
            }
            return View(place);
        }

        //
        // GET: /Place/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Place place = _db.Place.Find(id);
            if (place == null)
            {
                return HttpNotFound();
            }
            return View(place);
        }

        //
        // POST: /Place/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Place place = _db.Place.Find(id);
            _db.Place.Remove(place);
            _db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            _db.Dispose();
            base.Dispose(disposing);
        }
    }

}
