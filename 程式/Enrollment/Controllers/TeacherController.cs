﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web;
using MvcPaging;
using System.Web.Mvc;
using Enrollment.Filters;
using Enrollment.Models;

namespace Enrollment.Controllers
{
	[PermissionFilters]
    [Authorize]
    public class TeacherController : _BaseController
    {
        private BackendContext _db = new BackendContext();
        private const int DefaultPageSize = 15;
        //

        


        public ActionResult Index(int? page, FormCollection fc )
        {
			//記住搜尋條件
            GetCatcheRoutes(page, fc);

            //取得正確的頁面
            int currentPageIndex = getCurrentPage(page, fc);
            

            var teacher = _db.Teacher.OrderByDescending(p => p.InitDate).AsQueryable();
            if (hasViewData("SearchByName")) 
            { 
            string searchByName = getViewDateStr("SearchByName");             
                teacher = teacher.Where(w => w.Name.Contains(searchByName)); 
            } 
 

//            ViewBag.Name = Name;
            return View(teacher.OrderByDescending(p => p.InitDate).ToPagedList(currentPageIndex, DefaultPageSize));

        }



        

        //
        // GET: /Teacher/Details/5

        public ActionResult Details(int id = 0)
        {
            Teacher teacher = _db.Teacher.Find(id);
            if (teacher == null)
            {
                //return HttpNotFound();
				 return View();
            }
            return View(teacher);
        }

        //
        // GET: /Teacher/Create

        public ActionResult Create()
        {
            var field = _db.Field.ToList();
            if (field.Count > 0)
            {
                ViewBag.Showlist = true;
                ViewBag.fieldlist = new SelectList(field, "Id", "FieldName");
            }
            else
            {
                ViewBag.Showlist = false;
            }
            return View();
        }

        //
        // POST: /Teacher/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public ActionResult Create(Teacher teacher, int[] fieldlist, HttpPostedFileBase file, HttpPostedFileBase resume, List<int> checkaddbtn, List<License> Licenselist)
        {
            if (ModelState.IsValid)
            {
                if (resume != null)
                {
                    string extensionName = resume.FileName.Substring(resume.FileName.LastIndexOf('.') + 1).ToLower();

                    string FileName2 = DateTime.Now.ToString("yyyyMMdd") + teacher.Name + "." + extensionName;

                    string docupath2 = Request.PhysicalApplicationPath + "upfiles\\resume\\";

                    if (!Directory.Exists(docupath2))
                    {
                        Directory.CreateDirectory(@docupath2);
                    }

                    string filePath2 = Server.MapPath("~/upfiles/resume/");

                    resume.SaveAs(Path.Combine(filePath2, FileName2));

                    teacher.Resume = FileName2;

                }


                if (file!=null)
                {
                    string extensionName = file.FileName.Substring(file.FileName.LastIndexOf('.') + 1).ToLower();
  
                    string FileName = DateTime.Now.ToString("yyyyMMdd") + teacher.Name + "." + extensionName;
                    
                    string docupath = Request.PhysicalApplicationPath + "upfiles\\photo\\";
                 
                    if (!Directory.Exists(docupath))
                    {
                        Directory.CreateDirectory(@docupath);
                    }
                   
                    string filePath = Server.MapPath("~/upfiles/photo/");
                
                    file.SaveAs(Path.Combine(filePath, FileName));
          
                    teacher.Photo = FileName;
             
                }

                _db.Teacher.Add(teacher);
                teacher.Create(_db, _db.Teacher);
                TimeZoneInfo TPZone = TimeZoneInfo.FindSystemTimeZoneById("Taipei Standard Time");
                DateTime NOWTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, TPZone);
                foreach (var item in fieldlist)
                {
                    TeacherAssociation teacherAssociation = new TeacherAssociation();
                    teacherAssociation.TeacherFK = teacher.Id;
                    teacherAssociation.FieldFK = item;
                    teacherAssociation.Poster = User.Identity.Name;
                    teacherAssociation.InitDate = NOWTime;
                    _db.TeacherAssociation.Add(teacherAssociation);

                }

                _db.SaveChanges();

                if (Licenselist != null)
                {
                    int count = 0;
                    List<License> addLicense = new List<License>();
                    foreach (var item in Licenselist)
                    {
                        bool addcheck = true;
                        if (checkaddbtn != null)
                        {
                            foreach (var items in checkaddbtn)
                            {
                                if (count == items)
                                {
                                    addcheck = false;
                                    break;
                                }
                            }
                        }

                        if (addcheck)
                        {
                            item.TeacherFK = teacher.Id;
                            item.InitDate = teacher.InitDate;
                            item.Poster = teacher.Poster;
                            item.UpdateDate = teacher.InitDate;
                            item.UpdateID = teacher.Poster;

                            addLicense.Add(item);
                        }

                        count++;
                    }
                    foreach (var item in addLicense)
                    {
                        _db.Licenses.Add(item);
                    }

                    _db.SaveChanges();
                }




                return RedirectToAction("Index");
            }

            return View(teacher);
        }

        //
        // GET: /Teacher/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Teacher teacher = _db.Teacher.Find(id);
            if (teacher == null)
            {
                return HttpNotFound();
            }
            var field = _db.Field.ToList();
            if (field.Count > 0)
            {
                ViewBag.fieldlist = new SelectList(field, "Id", "FieldName");
            }

            string selectedlist = "";
            foreach (var item in teacher.TeacherAssociation)
            {
                selectedlist += item.FieldFK + ",";
            }

            ViewBag.selectedlist = selectedlist.TrimEnd(',');
            return View(teacher);
        }

        //
        // POST: /Teacher/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]

        public ActionResult Edit(Teacher teacher, int[] fieldlist, HttpPostedFileBase file, HttpPostedFileBase resume, List<License> Licenselist, List<License> updateLicenses, List<int> removeLicenses, List<int> checkaddbtn)
        {
            if (ModelState.IsValid)
            {

                if (resume != null)
                {
                    string extensionName = resume.FileName.Substring(resume.FileName.LastIndexOf('.') + 1).ToLower();

                    string FileName2 = DateTime.Now.ToString("yyyyMMdd") + teacher.Name + "." + extensionName;

                    string docupath2 = Request.PhysicalApplicationPath + "upfiles\\resume\\";

                    if (!Directory.Exists(docupath2))
                    {
                        Directory.CreateDirectory(@docupath2);
                    }

                    string filePath2 = Server.MapPath("~/upfiles/resume/");

                    resume.SaveAs(Path.Combine(filePath2, FileName2));

                    teacher.Resume = FileName2;

                }
                if (file != null)
                {
                    string extensionName = file.FileName.Substring(file.FileName.LastIndexOf('.') + 1).ToLower();
                    string FileName = DateTime.Now.ToString("yyyyMMdd") + teacher.Name + "." + extensionName;
                    string docupath = Request.PhysicalApplicationPath + "upfiles\\photo\\";
                    if (!Directory.Exists(docupath))
                    {
                        Directory.CreateDirectory(@docupath);
                    }
                    string filePath = Server.MapPath("~/upfiles/photo/");

                    file.SaveAs(Path.Combine(filePath, FileName));
                    teacher.Photo = FileName;
                }
                //_db.Entry(teacher).State = EntityState.Modified;
                teacher.Update();


                var teachernew = _db.Teacher.Find(teacher.Id);
                if (fieldlist != null)
                {

                    List<int> addTeacherAssociations = new List<int>();
                    List<int> removeTeacherAssociations = new List<int>();
                    string fieldliststr = ",";
                    foreach (var item in fieldlist)
                    {
                        if (teachernew.TeacherAssociation.FirstOrDefault(x => x.FieldFK == item) == null)
                        {
                            addTeacherAssociations.Add(item);
                        }

                        fieldliststr += item + ",";
                    }

                    foreach (var item in teachernew.TeacherAssociation)
                    {
                        if (!fieldliststr.Contains("," + item.FieldFK + ","))
                        {
                            removeTeacherAssociations.Add(item.Id);
                        }
                    }

                    foreach (var item in addTeacherAssociations)
                    {
                        TeacherAssociation teacherAssociation = new TeacherAssociation();
                        teacherAssociation.TeacherFK = teacher.Id;
                        teacherAssociation.FieldFK = item;
                        teacherAssociation.Poster = User.Identity.Name;
                        teacherAssociation.InitDate = teachernew.UpdateDate;
                        _db.TeacherAssociation.Add(teacherAssociation);
                    }
                    _db.SaveChanges();
                    foreach (var item in removeTeacherAssociations)
                    {
                        TeacherAssociation teacherAssociation = _db.TeacherAssociation.Find(item);
                        _db.TeacherAssociation.Remove(teacherAssociation);
                        _db.SaveChanges();
                    }

                }
                else
                {
                    var removeTeacherAssociations =
                         teachernew.TeacherAssociation.Select(x => new { Id = x.Id }).ToList();
                    foreach (var item in removeTeacherAssociations)
                    {
                        TeacherAssociation teacherAssociation = _db.TeacherAssociation.Find(item.Id);
                        _db.TeacherAssociation.Remove(teacherAssociation);

                    }
                    _db.SaveChanges();
                }




                if (updateLicenses != null)
                {
                    foreach (var item in updateLicenses)
                    {
                        item.Update();
                    }
                }

                if (Licenselist != null)
                {
                    int count = 0;
                    List<License> addLicense = new List<License>();
                    foreach (var item in Licenselist)
                    {
                        bool addcheck = true;
                        if (checkaddbtn != null)
                        {
                            foreach (var items in checkaddbtn)
                            {
                                if (count == items)
                                {
                                    addcheck = false;
                                    break;
                                }
                            }
                        }
                        if (addcheck)
                        {
                            item.TeacherFK = teacher.Id;
                            item.InitDate = teacher.InitDate;
                            item.Poster = teacher.Poster;
                            item.UpdateDate = teacher.InitDate;
                            item.UpdateID = teacher.Poster;
                            addLicense.Add(item);
                        }
                        count++;
                    }

                    foreach (var item in addLicense)
                    {
                        _db.Licenses.Add(item);
                    }

                    _db.SaveChanges();
                }
                if (removeLicenses != null)
                {
                    foreach (var item in removeLicenses)
                    {
                        License license = _db.Licenses.Find(item);
                        _db.Licenses.Remove(license);
                        _db.SaveChanges();
                    }
                }






                return RedirectToAction("Index", new { Page = -1 });
            }
            return View(teacher);
        }

        //
        // GET: /Teacher/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Teacher teacher = _db.Teacher.Find(id);
            if (teacher == null)
            {
                return HttpNotFound();
            }
            return View(teacher);
        }

        //
        // POST: /Teacher/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Teacher teacher = _db.Teacher.Find(id);
            _db.Teacher.Remove(teacher);
            _db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            _db.Dispose();
            base.Dispose(disposing);
        }
    }

}
