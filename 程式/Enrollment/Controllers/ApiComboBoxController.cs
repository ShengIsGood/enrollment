﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json.Linq;
using Enrollment.Models;


namespace Enrollment.Controllers
{
    public class ApiComboBoxController : Controller
    {
        private BackendContext db = new BackendContext();
        public ActionResult GetData(ComboBoxParam obj)
        {
      
            var CodeID = obj.CodeID != null ? obj.CodeID.ToString() : "";
            if (CodeID.Equals("Default", StringComparison.InvariantCultureIgnoreCase))
            {
                var r = db.ComCodes.Where(w => w.GroupID == obj.DefaultType&&w.Enabled==EnableType.是).OrderBy(o => o.SortIndex).Select(s => new { ID = s.Id, Name = s.CodeName1 }).ToList();
               
                return new JsonResult { Data = r };
            }
            else
            {
                switch (obj.DefaultType.ToString())
                {
                    //////////客製下拉選單/////////////////////////////////////
                    //case "StoreCode": //所有倉別
                    //    User UserData = base.GetUserData();
                    //    break;
                    default:
                        return new JsonResult { Data = "" };
                }
            }
        }
    }

    public class ComboBoxParam
    {
        public string DefaultType { get; set; }
        public string CodeID { get; set; }
    }
}