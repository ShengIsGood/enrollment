﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Enrollment.Filters;

namespace Enrollment.Controllers
{
    [PermissionFilters]
    [Authorize]
    public class CertificateController : Controller
    {
        //
        // GET: /Certificate/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Upload()
        {
            return View();
        }

    }
}
