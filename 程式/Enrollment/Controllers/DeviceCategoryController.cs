﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using MvcPaging;
using System.Web.Mvc;
using Enrollment.Filters;
using Enrollment.Models;

namespace Enrollment.Controllers
{
	[PermissionFilters]
    [Authorize]
    public class DeviceCategoryController : _BaseController
    {
        private BackendContext _db = new BackendContext();
        private const int DefaultPageSize = 15;
        //

        


        public ActionResult Index(int? page, FormCollection fc, int? id)
        {
			//記住搜尋條件
            GetCatcheRoutes(page, fc);

            //取得正確的頁面
            int currentPageIndex = getCurrentPage(page, fc);
            

            var devicecategories = _db.DeviceCategories.Include(d => d.ParentCategory).OrderBy(p => p.SortNum).AsQueryable();
            devicecategories = (id == null || id == 0) ? devicecategories.Where(m => m.ParentId == null) : devicecategories.Where(m => m.ParentId == id);

            string categoryPath = GetCategoryPath(id);
            ViewBag.categoryPath = categoryPath == "" ? "" : string.Format("->{0}", categoryPath);


            ViewBag.ParentId = id;
         
 

//            ViewBag.Subject = Subject;
            return View(devicecategories.OrderBy(p => p.SortNum).ToPagedList(currentPageIndex, DefaultPageSize));

        }




        private string GetCategoryPath(int? id)
        {
            string categoryPath = "";
            if (id != null && id != 0)
            {

                DeviceCategory deviceCategory = _db.DeviceCategories.Find(id);
                categoryPath = deviceCategory.Subject;

                DeviceCategory parentClass = deviceCategory.ParentCategory;
                while (parentClass != null)
                {
                    categoryPath = string.Format("<a href='{0}'>{1}</a> ->{2}", parentClass.Id, parentClass.Subject, categoryPath);
                    parentClass = parentClass.ParentCategory;
                }
            }
            return categoryPath;
        }




        //
        // GET: /DeviceCategory/Details/5

        public ActionResult Details(int id = 0)
        {
            DeviceCategory devicecategory = _db.DeviceCategories.Find(id);
            if (devicecategory == null)
            {
                //return HttpNotFound();
				 return View();
            }
            return View(devicecategory);
        }

        //
        // GET: /DeviceCategory/Create

        public ActionResult Create(int? ParentId)
        {
            var deviceCategories = _db.DeviceCategories.Where(x => !x.ParentId.HasValue).ToList();
            int sortnum = 0;
            if (deviceCategories.Count > 0)
            {
                sortnum = deviceCategories.Max(x => x.SortNum) + 1;
            }
           
            if (ParentId.HasValue && ParentId > 0)
            {
                if (deviceCategories.FirstOrDefault(x => x.Id == ParentId).ChildCategory.Count > 0)
                {
                    sortnum = deviceCategories.FirstOrDefault(x => x.Id == ParentId).ChildCategory.Max(x => x.SortNum) + 1;
                }
                else
                {
                    sortnum = 0;
                }
               
            }

            ViewBag.SortNum = sortnum;
            return View();
        }

        //
        // POST: /DeviceCategory/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public ActionResult Create(DeviceCategory devicecategory )
        {
            if (ModelState.IsValid)
            {

                _db.DeviceCategories.Add(devicecategory);
                devicecategory.Create(_db,_db.DeviceCategories);
                return RedirectToAction("Index", new { Id = devicecategory.ParentId });
            }

            ViewBag.SortNum = devicecategory.SortNum;
            return View(devicecategory);
        }

        //
        // GET: /DeviceCategory/Edit/5

        public ActionResult Edit(int id = 0)
        {
            DeviceCategory devicecategory = _db.DeviceCategories.Find(id);
            if (devicecategory == null)
            {
                return HttpNotFound();
            }


            return View(devicecategory);
        }

        //
        // POST: /DeviceCategory/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
         
        public ActionResult Edit(DeviceCategory devicecategory)
        {
            if (ModelState.IsValid)
            {

               //_db.Entry(devicecategory).State = EntityState.Modified;
                devicecategory.Update();
                return RedirectToAction("Index",new{Page=-1, Id = devicecategory.ParentId });
            }
                   return View(devicecategory);
        }

        //
        // GET: /DeviceCategory/Delete/5

        public ActionResult Delete(int id = 0)
        {
            DeviceCategory devicecategory = _db.DeviceCategories.Find(id);
            if (devicecategory == null)
            {
                return HttpNotFound();
            }
            return View(devicecategory);
        }

        //
        // POST: /DeviceCategory/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            DeviceCategory devicecategory = _db.DeviceCategories.Find(id);
            List<DeviceCategory> deviceCategories=new List<DeviceCategory>();
            foreach (var items in devicecategory.ChildCategory)
            {
                deviceCategories.Add(items);
              
            }

            foreach (var item in deviceCategories)
            {
                _db.DeviceCategories.Remove(item);
            }
            _db.DeviceCategories.Remove(devicecategory);
            _db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            _db.Dispose();
            base.Dispose(disposing);
        }
    }

}
