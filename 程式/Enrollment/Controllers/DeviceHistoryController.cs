﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using MvcPaging;
using System.Web.Mvc;
using Enrollment.Filters;
using Enrollment.Models;

namespace Enrollment.Controllers
{
	[PermissionFilters]
    [Authorize]
    public class DeviceHistoryController : _BaseController
    {
        private BackendContext _db = new BackendContext();
        private const int DefaultPageSize = 15;
        //

        


        public ActionResult Index(int? page, FormCollection fc )
        {
			//記住搜尋條件
            GetCatcheRoutes(page, fc);

            //取得正確的頁面
            int currentPageIndex = getCurrentPage(page, fc);
            

            var rentdevicehistories = _db.RentDeviceHistories.Include(r => r.Device).Include(r => r.ClassHistory).OrderByDescending(p => p.InitDate).AsQueryable();
            var devices = _db.Devices.Select(x => new
            {
                Id=x.Id,
                Subject=x.Subject+"("+x.DeviceId+")"
            }).ToList();
            ViewBag.DevicesCount = _db.Devices
                .Where(x => x.DeviceStatus == DeviceStatus.正常 && x.DeviceRentStatus == DeviceRentStatus.未租用).ToList()
                .Count;
            ViewBag.DeviceId = new SelectList(devices, "Id", "Subject");
            var course = _db.ClassHistories.ToList();
            List<ClassHistoryList>classHistoryLists=new List<ClassHistoryList>();
            foreach (var item in course)
            {
                ClassHistoryList classHistoryList=new ClassHistoryList();
                classHistoryList.Id = item.Id;
                classHistoryList.Subject = item.ClassDate.ToString("yyyy/MM/dd")+"("+item.StarTime+"-"+item.EndTime+")"+item.ClassTeacher.Course.CourseName;
                classHistoryLists.Add(classHistoryList);
            }


            ViewBag.ClassHistoryId = new SelectList(classHistoryLists.OrderBy(p=>p.Id), "Id", "Subject");
            if (hasViewData("SearchByDeviceId")) 
            { 
            int searchByDeviceId = getViewDateInt("SearchByDeviceId");             
                rentdevicehistories = rentdevicehistories.Where(w => w.DeviceId == searchByDeviceId); 
            } 
            if (hasViewData("SearchByClassHistoryId")) 
            { 
            int searchByClassHistoryId = getViewDateInt("SearchByClassHistoryId");             
                rentdevicehistories = rentdevicehistories.Where(w => w.ClassHistoryId == searchByClassHistoryId); 
            } 
            if (hasViewData("SearchByDeviceUse")) 
            { 
            string DeviceUse = getViewDateStr("SearchByDeviceUse");             
             Enrollment.Models.DeviceUse searchByDeviceUse= (Enrollment.Models.DeviceUse)Enum.Parse(typeof(Enrollment.Models.DeviceUse), DeviceUse, false); 
             
                rentdevicehistories = rentdevicehistories.Where(w => w.DeviceUse == searchByDeviceUse); 
            } 
 
            if (hasViewData("SearchByIsReturn")) 
            { 
            string IsReturn = getViewDateStr("SearchByIsReturn");             
             Enrollment.Models.EnableType searchByIsReturn= (Enrollment.Models.EnableType)Enum.Parse(typeof(Enrollment.Models.EnableType), IsReturn, false); 
             
                rentdevicehistories = rentdevicehistories.Where(w => w.IsReturn == searchByIsReturn); 
            } 
 


            return View(rentdevicehistories.OrderByDescending(p => p.InitDate).ToPagedList(currentPageIndex, DefaultPageSize));

        }



        

        //
        // GET: /DeviceHistory/Details/5

        public ActionResult Details(int id = 0)
        {
            RentDeviceHistory rentdevicehistory = _db.RentDeviceHistories.Find(id);
            if (rentdevicehistory == null)
            {
                //return HttpNotFound();
				 return View();
            }
            return View(rentdevicehistory);
        }

        //
        // GET: /DeviceHistory/Create

        public ActionResult Create()
        {
            var devices = _db.Devices.Where(x => x.DeviceStatus == DeviceStatus.正常 && x.DeviceRentStatus == DeviceRentStatus.未租用).Select(x => new
            {
                Id = x.Id,
                Subject = x.Subject + "(" + x.DeviceId + ")"
            }).ToList();
            ViewBag.DeviceId = new SelectList(devices, "Id", "Subject");
              var course = _db.ClassHistories.ToList();
            List<ClassHistoryList> classHistoryLists = new List<ClassHistoryList>();
            foreach (var item in course)
            {
                ClassHistoryList classHistoryList = new ClassHistoryList();
                classHistoryList.Id = item.Id;
                classHistoryList.Subject = item.ClassDate.ToString("yyyy/MM/dd") + "(" + item.StarTime + "-" + item.EndTime + ")" + item.ClassTeacher.Course.CourseName;
                classHistoryLists.Add(classHistoryList);
            }


            ViewBag.ClassHistoryId = new SelectList(classHistoryLists.OrderBy(p => p.Id), "Id", "Subject");
           
            return View();
        }

        //
        // POST: /DeviceHistory/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public ActionResult Create(RentDeviceHistory rentdevicehistory )
        {
            if (ModelState.IsValid)
            {

                _db.RentDeviceHistories.Add(rentdevicehistory);
                rentdevicehistory.Create(_db,_db.RentDeviceHistories);

                var device = _db.Devices.Find(rentdevicehistory.DeviceId);
                device.DeviceRentStatus = DeviceRentStatus.已租用;
                _db.SaveChanges();

                return RedirectToAction("Index");
            }
            var devices = _db.Devices.Where(x => x.DeviceStatus == DeviceStatus.正常 && x.DeviceRentStatus == DeviceRentStatus.未租用).Select(x => new
            {
                Id = x.Id,
                Subject = x.Subject + "(" + x.DeviceId + ")"
            }).ToList();
            ViewBag.DeviceId = new SelectList(devices, "Id", "Subject", rentdevicehistory.DeviceId);
            var course = _db.ClassHistories.ToList();
            List<ClassHistoryList> classHistoryLists = new List<ClassHistoryList>();
            foreach (var item in course)
            {
                ClassHistoryList classHistoryList = new ClassHistoryList();
                classHistoryList.Id = item.Id;
                classHistoryList.Subject = item.ClassDate.ToString("yyyy/MM/dd") + "(" + item.StarTime + "-" + item.EndTime + ")" + item.ClassTeacher.Course.CourseName;
                classHistoryLists.Add(classHistoryList);
            }


            ViewBag.ClassHistoryId = new SelectList(classHistoryLists.OrderBy(p => p.Id), "Id", "Subject",rentdevicehistory.ClassHistoryId);
            return View(rentdevicehistory);
        }

        //
        // GET: /DeviceHistory/Edit/5

        public ActionResult Edit(int id = 0)
        {
            RentDeviceHistory rentdevicehistory = _db.RentDeviceHistories.Find(id);
            if (rentdevicehistory == null)
            {
                return HttpNotFound();
            }

            var devices = _db.Devices.Where(x => (x.DeviceStatus == DeviceStatus.正常 && x.DeviceRentStatus == DeviceRentStatus.未租用)||x.Id==rentdevicehistory.DeviceId).Select(x => new
            {
                Id = x.Id,
                Subject = x.Subject + "(" + x.DeviceId + ")"
            }).ToList();
            ViewBag.DeviceId = new SelectList(devices, "Id", "Subject", rentdevicehistory.DeviceId);
            var course = _db.ClassHistories.ToList();
            List<ClassHistoryList> classHistoryLists = new List<ClassHistoryList>();
            foreach (var item in course)
            {
                ClassHistoryList classHistoryList = new ClassHistoryList();
                classHistoryList.Id = item.Id;
                classHistoryList.Subject = item.ClassDate.ToString("yyyy/MM/dd") + "(" + item.StarTime + "-" + item.EndTime + ")" + item.ClassTeacher.Course.CourseName;
                classHistoryLists.Add(classHistoryList);
            }
            ViewBag.ClassHistoryId = new SelectList(classHistoryLists.OrderBy(p => p.Id), "Id", "Subject" ,rentdevicehistory.ClassHistoryId);

            return View(rentdevicehistory);
        }

        //
        // POST: /DeviceHistory/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
         
        public ActionResult Edit(RentDeviceHistory rentdevicehistory)
        {
            if (ModelState.IsValid)
            {

               //_db.Entry(rentdevicehistory).State = EntityState.Modified;
                rentdevicehistory.Update();
                return RedirectToAction("Index",new{Page=-1});
            }
            var devices = _db.Devices.Where(x => (x.DeviceStatus == DeviceStatus.正常 && x.DeviceRentStatus == DeviceRentStatus.未租用) || x.Id == rentdevicehistory.DeviceId).Select(x => new
            {
                Id = x.Id,
                Subject = x.Subject + "(" + x.DeviceId + ")"
            }).ToList();
            ViewBag.DeviceId = new SelectList(devices, "Id", "Subject", rentdevicehistory.DeviceId);
            var course = _db.ClassHistories.ToList();
            List<ClassHistoryList> classHistoryLists = new List<ClassHistoryList>();
            foreach (var item in course)
            {
                ClassHistoryList classHistoryList = new ClassHistoryList();
                classHistoryList.Id = item.Id;
                classHistoryList.Subject = item.ClassDate.ToString("yyyy/MM/dd") + "(" + item.StarTime + "-" + item.EndTime + ")" + item.ClassTeacher.Course.CourseName;
                classHistoryLists.Add(classHistoryList);
            }
            ViewBag.ClassHistoryId = new SelectList(classHistoryLists.OrderBy(p => p.Id), "Id", "Subject", rentdevicehistory.ClassHistoryId);

            return View(rentdevicehistory);
        }

        //
        // GET: /DeviceHistory/Delete/5

        public ActionResult Delete(int id = 0)
        {
            RentDeviceHistory rentdevicehistory = _db.RentDeviceHistories.Find(id);
            if (rentdevicehistory == null)
            {
                return HttpNotFound();
            }
            return View(rentdevicehistory);
        }

        //
        // POST: /DeviceHistory/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            RentDeviceHistory rentdevicehistory = _db.RentDeviceHistories.Find(id);
            _db.RentDeviceHistories.Remove(rentdevicehistory);
            _db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            _db.Dispose();
            base.Dispose(disposing);
        }
    }

}
