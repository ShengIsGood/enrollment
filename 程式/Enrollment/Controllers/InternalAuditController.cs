﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Enrollment.Filters;

namespace Enrollment.Controllers
{
    [PermissionFilters]
    [Authorize]
    public class InternalAuditController : Controller
    {
        //
        // GET: /InternalAudit/

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult ContentEdit()
        {
            return View();
        }

    }
}
