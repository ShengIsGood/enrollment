﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using MvcPaging;
using System.Web.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Enrollment.Filters;
using Enrollment.Models;

namespace Enrollment.Controllers
{
	[PermissionFilters]
    [Authorize]
    public class MenuController : _BaseController
    {
        private BackendContext _db = new BackendContext();
        private const int DefaultPageSize = 15;
        //

        


        public ActionResult Index(int? page, FormCollection fc )
        {
			//記住搜尋條件
            GetCatcheRoutes(page, fc);

            //取得正確的頁面
            int currentPageIndex = getCurrentPage(page, fc);
            

            var menu = _db.Menu.Where(x=>x.TopMenuId==null).Include(m => m.Menus).OrderByDescending(p => p.InitDate).AsQueryable();
            ViewBag.TopMenuId = new SelectList(_db.Menu.OrderBy(p=>p.InitDate), "Id", "Subject");
            if (hasViewData("SearchBySubject")) 
            { 
            string searchBySubject = getViewDateStr("SearchBySubject");             
                menu = menu.Where(w => w.Subject.Contains(searchBySubject)); 
            } 
 
            if (hasViewData("SearchByHaveDetails")) 
            { 
            string HaveDetails = getViewDateStr("SearchByHaveDetails");             
             Enrollment.Models.BooleanType searchByHaveDetails= (Enrollment.Models.BooleanType)Enum.Parse(typeof(Enrollment.Models.BooleanType), HaveDetails, false); 
             
                menu = menu.Where(w => w.HaveDetails == searchByHaveDetails); 
            } 
 
            if (hasViewData("SearchByTopMenuId")) 
            { 
            int searchByTopMenuId = getViewDateInt("SearchByTopMenuId");             
                menu = menu.Where(w => w.TopMenuId == searchByTopMenuId); 
            } 
            if (hasViewData("SearchByIsPlugIn")) 
            { 
            string IsPlugIn = getViewDateStr("SearchByIsPlugIn");             
             Enrollment.Models.BooleanType searchByIsPlugIn= (Enrollment.Models.BooleanType)Enum.Parse(typeof(Enrollment.Models.BooleanType), IsPlugIn, false); 
             
                menu = menu.Where(w => w.IsPlugIn == searchByIsPlugIn); 
            } 
 
            if (hasViewData("SearchByIsWork")) 
            { 
            string IsWork = getViewDateStr("SearchByIsWork");             
             Enrollment.Models.BooleanType searchByIsWork= (Enrollment.Models.BooleanType)Enum.Parse(typeof(Enrollment.Models.BooleanType), IsWork, false); 
             
                menu = menu.Where(w => w.IsWork == searchByIsWork); 
            } 
 

//            ViewBag.Subject = Subject;
            return View(menu.OrderBy(x=>x.SortNum).ToPagedList(currentPageIndex, DefaultPageSize));

        }



        

        //
        // GET: /Menu/Details/5

        public ActionResult Details(int id = 0)
        {
            Menu menu = _db.Menu.Find(id);
            if (menu == null)
            {
                //return HttpNotFound();
				 return View();
            }
            return View(menu);
        }

        //
        // GET: /Menu/Create

        public ActionResult Create()
        {
            var menu = _db.Menu.Where(x => x.TopMenuId == null).ToList();
            if (menu.Count>0)
            {

                ViewBag.SortNum = _db.Menu.Where(x => x.TopMenuId == null).Max(s => s.SortNum) + 1;
            }
            else
            {

                ViewBag.SortNum = 1;
            }
         
            return View();
        }

        //
        // POST: /Menu/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public ActionResult Create(Menu menu )
        {
            var menulist = _db.Menu.Where(x=>x.TopMenuId==null).ToList();
            Menu menulastone=new Menu();
            if (menulist.Count>0)
            {
                menulastone = menulist.OrderByDescending(x => x.InitDate).First();
                int count = Convert.ToInt16(menulastone.PrivilegeCode.Replace("A", "")) + 1;
                menu.PrivilegeCode = "A" + count.ToString("00");
            }
            else
            {
                menu.PrivilegeCode = "A01";
            }

            ModelState.Remove("PrivilegeCode");


            if (ModelState.IsValid)
            {

                _db.Menu.Add(menu);
                menu.Create(_db,_db.Menu);
                return RedirectToAction("Index");
            }

            ViewBag.TopMenuId = new SelectList(_db.Menu.OrderBy(p=>p.SortNum), "Id", "Subject", menu.TopMenuId);
            return View(menu);
        }

        //
        // GET: /Menu/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Menu menu = _db.Menu.Find(id);
            if (menu == null)
            {
                return HttpNotFound();
            }
            ViewBag.TopMenuId = new SelectList(_db.Menu.OrderBy(p=>p.SortNum), "Id", "Subject", menu.TopMenuId);
            return View(menu);
        }

        //
        // POST: /Menu/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
         
        public ActionResult Edit(Menu menu, int? TopMenuId)
        {
            if (ModelState.IsValid)
            {
                if (TopMenuId.HasValue)
                {
                    menu.TopMenuId = TopMenuId;
                }
                //_db.Entry(menu).State = EntityState.Modified;
                menu.Update();
                if (TopMenuId.HasValue)
                {
                    return RedirectToAction("MenuDetailIndex", new { TopMenuId = TopMenuId });
                }
                return RedirectToAction("Index",new{Page=-1});
            }
            ViewBag.TopMenuId = new SelectList(_db.Menu.OrderBy(p=>p.SortNum), "Id", "Subject", menu.TopMenuId);
            return View(menu);
        }

        //
        // GET: /Menu/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Menu menu = _db.Menu.Find(id);
            if (menu == null)
            {
                return HttpNotFound();
            }
            return View(menu);
        }

        //
        // POST: /Menu/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Menu menu = _db.Menu.Find(id);
            _db.Menu.Remove(menu);
            _db.SaveChanges();
            return RedirectToAction("Index");
        }
        public ActionResult MenuDetailIndex(int TopMenuId)
        {
            var menu = _db.Menu.Where(x => x.TopMenuId == TopMenuId).ToList();
            ViewBag.TopMenuId = TopMenuId;
            var topmenu = _db.Menu.Find(TopMenuId);
            if (topmenu.TopMenuId != null)
            {
                ViewBag.level = topmenu.TopMenuId;
            }
            else
            {
                ViewBag.level = null;
            }
            return View(menu);
        }
        public ActionResult MenuDetailCreate(int TopMenuId)
        {
            ViewBag.TopMenuId = TopMenuId;
            var menu = _db.Menu.Where(x => x.TopMenuId == TopMenuId).ToList();
            if (menu.Count == 0)
            {
                ViewBag.SortNum =0;

            }
            else
            {
                ViewBag.SortNum = menu.Max(x => x.SortNum) + 1;

            }
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]

        public ActionResult MenuDetailCreate(Menu menu)
        {
            var topmenu = _db.Menu.Find(menu.TopMenuId);
            Menu menulastone = new Menu();
            if (topmenu.MenuDetails.Count > 0)
            {
                menulastone = topmenu.MenuDetails.OrderByDescending(x => x.InitDate).First();

                menu.PrivilegeCode = topmenu.PrivilegeCode+"-"+( Convert.ToInt16(menulastone.PrivilegeCode.Split('-')[1]) + 1);
      
            }
            else
            {
                menu.PrivilegeCode = topmenu.PrivilegeCode + "-1";
            }

            ModelState.Remove("PrivilegeCode");


            if (ModelState.IsValid)
            {

                _db.Menu.Add(menu);
                menu.Create(_db, _db.Menu);
                return RedirectToAction("MenuDetailIndex",new { TopMenuId =menu.TopMenuId});
            }

            ViewBag.TopMenuId = menu.TopMenuId;

            //var menu = _db.Menu.Where(x => x.TopMenuId == TopMenuId).ToList();
            //if (menu.Count == 0)
            //{
            //    ViewBag.SortNum = 0;

            //}
            //else
            //{
            //    ViewBag.SortNum = menu.Max(x => x.SortNum) + 1;

            //}

              return View(menu);
        }

        public ActionResult MenuDetailThirdCreate(int TopMenuId)
        {
            ViewBag.TopMenuId = TopMenuId;
            var menu = _db.Menu.Where(x => x.TopMenuId == TopMenuId).ToList();
            if (menu.Count == 0)
            {
                ViewBag.SortNum = 0;

            }
            else
            {
                ViewBag.SortNum = menu.Max(x => x.SortNum) + 1;

            }
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]

        public ActionResult MenuDetailThirdCreate(Menu menu)
        {
            var topmenu = _db.Menu.Find(menu.TopMenuId);
            Menu menulastone = new Menu();
            if (topmenu.MenuDetails.Count > 0)
            {
                menulastone = topmenu.MenuDetails.OrderByDescending(x => x.InitDate).First();

                menu.PrivilegeCode = topmenu.PrivilegeCode + "-" + (Convert.ToInt16(menulastone.PrivilegeCode.Split('-')[1]) + 1);

            }
            else
            {
                menu.PrivilegeCode = topmenu.PrivilegeCode + "-1";
            }

            ModelState.Remove("PrivilegeCode");


            if (ModelState.IsValid)
            {

                _db.Menu.Add(menu);
                menu.Create(_db, _db.Menu);
                return RedirectToAction("MenuDetailIndex", new { TopMenuId = menu.TopMenuId });
            }

            ViewBag.TopMenuId = menu.TopMenuId;

            //var menu = _db.Menu.Where(x => x.TopMenuId == TopMenuId).ToList();
            //if (menu.Count == 0)
            //{
            //    ViewBag.SortNum = 0;

            //}
            //else
            //{
            //    ViewBag.SortNum = menu.Max(x => x.SortNum) + 1;

            //}

            return View(menu);
        }



        public ActionResult MenuDetailEdit(int id = 0)
        {
            Menu menu = _db.Menu.Find(id);
            if (menu == null)
            {
                return HttpNotFound();
            }
            return View(menu);
        }

        //
        // POST: /Menu/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]

        public ActionResult MenuDetailEdit(Menu menu)
        {
            if (ModelState.IsValid)
            {

                //_db.Entry(menu).State = EntityState.Modified;
                menu.Update();
                return RedirectToAction("MenuDetailIndex", new { TopMenuId=menu.TopMenuId });
            }
              return View(menu);
        }






        [HttpPost]
        public ActionResult Delmenu(int id)
        {   
            JObject jObject = new JObject();
            string jsonContent;
            Menu menu = _db.Menu.Find(id);
            List<Menu> delmenu=new List<Menu>();
            if (menu.MenuDetails.Count > 0)
            {
                foreach (var item in menu.MenuDetails)
                {
                    if (item.MenuDetails.Count > 0)
                    {
                        foreach (var items in item.MenuDetails)
                        {
                            delmenu.Add(items);
                           
                        }
                    }
                    delmenu.Add(item);
                }
            }

           List<string> delPermission=new List<string>();
            foreach (var item in delmenu)
            {
                delPermission.Add(item.PrivilegeCode);
                _db.Menu.Remove(item);
            }
            _db.Menu.Remove(menu);
            _db.SaveChanges();

            var role = _db.Roles.ToList();
            if (role != null)
            {
                foreach (var item in role)
                {
                    item.Permission = "";
                    foreach (var additem in item.RoleMenus)
                    {
                        item.Permission += additem.Menu.PrivilegeCode + ",";
                    }

                    item.Permission = item.Permission.TrimEnd(',');
                }
            }
            _db.SaveChanges();



            RoleController roleController = new RoleController();
            roleController.resectpermission(User.Identity.Name);
            jObject.Add(new JProperty("Message", "Success"));
            jsonContent = JsonConvert.SerializeObject(jObject, Formatting.Indented);
            return new ContentResult { Content = jsonContent, ContentType = "application/json" };
        }










        protected override void Dispose(bool disposing)
        {
            _db.Dispose();
            base.Dispose(disposing);
        }
    }

}
