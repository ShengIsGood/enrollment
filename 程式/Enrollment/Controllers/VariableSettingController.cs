﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using MvcPaging;
using System.Web.Mvc;
using Enrollment.Filters;
using Enrollment.Models;

namespace Enrollment.Controllers
{
	[PermissionFilters]
    [Authorize]
    public class VariableSettingController : _BaseController
    {
        private BackendContext _db = new BackendContext();
        private const int DefaultPageSize = 15;
        //

        


        public ActionResult Index(int? page, FormCollection fc )
        {
			//記住搜尋條件
            GetCatcheRoutes(page, fc);

            //取得正確的頁面
            int currentPageIndex = getCurrentPage(page, fc);
            

            var comgroups = _db.ComGroups.OrderByDescending(p => p.InitDate).AsQueryable();

           

            if (hasViewData("SearchByGroupID"))
            {
                string SearchByGroupID = getViewDateStr("SearchByGroupID");
                comgroups = comgroups.Where(w => w.GroupID.Contains(SearchByGroupID));

            }

            if (hasViewData("SearchByGroupName"))
            {
                string SearchByGroupName = getViewDateStr("SearchByGroupName");
                comgroups = comgroups.Where(w => w.GroupName.Contains(SearchByGroupName));

            }
            return View(comgroups.OrderByDescending(p => p.InitDate).ToPagedList(currentPageIndex, DefaultPageSize));

        }

 

        //
        // GET: /VariableSetting/Details/5

        public ActionResult Details(string id = null)
        {
            ComGroup comgroup = _db.ComGroups.Find(id);
            if (comgroup == null)
            {
                //return HttpNotFound();
				 return View();
            }
            return View(comgroup);
        }

        //
        // GET: /VariableSetting/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /VariableSetting/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public ActionResult Create(ComGroup comgroup )
        {
            if (ModelState.IsValid)
            {
              
                if (string.IsNullOrEmpty(comgroup.GroupType))
                {
                    comgroup.GroupType = "";
                }
                if (string.IsNullOrEmpty(comgroup.SysType))
                {
                    comgroup.SysType = "";
                }
             
                _db.ComGroups.Add(comgroup);
                comgroup.Create(_db,_db.ComGroups);
                return RedirectToAction("Index");
            }

            return View(comgroup);
        }

        //
        // GET: /VariableSetting/Edit/5

        public ActionResult Edit(string id = null)
        {
            ComGroup comgroup = _db.ComGroups.Find(id);
            if (comgroup == null)
            {
                return HttpNotFound();
            }
            return View(comgroup);
        }

        //
        // POST: /VariableSetting/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
         
        public ActionResult Edit(ComGroup comgroup)
        {
            if (ModelState.IsValid)
            {
                if (string.IsNullOrEmpty(comgroup.GroupType))
                {
                    comgroup.GroupType = "";
                }
                if (string.IsNullOrEmpty(comgroup.SysType))
                {
                    comgroup.SysType = "";
                }
                //_db.Entry(comgroup).State = EntityState.Modified;
                comgroup.Update();
                return RedirectToAction("Index",new{Page=-1});
            }
            return View(comgroup);
        }

        //
        // GET: /VariableSetting/Delete/5

        public ActionResult Delete(string id = null)
        {
            ComGroup comgroup = _db.ComGroups.Find(id);
           
            if (comgroup == null)
            {
                return HttpNotFound();
            }

            ViewBag.ErrorMessage = Session["ErrorMessage"];
            Session["ErrorMessage"] = null;
            return View(comgroup);
        }

        //
        // POST: /VariableSetting/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            ComGroup comgroup = _db.ComGroups.Find(id);
            if (comgroup.ComCode.Count != 0)
            {
                try
                {
                    foreach (var item in comgroup.ComCode.ToList())
                    {
                        _db.ComCodes.Remove(item);
                    }
                    _db.SaveChanges();
                }
                catch (Exception e)
                {
                    Session["ErrorMessage"] = "很抱歉無法刪除，因有資料取自於您要刪除的群組中，請更改完畢再刪除，謝謝";
                    return RedirectToAction("Delete",new{id=id});
                }  
            }
            _db.ComGroups.Remove(comgroup);
            _db.SaveChanges();
            return RedirectToAction("Index");
        }


        public ActionResult ComCodeIndex(string GroupID)
        {
            var group = _db.ComGroups.FirstOrDefault(x => x.GroupID == GroupID);
            ViewBag.GroupID = group.GroupID;
            ViewBag.GroupName = group.GroupName;
            var comcodes = _db.ComCodes.Where(x => x.GroupID == GroupID).ToList();


            return View(comcodes);

        }
        public ActionResult ComCodeCreate(string GroupID)
        {
            var group = _db.ComGroups.FirstOrDefault(x => x.GroupID == GroupID);
            ViewBag.GroupID = group.GroupID;
            ViewBag.GroupName = group.GroupName;
            int maxsortindex = 1;
            if (group.ComCode.Count != 0)
            {
                 maxsortindex = Convert.ToInt16(group.ComCode.OrderByDescending(x => x.SortIndex).First().SortIndex) + 1;
            }

            ViewBag.sortindex = maxsortindex;
            return View();
        }

        //
        // POST: /FF/Create

        [HttpPost]
        [ValidateAntiForgeryToken]

        public ActionResult ComCodeCreate(ComCode comcode)
        {
            if (ModelState.IsValid)
            {
                if (string.IsNullOrEmpty(comcode.CodeName1))
                {
                    comcode.CodeName1 = "";
                }
                if (string.IsNullOrEmpty(comcode.CodeName2))
                {
                    comcode.CodeName2 = "";
                }
                if (string.IsNullOrEmpty(comcode.CodeName3))
                {
                    comcode.CodeName3 = "";
                }
                if (string.IsNullOrEmpty(comcode.Memo))
                {
                    comcode.Memo = "";
                }

             
                _db.ComCodes.Add(comcode);
                comcode.Create(_db, _db.ComCodes);
                return RedirectToAction("ComCodeIndex",new{GroupID=comcode.GroupID});
            }

            var group = _db.ComGroups.FirstOrDefault(x => x.GroupID == comcode.GroupID);
            ViewBag.GroupID = group.GroupID;
            ViewBag.GroupName = group.GroupName;
            int maxsortindex = 1;
            if (group.ComCode.Count != 0)
            {
                maxsortindex = Convert.ToInt16(group.ComCode.OrderByDescending(x => x.SortIndex).First().SortIndex) + 1;
            }

            if (comcode.SortIndex == 0)
            {
                ViewBag.sortindex = maxsortindex;
            }
            else
            {
                ViewBag.sortindex = comcode.SortIndex;
            }

            return View(comcode);
        }




        public ActionResult ComCodeEdit(int id = 0)
        {
            ComCode comcode = _db.ComCodes.Find(id);
            if (comcode == null)
            {
                return HttpNotFound();
            }
          
            return View(comcode);
        }

        //
        // POST: /FF/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]

        public ActionResult ComCodeEdit(ComCode comcode)
        {
            if (ModelState.IsValid)
            {
                if (string.IsNullOrEmpty(comcode.CodeName1))
                {
                    comcode.CodeName1 = "";
                }
                if (string.IsNullOrEmpty(comcode.CodeName2))
                {
                    comcode.CodeName2 = "";
                }
                if (string.IsNullOrEmpty(comcode.CodeName3))
                {
                    comcode.CodeName3 = "";
                }
                if (string.IsNullOrEmpty(comcode.Memo))
                {
                    comcode.Memo = "";
                }
                //_db.Entry(comcode).State = EntityState.Modified;
                comcode.Update();
                return RedirectToAction("ComCodeIndex", new { GroupID =comcode.GroupID });
            }
           
            return View(comcode);
        }



     

        public ActionResult ComCodeDelete(int id = 0)
        {
            ComCode comcode = _db.ComCodes.Find(id);
            if (comcode == null)
            {
                return HttpNotFound();
            }
            ViewBag.ErrorMessage = Session["ErrorMessage"];
            Session["ErrorMessage"] = null;
            return View(comcode);
        }

    

        [HttpPost, ActionName("ComCodeDelete")]
        [ValidateAntiForgeryToken]
        public ActionResult ComCodeDeleteConfirmed(int id)
        {
            ComCode comcode = _db.ComCodes.Find(id);
            string groupId = comcode.GroupID;

            try
            {
                _db.ComCodes.Remove(comcode);
                _db.SaveChanges();
            }
            catch (Exception e)
            {
                Session["ErrorMessage"] = "很抱歉無法刪除，因有資料取自於您要刪除的群組中，請更改完畢再刪除，謝謝";
                return RedirectToAction("ComCodeDelete", new { id = id });
            }
           
            return RedirectToAction("ComCodeIndex",new{ GroupID=groupId });
        }




        protected override void Dispose(bool disposing)
        {
            _db.Dispose();
            base.Dispose(disposing);
        }
    }

}
