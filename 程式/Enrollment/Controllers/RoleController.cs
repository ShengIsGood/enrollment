﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Enrollment.Filters;
using Enrollment.Models;


namespace Enrollment.Controllers
{
    [PermissionFilters]
    [Authorize]
    public class RoleController : Controller
    {
        private BackendContext db = new BackendContext();

        //
        // GET: /Role/

        public ActionResult Index()
        {
           
            return View(db.Roles.ToList());
        }

        public void resectpermission(string  username="")
        {
            var member = db.Members.ToList();
            TimeZoneInfo TPZone = TimeZoneInfo.FindSystemTimeZoneById("Taipei Standard Time");
            DateTime NOWTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, TPZone);
           
            foreach (var item in member)
            {
                item.Permission = "";
                string allpermission = "";
                foreach (var roleitem in item.Roles)
                {
                    allpermission += roleitem.Permission + ",";
                }
                List<string> permissionlist=allpermission.Split(',').Distinct().ToList();
                foreach (var pitem in permissionlist)
                {
                    item.Permission += pitem + ",";
                }

                item.Permission = item.Permission.TrimEnd(',');
                item.UpdateDate = NOWTime;
                if (string.IsNullOrEmpty(username))
                {
                    item.UpdateID = User.Identity.Name;
                }
                else
                {
                    item.UpdateID = username;
                }
             
                db.SaveChanges();
            }

           
        }

        //
        // GET: /Role/Details/5

        public ActionResult Details(int id = 0)
        {
            Role role = db.Roles.Find(id);
            if (role == null)
            {
                return HttpNotFound();
            }
            return View(role);
        }

        //
        // GET: /Role/Create

        public ActionResult Create()
        {
            ViewBag.Members = db.Members.ToList();
            return View();
        }

        //
        // POST: /Role/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Role role, string MemberListSelect)
        {
            role.Permission = role.Permission.Replace(" ", "");
            if (ModelState.IsValid)
            {
                string aa = Request["MemberListSelect"];
                if (!string.IsNullOrEmpty(aa))
                {
                    string[] strArray = MemberListSelect.Split(',');

                    var members = db.Members.ToList().Where(c => strArray.Contains(c.Id.ToString()));

                    foreach (var member in members)
                    {
                        role.Members.Add(member);
                    }
                }

                role.Create(db, db.Roles);
                if (!string.IsNullOrEmpty(MemberListSelect))
                {
                    resectpermission();
                }

                int roleid = role.Id;
                string permission =role.Permission;
                db=new BackendContext();
                var menu = db.Menu.Where(x => permission.Contains( x.PrivilegeCode )).ToList();
                permission = "," + permission + ",";
                foreach (var item in menu)
                {
                    if (permission.Replace("," + item.PrivilegeCode + ",", "").Length < permission.Length)
                    {
                        RoleMenu rolemenuitem = new RoleMenu();
                        rolemenuitem.MenuId = item.Id;
                        rolemenuitem.RoleId = roleid;
                        rolemenuitem.InitDate = Utility.GetDateTime();
                        rolemenuitem.Poster = User.Identity.Name;
                        db.RoleMenu.Add(rolemenuitem);
                    }
          
               }

                db.SaveChanges();




                return RedirectToAction("Index");
            }

            return View(role);
        }

        //
        // GET: /Role/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Role role = db.Roles.Find(id);
            ViewBag.Members = db.Members.ToList().Where(p => !(role.Members.Contains(p)));


            if (role == null)
            {
                return HttpNotFound();
            }
            return View(role);
        }

        //
        // POST: /Role/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Role role, string MemberListSelect)
        {
            TimeZoneInfo TPZone = TimeZoneInfo.FindSystemTimeZoneById("Taipei Standard Time");
            DateTime NOWTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, TPZone);
            role.UpdateDate = NOWTime;
            if (ModelState.IsValid)
            {
                //取得資料庫裏面原來的值
                var roleItem = db.Roles.Single(r => r.Id == role.Id);

                //套用新的值
                db.Entry(roleItem).CurrentValues.SetValues(role);


                //放入新的值
                roleItem.Members.Clear();
                if (!string.IsNullOrEmpty(MemberListSelect))
                {
                    string[] strArray = MemberListSelect.Split(',');
                    var memberLists = db.Members.ToList().Where(c => strArray.Contains(c.Id.ToString()));
                    foreach (var m in memberLists)
                    {
                        roleItem.Members.Add(m);
                    }
                }
                roleItem.Permission = role.Permission.Replace(" ", "");
                db.Entry(roleItem).State = EntityState.Modified;
                db.SaveChanges();
                if (!string.IsNullOrEmpty(MemberListSelect))
                {
                    resectpermission();
                }
                string permission = roleItem.Permission;     
                var menu = db.Menu.Where(x => permission.Contains(x.PrivilegeCode)).ToList();
                permission = "," + permission + ",";
                List<RoleMenu> roleMenus = roleItem.RoleMenus.ToList();
                foreach (var item in roleMenus)
                {
                    db.RoleMenu.Remove(item);                 
                }
                db.SaveChanges();
                db = new BackendContext();
                foreach (var item in menu)
                {
                    if (permission.Replace("," + item.PrivilegeCode + ",", "").Length < permission.Length)
                    {
                        RoleMenu rolemenuitem = new RoleMenu();
                        rolemenuitem.MenuId = item.Id;
                        rolemenuitem.RoleId = role.Id;
                        rolemenuitem.InitDate = Utility.GetDateTime();
                        rolemenuitem.Poster = User.Identity.Name;
                        db.RoleMenu.Add(rolemenuitem);
                    }
                }
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return RedirectToAction("Edit");
        }

        //
        // GET: /Role/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Role role = db.Roles.Find(id);
            if (role == null)
            {
                return HttpNotFound();
            }
            return View(role);
        }

        //
        // POST: /Role/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Role role = db.Roles.Find(id);
            db.Roles.Remove(role);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        //輸出treeView javascript Code 
        public JavaScriptResult TreeScript(int id = 0)
        {
            Role role = db.Roles.Find(id);
            string strPermission = "";
            if (role != null)
            {
                strPermission = ","+role.Permission+",";
            }

            var menu = db.Menu.Where(x => x.IsWork == BooleanType.是).ToList();
            //string strMenu = string.Format("var treeData =[{0}]", Utility.GetMenu(strPermission));
            string strMenu = string.Format("var treeData =[{0}]", Utility.GetMenu(strPermission, menu));
            string treeScript = System.IO.File.ReadAllText(Server.MapPath("~/Config/PermissionTree.js"));

            return JavaScript(strMenu + treeScript);

        }

        [HttpPost]
        public ActionResult DelRole(int id)
        {
            JObject jObject = new JObject();
            string jsonContent;
            Role role = db.Roles.Find(id);
            db.Roles.Remove(role);
            db.SaveChanges();
            jObject.Add(new JProperty("Message", "Success"));
            jsonContent = JsonConvert.SerializeObject(jObject, Formatting.Indented);
            return new ContentResult { Content = jsonContent, ContentType = "application/json" };
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}