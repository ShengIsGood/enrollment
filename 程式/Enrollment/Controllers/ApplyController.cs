﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using MvcPaging;
using System.Web.Mvc;
using Enrollment.Filters;
using Enrollment.Models;

namespace Enrollment.Controllers
{
    [PermissionFilters]
    [Authorize]
    public class ApplyController : _BaseController
    {
        private BackendContext _db = new BackendContext();
        private const int DefaultPageSize = 15;
        //




        public ActionResult Index(int? page, FormCollection fc)
        {
            //記住搜尋條件
            GetCatcheRoutes(page, fc);

            //取得正確的頁面
            int currentPageIndex = getCurrentPage(page, fc);


            var classstudents = _db.ClassStudents.Include(c => c.ClassTeacher).Include(c => c.Student).OrderByDescending(p => p.InitDate).AsQueryable();
            var ClassTeachersLists = _db.ClassTeachers.OrderBy(p => p.InitDate).Select(x => new
            {
                Id = x.Id,
                Subject = x.Course.CourseName,
                InitDate = x.InitDate
            }).ToList();
            ViewBag.ClassTeacherId = new SelectList(ClassTeachersLists.OrderBy(p => p.InitDate), "Id", "Subject");
            ViewBag.StudentId = new SelectList(_db.Student.OrderBy(p => p.InitDate), "Id", "Name");
            if (hasViewData("SearchByClassTeacherId"))
            {
                int searchByClassTeacherId = getViewDateInt("SearchByClassTeacherId");
                classstudents = classstudents.Where(w => w.ClassTeacherId == searchByClassTeacherId);
            }
            if (hasViewData("SearchByStudentId"))
            {
                int searchByStudentId = getViewDateInt("SearchByStudentId");
                classstudents = classstudents.Where(w => w.StudentId == searchByStudentId);
            }
            if (hasViewData("SearchByClassStatus"))
            {
                string ClassStatus = getViewDateStr("SearchByClassStatus");
                Enrollment.Models.ApplyStatus searchByClassStatus = (Enrollment.Models.ApplyStatus)Enum.Parse(typeof(Enrollment.Models.ApplyStatus), ClassStatus, false);

                classstudents = classstudents.Where(w => w.ClassStatus == searchByClassStatus);
            }

            if (hasViewData("SearchByPaymentStatus"))
            {
                string PaymentStatus = getViewDateStr("SearchByPaymentStatus");
                Enrollment.Models.PaymentStatus searchByPaymentStatus = (Enrollment.Models.PaymentStatus)Enum.Parse(typeof(Enrollment.Models.PaymentStatus), PaymentStatus, false);

                classstudents = classstudents.Where(w => w.PaymentStatus == searchByPaymentStatus);
            }

            if (hasViewData("SearchByNeedRoom"))
            {
                string NeedRoom = getViewDateStr("SearchByNeedRoom");
                Enrollment.Models.EnableType searchByNeedRoom = (Enrollment.Models.EnableType)Enum.Parse(typeof(Enrollment.Models.EnableType), NeedRoom, false);

                classstudents = classstudents.Where(w => w.NeedRoom == searchByNeedRoom);
            }



            return View(classstudents.OrderByDescending(p => p.InitDate).ToPagedList(currentPageIndex, DefaultPageSize));

        }





        //
        // GET: /Apply/Details/5

        public ActionResult Details(int id = 0)
        {
            ClassStudents classstudents = _db.ClassStudents.Find(id);
            if (classstudents == null)
            {
                //return HttpNotFound();
                return View();
            }
            return View(classstudents);
        }

        //
        // GET: /Apply/Create

        public ActionResult Create()
        {
            var ClassTeachersLists = _db.ClassTeachers.OrderBy(p => p.InitDate).Select(x => new
            {
                Id = x.Id,
                Subject = x.Course.CourseName,
                InitDate = x.InitDate
            }).ToList();
            ViewBag.ClassTeacherId = new SelectList(ClassTeachersLists.OrderBy(p => p.InitDate), "Id", "Subject");
            ViewBag.StudentId = new SelectList(_db.Student.OrderBy(p => p.InitDate), "Id", "Name");
            return View();
        }

        //
        // POST: /Apply/Create

        [HttpPost]
        [ValidateAntiForgeryToken]

        public ActionResult Create(ClassStudents classstudents)
        {
            if (ModelState.IsValid)
            {

                _db.ClassStudents.Add(classstudents);
                classstudents.Create(_db, _db.ClassStudents);
                return RedirectToAction("Index");
            }

            var ClassTeachersLists = _db.ClassTeachers.OrderBy(p => p.InitDate).Select(x => new
            {
                Id = x.Id,
                Subject = x.Course.CourseName,
                InitDate = x.InitDate
            }).ToList();
            ViewBag.ClassTeacherId = new SelectList(ClassTeachersLists.OrderBy(p => p.InitDate), "Id", "Subject", classstudents.ClassTeacherId);
            ViewBag.StudentId = new SelectList(_db.Student.OrderBy(p => p.InitDate), "Id", "Name", classstudents.StudentId);
            return View(classstudents);
        }

        //
        // GET: /Apply/Edit/5

        public ActionResult Edit(int id = 0)
        {
            ClassStudents classstudents = _db.ClassStudents.Find(id);
            if (classstudents == null)
            {
                return HttpNotFound();
            }
            var ClassTeachersLists = _db.ClassTeachers.OrderBy(p => p.InitDate).Select(x => new
            {
                Id = x.Id,
                Subject = x.Course.CourseName,
                InitDate = x.InitDate
            }).ToList();
            ViewBag.ClassTeacherId = new SelectList(ClassTeachersLists.OrderBy(p => p.InitDate), "Id", "Subject", classstudents.ClassTeacherId);
            ViewBag.StudentId = new SelectList(_db.Student.OrderBy(p => p.InitDate), "Id", "Name", classstudents.StudentId);
            return View(classstudents);
        }

        //
        // POST: /Apply/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]

        public ActionResult Edit(ClassStudents classstudents)
        {
            if (ModelState.IsValid)
            {

                //_db.Entry(classstudents).State = EntityState.Modified;
                classstudents.Update();
                return RedirectToAction("Index", new { Page = -1 });
            }
            var ClassTeachersLists = _db.ClassTeachers.OrderBy(p => p.InitDate).Select(x => new
            {
                Id = x.Id,
                Subject = x.Course.CourseName,
                InitDate = x.InitDate
            }).ToList();
            ViewBag.ClassTeacherId = new SelectList(ClassTeachersLists.OrderBy(p => p.InitDate), "Id", "Subject", classstudents.ClassTeacherId);
            ViewBag.StudentId = new SelectList(_db.Student.OrderBy(p => p.InitDate), "Id", "Name", classstudents.StudentId);
            return View(classstudents);
        }

        //
        // GET: /Apply/Delete/5

        public ActionResult Delete(int id = 0)
        {
            ClassStudents classstudents = _db.ClassStudents.Find(id);
            if (classstudents == null)
            {
                return HttpNotFound();
            }
            return View(classstudents);
        }

        //
        // POST: /Apply/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ClassStudents classstudents = _db.ClassStudents.Find(id);
            _db.ClassStudents.Remove(classstudents);
            _db.SaveChanges();
            return RedirectToAction("Index");
        }



        public ActionResult ApplyClass(int studentnum, int id = 0)
        {
            var students = _db.Student.OrderBy(x => Guid.NewGuid()).Take(studentnum).ToList();
            var classTeacher = _db.ClassTeachers.Find(id);
            var classStudentslist = _db.ClassStudents.ToList();
            foreach (var item in students)
            {
                if (!classStudentslist.Any(x => x.StudentId == item.Id&&x.ClassTeacherId==id))
                {
                    ClassStudents classStudent = new ClassStudents();
                    classStudent.ClassTeacherId = id;
                    classStudent.StudentId = item.Id;
                    if (classTeacher.ClassStatus == ClassStatus.報名中 || classTeacher.ClassStatus == ClassStatus.已達最低開課人數)
                    {
                        classStudent.ClassStatus = ApplyStatus.錄取;
                    }
                    else if (classTeacher.ClassStatus == ClassStatus.人數不足不開課)
                    {
                        classStudent.ClassStatus = ApplyStatus.人數不足取消課程;

                    }
                    else if (classTeacher.ClassStatus == ClassStatus.取消課程)
                    {
                        classStudent.ClassStatus = ApplyStatus.課程已取消;

                    }
                    else if (classTeacher.ClassStatus == ClassStatus.已額滿)
                    {
                        classStudent.ClassStatus = ApplyStatus.備取;

                    }
                    else if (classTeacher.ClassStatus == ClassStatus.確定開課)
                    {
                        classStudent.ClassStatus = ApplyStatus.報名已截止;

                    }

                    classStudent.PaymentStatus = PaymentStatus.未繳費;
                    if (classTeacher.ProvideDormitory == EnableType.否)
                    {
                        classStudent.NeedRoom = EnableType.否;
                    }
                    else
                    {
                        classStudent.NeedRoom = EnableType.是;
                    }

                    _db.ClassStudents.Add(classStudent);
                    classStudent.Create(_db, _db.ClassStudents);
                }

            }
            TimeZoneInfo TPZone = TimeZoneInfo.FindSystemTimeZoneById("Taipei Standard Time");
            DateTime NOWTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, TPZone);
            classTeacher.UpdateDate = NOWTime;
            classTeacher.UpdateID = User.Identity.Name;
            if (classTeacher.MaxStudents <= classTeacher.ClassStudentses.Count)
            {
                classTeacher.ClassStatus = ClassStatus.已額滿;
                
            }else if (classTeacher.MinStudents <= classTeacher.ClassStudentses.Count)
            {
                classTeacher.ClassStatus = ClassStatus.已達最低開課人數;
              
            }

            _db.SaveChanges();
            return Content("Success");
        }

        public ActionResult List(int id)
        {
            ClassTeacher classTeacher = _db.ClassTeachers.Find(id);
         
            return View(classTeacher);
        }

        protected override void Dispose(bool disposing)
        {
            _db.Dispose();
            base.Dispose(disposing);
        }
    }

}
