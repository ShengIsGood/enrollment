﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Web;
using MvcPaging;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using Enrollment.Filters;
using Enrollment.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Enrollment.Controllers
{
    [PermissionFilters]
    [Authorize]
    public class ClassTeacherController : _BaseController
    {
        private BackendContext _db = new BackendContext();
        private const int DefaultPageSize = 15;
        //




        public ActionResult Index(int? page, FormCollection fc)
        {
            //記住搜尋條件
            GetCatcheRoutes(page, fc);

            //取得正確的頁面
            int currentPageIndex = getCurrentPage(page, fc);


            var classteachers = _db.ClassTeachers.Include(c => c.Course).Include(c => c.Teacher).Include(c => c.Place).OrderByDescending(p => p.InitDate).AsQueryable();
            var courseLists = _db.course.OrderBy(p => p.InitDate).Select(x => new
            {
                Id = x.Id,
                CourseNumber = "(" + x.CourseNumber + ")" + x.CourseName,

            }).ToList();
            ViewBag.CourseId = new SelectList(courseLists, "Id", "CourseNumber");

            ViewBag.TeacherId = new SelectList(_db.Teacher.OrderBy(p => p.InitDate), "Id", "Name");
            var placeLists = _db.Place.OrderBy(p => p.InitDate).Select(x => new
            {
                Id = x.Id,
                PlaceNumberName = x.PlaceNumber + x.PlaceName,
                Type = x.category,
            }).ToList();
            ViewBag.PlaceId = new SelectList(placeLists.Where(x => x.Type != Category.宿舍), "Id", "PlaceNumberName");
            if (hasViewData("SearchByCourseId"))
            {
                int searchByCourseId = getViewDateInt("SearchByCourseId");
                classteachers = classteachers.Where(w => w.CourseId == searchByCourseId);
            }
            if (hasViewData("SearchByTeacherId"))
            {
                int searchByTeacherId = getViewDateInt("SearchByTeacherId");
                classteachers = classteachers.Where(w => w.TeacherId == searchByTeacherId);
            }
            if (hasViewData("SearchByPlaceId"))
            {
                int searchByPlaceId = getViewDateInt("SearchByPlaceId");
                classteachers = classteachers.Where(w => w.PlaceId == searchByPlaceId);
            }
            if (hasViewData("SearchByProvideDormitory"))
            {
                string ProvideDormitory = getViewDateStr("SearchByProvideDormitory");
                Enrollment.Models.EnableType searchByProvideDormitory = (Enrollment.Models.EnableType)Enum.Parse(typeof(Enrollment.Models.EnableType), ProvideDormitory, false);

                classteachers = classteachers.Where(w => w.ProvideDormitory == searchByProvideDormitory);
            }

            if (hasViewData("SearchByClassCycle"))
            {
                string ClassCycle = getViewDateStr("SearchByClassCycle");
                Enrollment.Models.ClassCycle searchByClassCycle = (Enrollment.Models.ClassCycle)Enum.Parse(typeof(Enrollment.Models.ClassCycle), ClassCycle, false);

                classteachers = classteachers.Where(w => w.ClassCycle == searchByClassCycle);
            }



            return View(classteachers.OrderByDescending(p => p.InitDate).ToPagedList(currentPageIndex, DefaultPageSize));

        }





        //
        // GET: /ClassTeacher/Details/5

        public ActionResult Details(int id = 0)
        {
            ClassTeacher classteacher = _db.ClassTeachers.Find(id);
            if (classteacher == null)
            {
                //return HttpNotFound();
                return View();
            }
            return View(classteacher);
        }

        //
        // GET: /ClassTeacher/Create

        public ActionResult Create()
        {
            var courseLists = _db.course.OrderBy(p => p.InitDate).Select(x => new
            {
                Id = x.Id,
                CourseNumber = "(" + x.CourseNumber + ")" + x.CourseName,

            }).ToList();
            ViewBag.CourseId = new SelectList(courseLists, "Id", "CourseNumber");
            ViewBag.TeacherId = new SelectList(_db.Teacher.OrderBy(p => p.InitDate), "Id", "Name");


            var placeLists = _db.Place.OrderBy(p => p.InitDate).Select(x => new
            {
                Id = x.Id,
                PlaceNumberName = x.PlaceNumber + x.PlaceName,
                Type = x.category,
            }).ToList();
            ViewBag.PlaceId = new SelectList(placeLists.Where(x => x.Type != Category.宿舍), "Id", "PlaceNumberName");
            ViewBag.DormRoom = new SelectList(placeLists.Where(x => x.Type == Category.宿舍), "Id", "PlaceNumberName");

            return View();
        }

        //
        // POST: /ClassTeacher/Create

        [HttpPost]
        [ValidateAntiForgeryToken]

        public ActionResult Create(ClassTeacher classteacher, List<ClassTime> classTime, List<int> checkaddbtn, ClassCycle ClassCycletrue)
        {
            if (ModelState.IsValid)
            {
                classteacher.ClassCycle = ClassCycletrue;
                if (classteacher.ProvideDormitory == EnableType.否)
                {
                    classteacher.DormRoom = null;
                }
                _db.ClassTeachers.Add(classteacher);
                classteacher.Create(_db, _db.ClassTeachers);


                if (classTime != null)
                {
                    int count = 0;
                    List<ClassTime> addClassTimes = new List<ClassTime>();
                    foreach (var item in classTime)
                    {
                        bool addcheck = true;
                        if (checkaddbtn != null)
                        {
                            foreach (var items in checkaddbtn)
                            {
                                if (count == items)
                                {
                                    addcheck = false;
                                    break;
                                }
                            }
                        }

                        if (addcheck)
                        {
                            if (classteacher.ClassCycle == ClassCycle.特定日期)
                            {
                                item.Day = item.Date.Value.DayOfWeek.ToString("d");

                            }

                            item.ClassTeacherId = classteacher.Id;
                            item.InitDate = classteacher.InitDate;
                            item.Poster = classteacher.Poster;
                            item.UpdateDate = classteacher.InitDate;
                            item.UpdateID = classteacher.Poster;
                            addClassTimes.Add(item);
                        }

                        count++;
                    }

                    foreach (var item in addClassTimes)
                    {
                        _db.ClassTimes.Add(item);
                    }

                    _db.SaveChanges();
                }

                return RedirectToAction("Index");
            }

            ViewBag.CourseId = new SelectList(_db.course.OrderBy(p => p.InitDate), "Id", "CourseNumber", classteacher.CourseId);
            ViewBag.TeacherId = new SelectList(_db.Teacher.OrderBy(p => p.InitDate), "Id", "Name", classteacher.TeacherId);
            ViewBag.PlaceId = new SelectList(_db.Place.OrderBy(p => p.InitDate), "Id", "PlaceNumber", classteacher.PlaceId);
            return View(classteacher);
        }

        //
        // GET: /ClassTeacher/Edit/5

        public ActionResult Edit(int id = 0)
        {
            ClassTeacher classteacher = _db.ClassTeachers.Find(id);
            if (classteacher == null)
            {
                return HttpNotFound();
            }
            var courseLists = _db.course.OrderBy(p => p.InitDate).Select(x => new
            {
                Id = x.Id,
                CourseNumber = "(" + x.CourseNumber + ")" + x.CourseName,

            }).ToList();
            ViewBag.CourseId = new SelectList(courseLists, "Id", "CourseNumber", classteacher.CourseId);
            ViewBag.TeacherId = new SelectList(_db.Teacher.OrderBy(p => p.InitDate), "Id", "Name", classteacher.TeacherId);

            var places = _db.Place.OrderBy(p => p.InitDate).ToList();
            var placeLists = places.Select(x => new
            {
                Id = x.Id,
                PlaceNumberName = x.PlaceNumber + x.PlaceName,
                Type = x.category,
            }).ToList();
            ViewBag.PlaceId = new SelectList(placeLists.Where(x => x.Type != Category.宿舍), "Id", "PlaceNumberName", classteacher.PlaceId);
            ViewBag.DormRoom = new SelectList(placeLists.Where(x => x.Type == Category.宿舍), "Id", "PlaceNumberName", Convert.ToInt16(classteacher.DormRoom));

            return View(classteacher);
        }

        //
        // POST: /ClassTeacher/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]

        public ActionResult Edit(ClassTeacher classteacher, List<ClassTime> ClassTime, List<ClassTime> updateClassTime, List<int> removeClassTimes, List<int> checkaddbtn, ClassCycle ClassCycletrue)
        {
            if (ModelState.IsValid)
            {
                if (classteacher.ProvideDormitory == EnableType.否)
                {
                    classteacher.DormRoom = null;
                }
                classteacher.ClassCycle = ClassCycletrue;
                //_db.Entry(classteacher).State = EntityState.Modified;
                classteacher.Update();

                foreach (var item in updateClassTime)
                {
                    item.Update();
                }




                if (ClassTime != null)
                {
                    int count = 0;
                    List<ClassTime> addClassTimes = new List<ClassTime>();
                    foreach (var item in ClassTime)
                    {
                        bool addcheck = true;
                        if (checkaddbtn != null)
                        {
                            foreach (var items in checkaddbtn)
                            {
                                if (count == items)
                                {
                                    addcheck = false;
                                    break;
                                }
                            }
                        }
                        if (addcheck)
                        {
                            item.ClassTeacherId = classteacher.Id;
                            item.InitDate = classteacher.InitDate;
                            item.Poster = classteacher.Poster;
                            item.UpdateDate = classteacher.InitDate;
                            item.UpdateID = classteacher.Poster;
                            addClassTimes.Add(item);
                        }
                        count++;
                    }

                    foreach (var item in addClassTimes)
                    {
                        _db.ClassTimes.Add(item);
                    }

                    _db.SaveChanges();
                }
                if (removeClassTimes != null)
                {
                    foreach (var item in removeClassTimes)
                    {
                        ClassTime classTime = _db.ClassTimes.Find(item);
                        _db.ClassTimes.Remove(classTime);
                        _db.SaveChanges();
                    }
                }






                return RedirectToAction("Index", new { Page = -1 });
            }
            ViewBag.CourseId = new SelectList(_db.course.OrderBy(p => p.InitDate), "Id", "CourseNumber", classteacher.CourseId);
            ViewBag.TeacherId = new SelectList(_db.Teacher.OrderBy(p => p.InitDate), "Id", "Name", classteacher.TeacherId);
            ViewBag.PlaceId = new SelectList(_db.Place.OrderBy(p => p.InitDate), "Id", "PlaceNumber", classteacher.PlaceId);
            return View(classteacher);
        }

        //
        // GET: /ClassTeacher/Delete/5

        public ActionResult Delete(int id = 0)
        {
            ClassTeacher classteacher = _db.ClassTeachers.Find(id);
            if (classteacher == null)
            {
                return HttpNotFound();
            }
            return View(classteacher);
        }

        //
        // POST: /ClassTeacher/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ClassTeacher classteacher = _db.ClassTeachers.Find(id);
            _db.ClassTeachers.Remove(classteacher);
            _db.SaveChanges();
            return RedirectToAction("Index");
        }

        #region 暫時不用

        [HttpPost]
        public ActionResult CheckOK(string type, string day, string date, int teacher, int place, string provideDormitory, int dormroom, string workstrart, string workend)
        {

            JObject jObject = new JObject();
            string jsonContent;
            DateTime today = DateTime.Now.Date;
            var allclassteacher = _db.ClassTeachers.ToList();

            //判斷時間是否重疊
            var classteacher = allclassteacher.Where(x => x.StarDateTime >= today && x.EndDateTime < today && x.TeacherId == teacher).ToList();
            int startnum = Convert.ToInt16(workstrart.Replace(":", ""));
            int endnum = Convert.ToInt16(workend.Replace(":", ""));
            if (classteacher.Count != 0)
            {
                if (type == "每天")
                {

                    foreach (var item in classteacher)
                    {
                        foreach (var timeitem in item.ClassTimes)
                        {
                            int oldstartnum = Convert.ToInt16(timeitem.StarTime.Replace(":", ""));
                            int oldendnum = Convert.ToInt16(timeitem.EndTime.Replace(":", ""));
                            if ((startnum >= oldstartnum && startnum < oldendnum) ||
                                (endnum >= oldstartnum && endnum < oldendnum))
                            {
                                jObject.Add(new JProperty("Status", "Error"));
                                jObject.Add(new JProperty("Message", "此教師此時段已有課程在進行"));
                                jsonContent = JsonConvert.SerializeObject(jObject, Formatting.Indented);
                                return new ContentResult { Content = jsonContent, ContentType = "application/json" };
                            }
                        }
                    }
                }
                else if (type == "每周")
                {

                    foreach (var item in classteacher)
                    {
                        foreach (var timeitem in item.ClassTimes.Where(x => x.Day == day || x.ClassTeacher.ClassCycle == ClassCycle.平日每天))
                        {
                            int oldstartnum = Convert.ToInt16(timeitem.StarTime.Replace(":", ""));
                            int oldendnum = Convert.ToInt16(timeitem.EndTime.Replace(":", ""));
                            if ((startnum >= oldstartnum && startnum < oldendnum) ||
                                (endnum >= oldstartnum && endnum < oldendnum))
                            {
                                jObject.Add(new JProperty("Status", "Error"));
                                jObject.Add(new JProperty("Message", "此教師此時段已有課程在進行"));
                                jsonContent = JsonConvert.SerializeObject(jObject, Formatting.Indented);
                                return new ContentResult { Content = jsonContent, ContentType = "application/json" };
                            }
                        }
                    }
                }
                else
                {
                    DateTime dateTime = Convert.ToDateTime(date).Date;
                    string timeodweek = dateTime.DayOfWeek.ToString("d");
                    foreach (var item in classteacher)
                    {
                        foreach (var timeitem in item.ClassTimes.Where(x => x.Date == dateTime || x.Day == timeodweek || x.ClassTeacher.ClassCycle == ClassCycle.平日每天))
                        {
                            int oldstartnum = Convert.ToInt16(timeitem.StarTime.Replace(":", ""));
                            int oldendnum = Convert.ToInt16(timeitem.EndTime.Replace(":", ""));
                            if ((startnum >= oldstartnum && startnum < oldendnum) ||
                                (endnum >= oldstartnum && endnum < oldendnum))
                            {
                                jObject.Add(new JProperty("Status", "Error"));
                                jObject.Add(new JProperty("Message", "此教師此時段已有課程在進行"));
                                jsonContent = JsonConvert.SerializeObject(jObject, Formatting.Indented);
                                return new ContentResult { Content = jsonContent, ContentType = "application/json" };
                            }
                        }
                    }

                }
            }
            //判斷地點是否有人使用
            var classPlace = allclassteacher.Where(x => x.StarDateTime >= today && x.EndDateTime < today && x.PlaceId == place).ToList();

            if (classPlace.Count == 0)
            {

                if (type == "每天")
                {

                    foreach (var item in classPlace)
                    {
                        foreach (var timeitem in item.ClassTimes)
                        {
                            int oldstartnum = Convert.ToInt16(timeitem.StarTime.Replace(":", ""));
                            int oldendnum = Convert.ToInt16(timeitem.EndTime.Replace(":", ""));
                            if ((startnum >= oldstartnum && startnum < oldendnum) ||
                                (endnum >= oldstartnum && endnum < oldendnum))
                            {
                                jObject.Add(new JProperty("Status", "Error"));
                                jObject.Add(new JProperty("Message", "此地點該時段已被借用"));
                                jsonContent = JsonConvert.SerializeObject(jObject, Formatting.Indented);
                                return new ContentResult { Content = jsonContent, ContentType = "application/json" };
                            }
                        }
                    }
                }
                else if (type == "每周")
                {

                    foreach (var item in classPlace)
                    {
                        foreach (var timeitem in item.ClassTimes.Where(x => x.Day == day || x.ClassTeacher.ClassCycle == ClassCycle.平日每天))
                        {
                            int oldstartnum = Convert.ToInt16(timeitem.StarTime.Replace(":", ""));
                            int oldendnum = Convert.ToInt16(timeitem.EndTime.Replace(":", ""));
                            if ((startnum >= oldstartnum && startnum < oldendnum) ||
                                (endnum >= oldstartnum && endnum < oldendnum))
                            {
                                jObject.Add(new JProperty("Status", "Error"));
                                jObject.Add(new JProperty("Message", "此地點該時段已被借用"));
                                jsonContent = JsonConvert.SerializeObject(jObject, Formatting.Indented);
                                return new ContentResult { Content = jsonContent, ContentType = "application/json" };
                            }
                        }
                    }
                }
                else
                {
                    DateTime dateTime = Convert.ToDateTime(date).Date;
                    string timeodweek = dateTime.DayOfWeek.ToString("d");
                    foreach (var item in classteacher)
                    {
                        foreach (var timeitem in item.ClassTimes.Where(x => x.Date == dateTime || x.Day == timeodweek || x.ClassTeacher.ClassCycle == ClassCycle.平日每天))
                        {
                            int oldstartnum = Convert.ToInt16(timeitem.StarTime.Replace(":", ""));
                            int oldendnum = Convert.ToInt16(timeitem.EndTime.Replace(":", ""));
                            if ((startnum >= oldstartnum && startnum < oldendnum) ||
                                (endnum >= oldstartnum && endnum < oldendnum))
                            {
                                jObject.Add(new JProperty("Status", "Error"));
                                jObject.Add(new JProperty("Message", "此地點該時段已被借用"));
                                jsonContent = JsonConvert.SerializeObject(jObject, Formatting.Indented);
                                return new ContentResult { Content = jsonContent, ContentType = "application/json" };
                            }
                        }
                    }

                }
            }



























            jsonContent = JsonConvert.SerializeObject(jObject, Formatting.Indented);
            return new ContentResult { Content = jsonContent, ContentType = "application/json" };
        }


        #endregion
        [HttpPost]
        public ActionResult OpenClass(int id = 0)
        {
            JObject jObject = new JObject();
            string jsonContent;
            ClassTeacher classteacher = _db.ClassTeachers.Find(id);
            if (classteacher == null)
            {
                return HttpNotFound();
            }
            classteacher.ClassStatus = ClassStatus.確定開課;

            TimeZoneInfo TPZone = TimeZoneInfo.FindSystemTimeZoneById("Taipei Standard Time");
            DateTime NOWTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, TPZone);
            classteacher.UpdateID = User.Identity.Name;
            classteacher.UpdateDate = NOWTime;
            _db.SaveChanges();

            List<Attendance> addAttendances = new List<Attendance>();

            if (classteacher.ClassCycle == ClassCycle.平日每天)
            {
                int days = (classteacher.EndDateTime.Date - classteacher.StarDateTime.Date).Days;
                DateTime startdate = classteacher.StarDateTime.Date;
                for (int i = 0; i <= days; i++)
                {
                    if (i != 0)
                    {
                        startdate = startdate.AddDays(1);
                    }
                    string DayOfWeek = startdate.DayOfWeek.ToString("d");
                    if (DayOfWeek != "6" && DayOfWeek != "0")
                    {
                        foreach (var item in classteacher.ClassTimes)
                        {
                            ClassHistory classHistory = new ClassHistory();
                            classHistory.ClassTeacherId = classteacher.Id;
                            classHistory.ClassDate = startdate;
                            classHistory.Day = item.Day;
                            classHistory.StarTime = item.StarTime;
                            classHistory.EndTime = item.EndTime;
                            _db.ClassHistories.Add(classHistory);
                            classHistory.Create(_db, _db.ClassHistories);



                            foreach (var studentitem in classteacher.ClassStudentses.Where(x => x.ClassStatus == ApplyStatus.錄取))
                            {
                                Attendance attendance = new Attendance(); ;
                                attendance.ClassTeacherId = classteacher.Id;
                                attendance.StudentId = studentitem.StudentId;
                                attendance.ClassDate = startdate;
                                attendance.Day = item.Day;
                                attendance.StarTime = item.StarTime;
                                attendance.EndTime = item.EndTime;
                                attendance.Poster = classteacher.UpdateID;
                                attendance.InitDate = classteacher.UpdateDate;
                                attendance.UpdateID = classteacher.UpdateID;
                                attendance.UpdateDate = classteacher.UpdateDate;
                                addAttendances.Add(attendance);
                            }
                        }
                      
                    }

                }
            }
            else if (classteacher.ClassCycle == ClassCycle.每週)
            {

                int days = (classteacher.EndDateTime.Date - classteacher.StarDateTime.Date).Days;
                DateTime startdate = classteacher.StarDateTime.Date;
                for (int i = 0; i <= days; i++)
                {
                    if (i != 0)
                    {
                        startdate = startdate.AddDays(1);
                    }
                    string DayOfWeek = startdate.DayOfWeek.ToString("d");
                    if (classteacher.ClassTimes.Any(x => x.Day == DayOfWeek))
                    {
                        foreach (var item in classteacher.ClassTimes.Where(x => x.Day == DayOfWeek))
                        {
                            ClassHistory classHistory = new ClassHistory();
                            classHistory.ClassTeacherId = classteacher.Id;
                            classHistory.ClassDate = startdate;
                            classHistory.Day = item.Day;
                            classHistory.StarTime = item.StarTime;
                            classHistory.EndTime = item.EndTime;
                            _db.ClassHistories.Add(classHistory);
                            classHistory.Create(_db, _db.ClassHistories);
                            foreach (var studentitem in classteacher.ClassStudentses.Where(x => x.ClassStatus == ApplyStatus.錄取))
                            {
                                Attendance attendance = new Attendance(); ;
                                attendance.ClassTeacherId = classteacher.Id;
                                attendance.StudentId = studentitem.StudentId;
                                attendance.ClassDate = startdate;
                                attendance.Day = item.Day;
                                attendance.StarTime = item.StarTime;
                                attendance.EndTime = item.EndTime;
                                attendance.Poster = classteacher.UpdateID;
                                attendance.InitDate = classteacher.UpdateDate;
                                attendance.UpdateID = classteacher.UpdateID;
                                attendance.UpdateDate = classteacher.UpdateDate;
                                addAttendances.Add(attendance);
                            }
                        }

                    }

                }


            }
            if (classteacher.ClassCycle == ClassCycle.特定日期)
            {
                foreach (var item in classteacher.ClassTimes)
                {
                    ClassHistory classHistory = new ClassHistory();
                    classHistory.ClassTeacherId = classteacher.Id;
                    classHistory.ClassDate = item.Date.Value;
                    classHistory.StarTime = item.StarTime;
                    classHistory.EndTime = item.EndTime;
                    classHistory.Day = item.Day;
                    _db.ClassHistories.Add(classHistory);
                    classHistory.Create(_db, _db.ClassHistories);

                    foreach (var studentitem in classteacher.ClassStudentses.Where(x => x.ClassStatus == ApplyStatus.錄取))
                    {
                        string DayOfWeek = item.Date.Value.DayOfWeek.ToString("d");
                        Attendance attendance = new Attendance(); ;
                        attendance.ClassTeacherId = classteacher.Id;
                        attendance.StudentId = studentitem.StudentId;
                        attendance.ClassDate = item.Date.Value;
                        attendance.Day = DayOfWeek;
                        attendance.StarTime = item.StarTime;
                        attendance.EndTime = item.EndTime;
                        attendance.Poster = classteacher.UpdateID;
                        attendance.InitDate = classteacher.UpdateDate;
                        attendance.UpdateID = classteacher.UpdateID;
                        attendance.UpdateDate = classteacher.UpdateDate;
                        addAttendances.Add(attendance);
                    }
                }
            }
            if (addAttendances.Count > 0)
            {
                foreach (var item in addAttendances)
                {
                    _db.Attendances.Add(item);

                }

                _db.SaveChanges();
            }

            jObject.Add(new JProperty("Message", "Success"));
            jsonContent = JsonConvert.SerializeObject(jObject, Formatting.Indented);
            return new ContentResult { Content = jsonContent, ContentType = "application/json" };
        }



        public ActionResult AttendanceList(int? page, FormCollection fc, int id = 0)
        {
            //記住搜尋條件
            GetCatcheRoutes(page, fc);

            //取得正確的頁面
            int currentPageIndex = getCurrentPage(page, fc);


            var classHistories = _db.ClassHistories.Where(x => x.ClassTeacherId == id).OrderByDescending(p => p.InitDate).AsQueryable();
            List<ClassDateList> classDateLists=new List<ClassDateList>();
            var classdates = _db.ClassHistories.Where(x => x.ClassTeacherId == id).OrderBy(p => p.ClassDate).ThenBy(x=>x.StarTime).ToList();

            foreach (var item in classdates)
            {
                string dayofweek = "";
                switch (item.Day)
                {
                    case "0":
                        dayofweek = "日";
                        break;
                    case "1":
                        dayofweek = "一";
                        break;
                    case "2":
                        dayofweek = "二";
                        break;
                    case "3":
                        dayofweek = "三";
                        break;
                    case "4":
                        dayofweek = "四";
                        break;
                    case "5":
                        dayofweek = "五";
                        break;
                    case "6":
                        dayofweek = "六";
                        break;
                }
                ClassDateList classDate=new ClassDateList();
                classDate.Id = item.Id;
                classDate.Date = item.ClassDate.ToString("yyyy/MM/dd") + "(" + dayofweek + ") " + item.StarTime + "~" +
                                 item.EndTime;
                classDateLists.Add(classDate);
            }



            ViewBag.classdates = new SelectList(classDateLists, "Id", "Date");

            if (hasViewData("SearchByDate"))
            {
                int searchByDate = getViewDateInt("SearchByDate");
            
                classHistories = classHistories.Where(w => w.Id == searchByDate);
            }


            return View(classHistories.OrderBy(p => p.ClassDate).ToPagedList(currentPageIndex, DefaultPageSize));
        }

        public ActionResult EditAttendance(int id)
        {
            var classHistory = _db.ClassHistories.Find(id);
            var attendances = _db.Attendances.Where(x =>
                x.ClassDate == classHistory.ClassDate && x.StarTime == classHistory.StarTime &&
                x.EndTime == classHistory.EndTime && x.ClassTeacherId == classHistory.ClassTeacherId).ToList();

            return View(attendances);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditAttendance(List<Attendance> attendances)
        {
            foreach (var item in attendances)
            {
                item.Update();
            }

            return RedirectToAction("AttendanceList",new {id= attendances.First().ClassTeacherId});
        }




        public ActionResult EditClassHistory(int id)
        {
            var classHistory = _db.ClassHistories.Find(id);
          

            return View(classHistory);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditClassHistory(ClassHistory classhistory)
        {
            classhistory.Update();

            return RedirectToAction("AttendanceList", new { id = classhistory.ClassTeacherId });
        }

        public ActionResult CreateScore(int id)
        {
            var classhistory = _db.ClassHistories.Find(id);
            ViewBag.students = _db.ClassStudents.Where(x => x.ClassTeacherId == classhistory.ClassTeacherId).ToList();
            ViewBag.classHistoryId = id;
            ViewBag.classteacherId =classhistory.ClassTeacherId;
            ViewBag.Course = classhistory.ClassDate.ToString("yyyy/MM/dd") + classhistory.ClassTeacher.Course.CourseName;
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateScore(List<Score> Score, int ClassTeacherId)
        {
            //_db.ClassTeachers.Add(classteacher);
            //classteacher.Create(_db, _db.ClassTeachers);
            List<Score> scores=new List<Score>();
            int parentscoreid = 0;
            string Poster ="";
            DateTime InitDate = DateTime.Now;
            foreach (var item in Score)
            {
                if (item.ClassHistoryId.HasValue)
                {
                    _db.Scores.Add(item);
                    item.Create(_db,_db.Scores);
                    parentscoreid = item.Id;
                    Poster = item.Poster;
                    InitDate = item.InitDate.Value;
                }
                else
                {
                    item.ScoreId = parentscoreid;
                    item.Poster = Poster;
                    item.InitDate = InitDate;
                    scores.Add(item);
                }
            }
            foreach (var item in scores)
            {
                _db.Scores.Add(item);
            }

            _db.SaveChanges();


            return RedirectToAction("AttendanceList", new { id = ClassTeacherId });
        }

        public ActionResult EditScore(int id)
        {
            var score = _db.Scores.Where(x => x.ClassHistoryId == id).ToList();
        var students = _db.Student.Where(x => x.Scores.Any(y => y.ParentScoreId.ClassHistoryId == id)).ToList();
        ViewBag.students = students;
        ViewBag.ClassTeacherId = score.FirstOrDefault(x => x.ClassHistoryId.HasValue).ClassHistory.ClassTeacherId;
            return View(score);
        }




        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditScore(List<Score> Score, List<Score> AddScore,int ClassTeacherId)
        {

            foreach (var item in Score)
            {
                item.Update();
            }

            List<Score> scoredetails = new List<Score>();

            int parentscoreid = 0;
            string Poster = "";
            DateTime InitDate = DateTime.Now;
            foreach (var item in AddScore)
            {
                if (item.ClassHistoryId.HasValue)
                {
                    _db.Scores.Add(item);
                    item.Create(_db, _db.Scores);
                    parentscoreid = item.Id;
                    Poster = item.Poster;
                    InitDate = item.InitDate.Value;
                }
                else
                {
                    item.ScoreId = parentscoreid;
                    item.Poster = Poster;
                    item.InitDate = InitDate;
                    scoredetails.Add(item);
                }
            }
            foreach (var item in scoredetails)
            {
                _db.Scores.Add(item);
            }

            _db.SaveChanges();
            return RedirectToAction("AttendanceList", new { id = ClassTeacherId });
        }



        protected override void Dispose(bool disposing)
        {
            _db.Dispose();
            base.Dispose(disposing);
        }
    }

}
