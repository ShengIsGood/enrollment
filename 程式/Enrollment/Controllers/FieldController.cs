﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using MvcPaging;
using System.Web.Mvc;
using Enrollment.Filters;
using Enrollment.Models;

namespace Enrollment.Controllers
{
	[PermissionFilters]
    [Authorize]
    public class FieldController : _BaseController
    {
        private BackendContext _db = new BackendContext();
        private const int DefaultPageSize = 15;
        //

        


        public ActionResult Index(int? page, FormCollection fc )
        {
			//記住搜尋條件
            GetCatcheRoutes(page, fc);

            //取得正確的頁面
            int currentPageIndex = getCurrentPage(page, fc);
            

            var field = _db.Field.OrderByDescending(p => p.InitDate).AsQueryable();
            if (hasViewData("SearchByFieldName"))
            {
                string searchByFieldName = getViewDateStr("SearchByFieldName");
                field = field.Where(w => w.FieldName.Contains(searchByFieldName));
            }

            return View(field.OrderByDescending(p => p.InitDate).ToPagedList(currentPageIndex, DefaultPageSize));

        }



        

        //
        // GET: /Field/Details/5

        public ActionResult Details(int id = 0)
        {
            Field field = _db.Field.Find(id);
            if (field == null)
            {
                //return HttpNotFound();
				 return View();
            }
            return View(field);
        }

        //
        // GET: /Field/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Field/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public ActionResult Create(Field field )
        {
            if (ModelState.IsValid)
            {

                _db.Field.Add(field);
                field.Create(_db,_db.Field);
                return RedirectToAction("Index");
            }

            return View(field);
        }

        //
        // GET: /Field/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Field field = _db.Field.Find(id);
            if (field == null)
            {
                return HttpNotFound();
            }
            return View(field);
        }

        //
        // POST: /Field/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
         
        public ActionResult Edit(Field field)
        {
            if (ModelState.IsValid)
            {

               //_db.Entry(field).State = EntityState.Modified;
                field.Update();
                return RedirectToAction("Index",new{Page=-1});
            }
            return View(field);
        }

        //
        // GET: /Field/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Field field = _db.Field.Find(id);
            if (field == null)
            {
                return HttpNotFound();
            }
            return View(field);
        }

        //
        // POST: /Field/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Field field = _db.Field.Find(id);
            _db.Field.Remove(field);
            _db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            _db.Dispose();
            base.Dispose(disposing);
        }
    }

}
