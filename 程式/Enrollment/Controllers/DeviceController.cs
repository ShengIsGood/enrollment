﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using MvcPaging;
using System.Web.Mvc;
using Enrollment.Filters;
using Enrollment.Models;

namespace Enrollment.Controllers
{
	[PermissionFilters]
    [Authorize]
    public class DeviceController : _BaseController
    {
        private BackendContext _db = new BackendContext();
        private const int DefaultPageSize = 15;
        //

        


        public ActionResult Index(int? page, FormCollection fc )
        {
			//記住搜尋條件
            GetCatcheRoutes(page, fc);

            //取得正確的頁面
            int currentPageIndex = getCurrentPage(page, fc);
            

            var devices = _db.Devices.Include(d => d.DeviceCategory).OrderByDescending(p => p.InitDate).AsQueryable();
            ViewBag.DeviceCategoryId = new SelectList(_db.DeviceCategories.Where(x => x.ParentId.HasValue).OrderBy(p => p.SortNum), "Id", "Subject");
            if (hasViewData("SearchByDeviceCategoryId")) 
            { 
            int searchByDeviceCategoryId = getViewDateInt("SearchByDeviceCategoryId");             
                devices = devices.Where(w => w.DeviceCategoryId == searchByDeviceCategoryId); 
            } 
            if (hasViewData("SearchBySubject")) 
            { 
            string searchBySubject = getViewDateStr("SearchBySubject");             
                devices = devices.Where(w => w.Subject.Contains(searchBySubject)); 
            }
            if (hasViewData("SearchByDeviceStatus"))
            {
                string DeviceStatus = getViewDateStr("SearchByDeviceStatus");
                Enrollment.Models.DeviceStatus searchByDeviceStatus = (Enrollment.Models.DeviceStatus)Enum.Parse(typeof(Enrollment.Models.DeviceStatus), DeviceStatus, false);

                devices = devices.Where(w => w.DeviceStatus == searchByDeviceStatus);
            }
            if (hasViewData("SearchByDeviceRentStatus"))
            {
                string DeviceRentStatus = getViewDateStr("searchByDeviceRentStatus");
                Enrollment.Models.DeviceRentStatus searchByDeviceRentStatus = (Enrollment.Models.DeviceRentStatus)Enum.Parse(typeof(Enrollment.Models.DeviceRentStatus), DeviceRentStatus, false);

                devices = devices.Where(w => w.DeviceRentStatus == searchByDeviceRentStatus);
            }

            //            ViewBag.Subject = Subject;
            return View(devices.OrderByDescending(p => p.InitDate).ToPagedList(currentPageIndex, DefaultPageSize));

        }



        

        //
        // GET: /Device/Details/5

        public ActionResult Details(int id = 0)
        {
            Device device = _db.Devices.Find(id);
            if (device == null)
            {
                //return HttpNotFound();
				 return View();
            }
            return View(device);
        }

        //
        // GET: /Device/Create

        public ActionResult Create()
        {
            ViewBag.DeviceCategoryId = new SelectList(_db.DeviceCategories.Where(x=>x.ParentId.HasValue).OrderBy(p=>p.SortNum), "Id", "Subject");
            return View();
        }

        //
        // POST: /Device/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public ActionResult Create(Device device )
        {
            if (ModelState.IsValid)
            {

                _db.Devices.Add(device);
                device.Create(_db,_db.Devices);
                return RedirectToAction("Index");
            }
            ViewBag.DeviceCategoryId = new SelectList(_db.DeviceCategories.Where(x => x.ParentId.HasValue).OrderBy(p => p.SortNum), "Id", "Subject", device.DeviceCategoryId);


            return View(device);
        }

        //
        // GET: /Device/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Device device = _db.Devices.Find(id);
            if (device == null)
            {
                return HttpNotFound();
            }
            ViewBag.DeviceCategoryId = new SelectList(_db.DeviceCategories.Where(x => x.ParentId.HasValue).OrderBy(p => p.SortNum), "Id", "Subject", device.DeviceCategoryId);

            return View(device);
        }

        //
        // POST: /Device/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
         
        public ActionResult Edit(Device device)
        {
            if (ModelState.IsValid)
            {

               //_db.Entry(device).State = EntityState.Modified;
                device.Update();
                return RedirectToAction("Index",new{Page=-1});
            }
            ViewBag.DeviceCategoryId = new SelectList(_db.DeviceCategories.Where(x => x.ParentId.HasValue).OrderBy(p => p.SortNum), "Id", "Subject", device.DeviceCategoryId);

        
            return View(device);
        }

        //
        // GET: /Device/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Device device = _db.Devices.Find(id);
            if (device == null)
            {
                return HttpNotFound();
            }
            return View(device);
        }

        //
        // POST: /Device/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Device device = _db.Devices.Find(id);
            _db.Devices.Remove(device);
            _db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            _db.Dispose();
            base.Dispose(disposing);
        }
    }

}
