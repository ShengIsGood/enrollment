﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using Enrollment.Models;
using Newtonsoft.Json;

namespace Enrollment.Controllers
{
    public class SSOController : Controller
    {
        private BackendContext db = new BackendContext();
        //
        // GET: /SSO/

        public ActionResult Index(string username)
        {
     
            ViewBag.username = username;
            return View();
        }

        [HttpPost, ActionName("Index")]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Login(string username)
        {
            Thread.Sleep(5000);
            Member member = db.Members.First(x => x.Account == username);
            if (member != null)
            {
                Utility.GetPerssion(member);
                string userData = JsonConvert.SerializeObject(member);
                Utility.SetAuthenTicket(userData, username);

                return Content("Success");
            }
            return Content("Success");

        }
    }
}
