﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web;
using MvcPaging;
using System.Web.Mvc;
using Enrollment.Filters;
using Enrollment.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Enrollment.Controllers
{
    [PermissionFilters]
    [Authorize]
    public class StudentController : _BaseController
    {
        private BackendContext _db = new BackendContext();
        private const int DefaultPageSize = 15;
        //




        public ActionResult Index(int? page, FormCollection fc)
        {
            //記住搜尋條件
            GetCatcheRoutes(page, fc);

            //取得正確的頁面
            int currentPageIndex = getCurrentPage(page, fc);


            var student = _db.Student.OrderByDescending(p => p.InitDate).AsQueryable();
            if (hasViewData("SearchByName"))
            {
                string searchByName = getViewDateStr("SearchByName");
                student = student.Where(w => w.Name.Contains(searchByName));
            }

            if (hasViewData("SearchByGender"))
            {
                string Gender = getViewDateStr("SearchByGender");
                Enrollment.Models.GenderType searchByGender = (Enrollment.Models.GenderType)Enum.Parse(typeof(Enrollment.Models.GenderType), Gender, false);

                student = student.Where(w => w.Gender == searchByGender);
            }


            //            ViewBag.Name = Name;
            return View(student.OrderByDescending(p => p.InitDate).ToPagedList(currentPageIndex, DefaultPageSize));

        }





        //
        // GET: /Student/Details/5

        public ActionResult Details(int id = 0)
        {
            Student student = _db.Student.Find(id);
            if (student == null)
            {
                //return HttpNotFound();
                return View();
            }
            return View(student);
        }

        //
        // GET: /Student/Create

        public ActionResult Create()
        {
            return View();
        }



        // POST: /Student/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Create(Student student, HttpPostedFileBase FileUpload, List<int> checkaddbtn, List<License> Licenselist)
        {
            if (ModelState.IsValid)
            {
                string fileName = "";

                if (FileUpload != null)
                {
                    fileName = FileUpload.FileName;
                    student.FileUrl = fileName;
                }

                student.PasswordSalt = Utility.CreateSalt();
                student.Password = Utility.GenerateHashWithSalt(student.Password, student.PasswordSalt);

                _db.Student.Add(student);
                student.Create(_db, _db.Student);

                if (FileUpload != null)
                {
                    string docupath = Request.PhysicalApplicationPath + "upfiles\\Student\\" + student.Id + "\\";
                    if (!Directory.Exists(docupath))
                    {
                        Directory.CreateDirectory(@docupath);
                    }

                    string filePath = Server.MapPath("~/upfiles/Student/" + student.Id);



                    FileUpload.SaveAs(Path.Combine(filePath, fileName));
                }

                if (Licenselist != null)
                {
                    int count = 0;
                    List<License> addLicense = new List<License>();
                    foreach (var item in Licenselist)
                    {
                        bool addcheck = true;
                        if (checkaddbtn != null)
                        {
                            foreach (var items in checkaddbtn)
                            {
                                if (count == items)
                                {
                                    addcheck = false;
                                    break;
                                }
                            }
                        }

                        if (addcheck)
                        {
                            item.StudentFK = student.Id;
                            item.InitDate = student.InitDate;
                            item.Poster = student.Poster;
                            item.UpdateDate = student.InitDate;
                            item.UpdateID = student.Poster;

                            addLicense.Add(item);
                        }

                        count++;
                    }
                    foreach (var item in addLicense)
                    {
                        _db.Licenses.Add(item);
                    }

                    _db.SaveChanges();
                }


                return RedirectToAction("Index");
            }

            return View(student);
        }

        public ActionResult Edit(int id, string message)
        {
            Student student = _db.Student.Find(id);
            if (student == null)
            {
                return HttpNotFound();
            }

            ViewBag.Message = message;
            return View(student);
        }


        // POST: /Student/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Edit(Student student, HttpPostedFileBase upfile, string remove, List<License> Licenselist, List<License> updateLicenses, List<int> removeLicenses, List<int> checkaddbtn)
        {

            //移除驗證
            ModelState.Remove("Account");
            ModelState.Remove("Password");
            student.Password = Request["NewPassword"] != "" ? Utility.GenerateHashWithSalt(Request["NewPassword"], student.PasswordSalt) : Request["hash"];

            if (ModelState.IsValid)
            {
                if (!string.IsNullOrEmpty(remove) && !remove.Equals(student.FileUrl))
                {
                    ViewBag.Message = "刪除檔案發生錯誤！";
                    return View(student);
                }

                if (!string.IsNullOrEmpty(student.FileUrl))
                {
                    if (upfile != null && string.IsNullOrEmpty(remove))
                    {
                        ViewBag.Message = "只能存在一份檔案，請先將原本的檔案刪除!";
                        return View(student);
                    }
                }

                if (!string.IsNullOrEmpty(remove))
                {
                    System.IO.File.Delete(Server.MapPath("~/upfiles/Student/" + student.Id + "/") + remove);
                    student.FileUrl = "";
                }

                if (upfile != null)
                {
                    string fileName = upfile.FileName;
                    string docupath = Request.PhysicalApplicationPath + "upfiles\\Student\\" + student.Id + "\\";
                    if (!Directory.Exists(docupath))
                    {
                        Directory.CreateDirectory(@docupath);
                    }

                    string filePath = Server.MapPath("~/upfiles/Student/" + student.Id);


                    upfile.SaveAs(Path.Combine(filePath, fileName));

                    student.FileUrl = fileName;
                }

                //_db.Entry(products).State = EntityState.Modified;
                student.Update();



                foreach (var item in updateLicenses)
                {
                    item.Update();
                }
                if (Licenselist != null)
                {
                    int count = 0;
                    List<License> addLicense = new List<License>();
                    foreach (var item in Licenselist)
                    {
                        bool addcheck = true;
                        if (checkaddbtn != null)
                        {
                            foreach (var items in checkaddbtn)
                            {
                                if (count == items)
                                {
                                    addcheck = false;
                                    break;
                                }
                            }
                        }
                        if (addcheck)
                        {
                            item.StudentFK = student.Id;
                            item.InitDate = student.InitDate;
                            item.Poster = student.Poster;
                            item.UpdateDate = student.InitDate;
                            item.UpdateID = student.Poster;
                            addLicense.Add(item);
                        }
                        count++;
                    }

                    foreach (var item in addLicense)
                    {
                        _db.Licenses.Add(item);
                    }

                    _db.SaveChanges();
                }
                if (removeLicenses != null)
                {
                    foreach (var item in removeLicenses)
                    {
                        License license = _db.Licenses.Find(item);
                        _db.Licenses.Remove(license);
                        _db.SaveChanges();
                    }
                }




                return RedirectToAction("Index", new { page = -1 });
            }

            return RedirectToAction("Edit", new { id = student.Id, message = "您輸入的資料有誤，請重新確認" });
        }

        private string Delfile(string productInfoFile, string removefile, string location)
        {
            productInfoFile = "#" + productInfoFile;
            string[] removearr = removefile.Split('#');
            foreach (var item in removearr)
            {
                productInfoFile = productInfoFile.Replace("#" + item + "#", "#");
                System.IO.File.Delete(Server.MapPath(location) + item);
            }

            return productInfoFile.TrimStart('#');
        }

        //
        // GET: /Student/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Student student = _db.Student.Find(id);
            if (student == null)
            {
                return HttpNotFound();
            }
            return View(student);
        }

        //
        // POST: /Student/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Student student = _db.Student.Find(id);
            string filename = student.FileUrl;
            if (!string.IsNullOrEmpty(filename))
            {
                //System.IO.File.Delete(Server.MapPath("~/upfiles/Student/" + student.Name + "/") + filename);
                //System.IO.File.Delete(Server.MapPath("~/upfiles/Student/" + student.Id));
                Directory.Delete(Server.MapPath("~/upfiles/Student/" + student.Id), true);
            }

            _db.Student.Remove(student);
            _db.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult GetFile(string dir, string filename, string type)
        {
            if (string.IsNullOrEmpty(dir))
            {
                return File(Server.MapPath("~/upfiles/" + type + "/" + filename), MimeMapping.GetMimeMapping(filename), filename);

            }
            return File(Server.MapPath("~/upfiles/" + type + "/" + dir + "/" + filename), MimeMapping.GetMimeMapping(filename), filename);
        }

        public ActionResult CheckAccount(string userName)
        {
            if (string.IsNullOrEmpty(userName))
            {
                return Content("參數錯誤");
            }
            Student student = _db.Student.SingleOrDefault(o => o.Account == userName);
            if (student == null)
            {
                return Content("這個帳號尚未使用!");
            }
            return Content("這個帳號已使用!");
        }













        public ActionResult NewCreate()
        {
            return View();
        }



        // POST: /Student/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult NewCreate(Student student, HttpPostedFileBase FileUpload)
        {
            if (ModelState.IsValid)
            {
                string fileName = "";

                if (FileUpload != null)
                {
                    fileName = FileUpload.FileName;
                    student.FileUrl = fileName;
                }

                student.PasswordSalt = Utility.CreateSalt();
                student.Password = Utility.GenerateHashWithSalt(student.Password, student.PasswordSalt);

                _db.Student.Add(student);
                student.Create(_db, _db.Student);

                if (FileUpload != null)
                {
                    string docupath = Request.PhysicalApplicationPath + "upfiles\\Student\\" + student.Id + "\\";
                    if (!Directory.Exists(docupath))
                    {
                        Directory.CreateDirectory(@docupath);
                    }

                    string filePath = Server.MapPath("~/upfiles/Student/" + student.Id);



                    FileUpload.SaveAs(Path.Combine(filePath, fileName));
                }



                return RedirectToAction("NewEdit", "Student", new { Id = student.Id });

            }

            return View(student);
        }

        public ActionResult NewEdit(int id, string tab)
        {
            Student student = _db.Student.Find(id);
            if (student == null)
            {
                return HttpNotFound();
            }
            ViewBag.tab = tab;

            return View(student);
        }


        [HttpPost]
        public ActionResult CreateOrEditLicense(License license,string type, string tab)
        {
            ModelState.Remove("Id");
            if (ModelState.IsValid)
            {
                if (type == "Create")
                {
                    _db.Licenses.Add(license);
                    license.Create(_db, _db.Licenses);
                }
                else
                {
                    license.Update();
                }
            

            }

            ViewBag.tab = tab;
            return RedirectToAction("NewEdit", "Student", new { Id = license.StudentFK, tab = tab });
        }


        public ActionResult EditLicense(int id, string tab)
        {
            License license = _db.Licenses.Find(id);

            ViewBag.tab = tab;
            return View(license);
        }

        public ActionResult SearchLicense(int id, string tab)
        {
            JObject jObject = new JObject();
            string jsonContent;
            License license = _db.Licenses.Find(id);
      
            jObject.Add(new JProperty("LicenseId", license.LicenseId));
            jObject.Add(new JProperty("ChineseName", license.ChineseName));
            jObject.Add(new JProperty("EnglishName", license.EnglishName));
            jObject.Add(new JProperty("LicenseCategory", license.LicenseCategory));
            jObject.Add(new JProperty("LicenseUnit",license.LicenseUnit));
            jObject.Add(new JProperty("IssueDate", license.IssueDate.Value.ToString("yyyy/MM/dd")));
            jObject.Add(new JProperty("EffectiveDate", license.EffectiveDate.Value.ToString("yyyy/MM/dd")));
            jObject.Add(new JProperty("Poster", license.Poster));
            jObject.Add(new JProperty("InitDate", license.InitDate));
            jsonContent = JsonConvert.SerializeObject(jObject, Formatting.Indented);
            return new ContentResult { Content = jsonContent, ContentType = "application/json" };
        }








        protected override void Dispose(bool disposing)
        {
            _db.Dispose();
            base.Dispose(disposing);
        }
    }

}
