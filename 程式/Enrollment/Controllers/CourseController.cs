﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web;
using MvcPaging;
using System.Web.Mvc;
using Enrollment.Filters;
using Enrollment.Models;

namespace Enrollment.Controllers
{
	[PermissionFilters]
    [Authorize]
    public class CourseController : _BaseController
    {
        private BackendContext _db = new BackendContext();
        private const int DefaultPageSize = 15;
        //

        


        public ActionResult Index(int? page, FormCollection fc )
        {
			//記住搜尋條件
            GetCatcheRoutes(page, fc);

            //取得正確的頁面
            int currentPageIndex = getCurrentPage(page, fc);
            

            var course = _db.course.OrderByDescending(p => p.InitDate).AsQueryable();

            if (hasViewData("SearchByCourseNumber"))
            {
                string searchByCourseNumber = getViewDateStr("SearchByCourseNumber");
                course = course.Where(w => w.CourseNumber.Contains(searchByCourseNumber));
            }

            if (hasViewData("SearchByCourseName"))
            {
                string searchByCourseName = getViewDateStr("SearchByCourseName");
              
                course = course.Where(w => w.CourseName == searchByCourseName);
            }
            return View(course.OrderByDescending(p => p.InitDate).ToPagedList(currentPageIndex, DefaultPageSize));

        }



        

        //
        // GET: /Coursess/Details/5

        public ActionResult Details(int id = 0)
        {
            course course = _db.course.Find(id);
            if (course == null)
            {
                //return HttpNotFound();
				 return View();
            }
            return View(course);
        }

        //
        // GET: /Coursess/Create

        public ActionResult Create()
        {
            var field = _db.Field.ToList();
            if (field.Count >0)
            {
                ViewBag.fieldlist=new SelectList(field,"Id", "FieldName");
            }

            return View();
        }

        //
        // POST: /Coursess/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Create(course course,int[] fieldlist, HttpPostedFileBase file)
        {
            if (ModelState.IsValid)
            {
                if (file!=null)
                {
                    string extensionName = file.FileName.Substring(file.FileName.LastIndexOf('.') + 1).ToLower();

                    string FileName = DateTime.Now.ToString("yyyyMMdd") + course.CourseName + "." + extensionName;

                    string docupath = Request.PhysicalApplicationPath + "upfiles\\materials\\";

                    if (!Directory.Exists(docupath))
                    {
                        Directory.CreateDirectory(@docupath);
                    }

                    string filePath = Server.MapPath("~/upfiles/materials/");

                    file.SaveAs(Path.Combine(filePath, FileName));

                    course.UploadMaterials= FileName;

                }

                _db.course.Add(course);
                course.Create(_db,_db.course);
                TimeZoneInfo TPZone = TimeZoneInfo.FindSystemTimeZoneById("Taipei Standard Time");
                DateTime NOWTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, TPZone);
                foreach (var item in fieldlist)
                {
                    CourseAssociation courseAssociation = new CourseAssociation();
                    courseAssociation.CourseFK = course.Id;
                    courseAssociation.FieldFK = item;
                    courseAssociation.Poster = User.Identity.Name;
                    courseAssociation.InitDate = NOWTime;
                     _db.CourseAssociation.Add(courseAssociation);
                    
                }

                _db.SaveChanges();
             
                return RedirectToAction("Index");
            }

            return View(course);
        }

        //
        // GET: /Coursess/Edit/5

        public ActionResult Edit(int id = 0)
        {
            course course = _db.course.Find(id);
            if (course == null)
            {
                return HttpNotFound();
            }
            var field = _db.Field.ToList();
            if (field.Count > 0)
            {
                ViewBag.fieldlist = new SelectList(field, "Id", "FieldName");
            }

            string selectedlist = "";
            foreach (var item in course.CourseAssociation)
            {
                selectedlist += item.FieldFK+",";
            }

            ViewBag.selectedlist = selectedlist.TrimEnd(',');
            return View(course);
        }

        //
        // POST: /Coursess/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
         [ValidateInput(false)]
        public ActionResult Edit(course course, int[] fieldlist, HttpPostedFileBase file)
        {
            if (ModelState.IsValid)
            {
                if (file != null)
                {
                    string extensionName = file.FileName.Substring(file.FileName.LastIndexOf('.') + 1).ToLower();
                    string FileName = DateTime.Now.ToString("yyyyMMdd") + course.CourseName + "." + extensionName;
                    string docupath = Request.PhysicalApplicationPath + "upfiles\\materials\\";

                    if (!Directory.Exists(docupath))
                    {
                        Directory.CreateDirectory(@docupath);
                    }

                    string filePath = Server.MapPath("~/upfiles/materials/");

                    file.SaveAs(Path.Combine(filePath, FileName));

                    course.UploadMaterials = FileName;

                }

                //_db.Entry(course).State = EntityState.Modified;
                course.Update();
                return RedirectToAction("Index",new{Page=-1});
            }
            return View(course);
        }

        //
        // GET: /Coursess/Delete/5

        public ActionResult Delete(int id = 0)
        {
            course course = _db.course.Find(id);
            if (course == null)
            {
                return HttpNotFound();
            }
            return View(course);
        }

        //
        // POST: /Coursess/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            course course = _db.course.Find(id);
            _db.course.Remove(course);
            _db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            _db.Dispose();
            base.Dispose(disposing);
        }
    }

}
