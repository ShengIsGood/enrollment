﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Enrollment.Filters;

namespace Enrollment.Controllers
{
    [PermissionFilters]
    [Authorize]
    public class LecturerController : Controller
    {
        //
        // GET: /Lecturer/

        public ActionResult Teaching()
        {
            return View();
        }
        public ActionResult Training()
        {
            return View();
        }
        public ActionResult Hours()
        {
            return View();
        }
        public ActionResult Audit()
        {
            return View();
        }
        public ActionResult CourseMaterials()
        {
            return View();
        }
        public ActionResult Assessment()
        {
            return View();
        }

    }
}