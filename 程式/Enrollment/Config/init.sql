﻿SET IDENTITY_INSERT [dbo].[Roles] ON 
INSERT [dbo].[Roles] ([Id], [Subject], [Permission], [Alias], [Poster], [InitDate], [UpdateID], [UpdateDate]) VALUES (2, N'總管理者', N'A01,A01-1,A01-2,A01-3,A01-4', NULL, N'admin', CAST(N'2019-04-23T15:42:35.000' AS DateTime), NULL, CAST(N'2019-04-29T14:46:04.643' AS DateTime))
SET IDENTITY_INSERT [dbo].[Roles] OFF
SET IDENTITY_INSERT [dbo].[Menus] ON 
INSERT [dbo].[Menus] ([Id], [Subject], [PrivilegeCode], [HaveDetails], [TopMenuId], [IsPlugIn], [ControllerName], [ActionName], [Url], [LoginUrl], [IsWork], [SortNum], [Poster], [InitDate], [UpdateID], [UpdateDate]) VALUES (1, N'系統設定', N'A01', 0, NULL, 1, NULL, NULL, NULL, NULL, 0, 1, N'admin', CAST(N'2019-05-24T16:38:55.133' AS DateTime), N'admin', CAST(N'2019-05-24T16:38:55.133' AS DateTime))
INSERT [dbo].[Menus] ([Id], [Subject], [PrivilegeCode], [HaveDetails], [TopMenuId], [IsPlugIn], [ControllerName], [ActionName], [Url], [LoginUrl], [IsWork], [SortNum], [Poster], [InitDate], [UpdateID], [UpdateDate]) VALUES (2, N'帳號管理', N'A01-1', 1, 1, 1, N'Member', N'Index', NULL, NULL, 0, 0, N'admin', CAST(N'2019-05-24T16:39:15.243' AS DateTime), N'admin', CAST(N'2019-05-24T16:39:15.243' AS DateTime))
INSERT [dbo].[Menus] ([Id], [Subject], [PrivilegeCode], [HaveDetails], [TopMenuId], [IsPlugIn], [ControllerName], [ActionName], [Url], [LoginUrl], [IsWork], [SortNum], [Poster], [InitDate], [UpdateID], [UpdateDate]) VALUES (3, N'權限控管維護', N'A01-2', 1, 1, 1, N'Role', N'Index', NULL, NULL, 0, 1, N'admin', CAST(N'2019-05-24T16:39:39.047' AS DateTime), N'admin', CAST(N'2019-05-24T16:39:39.047' AS DateTime))
INSERT [dbo].[Menus] ([Id], [Subject], [PrivilegeCode], [HaveDetails], [TopMenuId], [IsPlugIn], [ControllerName], [ActionName], [Url], [LoginUrl], [IsWork], [SortNum], [Poster], [InitDate], [UpdateID], [UpdateDate]) VALUES (4, N'參數設定', N'A01-3', 1, 1, 1, N'VariableSetting', N'Index', NULL, NULL, 0, 2, N'admin', CAST(N'2019-05-24T16:40:02.760' AS DateTime), N'admin', CAST(N'2019-05-24T16:40:02.760' AS DateTime))
INSERT [dbo].[Menus] ([Id], [Subject], [PrivilegeCode], [HaveDetails], [TopMenuId], [IsPlugIn], [ControllerName], [ActionName], [Url], [LoginUrl], [IsWork], [SortNum], [Poster], [InitDate], [UpdateID], [UpdateDate]) VALUES (5, N'選單管理', N'A01-4', 1, 1, 1, N'Menu', N'Index', NULL, NULL, 0, 3, N'admin', CAST(N'2019-05-24T16:40:20.110' AS DateTime), N'admin', CAST(N'2019-05-24T16:40:20.110' AS DateTime))
SET IDENTITY_INSERT [dbo].[Menus] OFF
SET IDENTITY_INSERT [dbo].[RoleMenus] ON 
INSERT [dbo].[RoleMenus] ([Id], [RoleId], [MenuId], [Poster], [InitDate], [UpdateID], [UpdateDate]) VALUES (1, 2, 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[RoleMenus] ([Id], [RoleId], [MenuId], [Poster], [InitDate], [UpdateID], [UpdateDate]) VALUES (2, 2, 2, NULL, NULL, NULL, NULL)
INSERT [dbo].[RoleMenus] ([Id], [RoleId], [MenuId], [Poster], [InitDate], [UpdateID], [UpdateDate]) VALUES (3, 2, 3, NULL, NULL, NULL, NULL)
INSERT [dbo].[RoleMenus] ([Id], [RoleId], [MenuId], [Poster], [InitDate], [UpdateID], [UpdateDate]) VALUES (4, 2, 4, NULL, NULL, NULL, NULL)
INSERT [dbo].[RoleMenus] ([Id], [RoleId], [MenuId], [Poster], [InitDate], [UpdateID], [UpdateDate]) VALUES (5, 2, 5, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[RoleMenus] OFF
SET IDENTITY_INSERT [dbo].[Units] ON 
INSERT [dbo].[Units] ([Id], [Subject], [Alias], [ListNum], [Poster], [InitDate], [UpdateID], [UpdateDate]) VALUES (1, N'Portal', N'Portal', 0, N'admin', CAST(N'2019-05-24T16:34:26.233' AS DateTime), NULL, NULL)
SET IDENTITY_INSERT [dbo].[Units] OFF
SET IDENTITY_INSERT [dbo].[Members] ON 
INSERT [dbo].[Members] ([Id], [Account], [Password], [PasswordSalt], [Name], [IDNum], [Mobile], [Memo], [Gender], [Email], [MyPic], [JobTitle], [Address], [UnitId], [Permission], [Poster], [InitDate], [UpdateID], [UpdateDate]) VALUES (1, N'admin', N'/oIZh6cwrV5BIca+i8zTXNbS2aR89H+7az9pdWFMDvo=', N'NqYBB/o=', N'總管理者', N'A123456789', NULL, NULL, 0, N'admin@gmail.com', NULL, NULL, NULL, 1, N'A01,A01-1,A01-2,A01-3,A01-4', N'admin', CAST(N'2019-05-24T16:34:26.233' AS DateTime), NULL, NULL)
SET IDENTITY_INSERT [dbo].[Members] OFF
INSERT [dbo].[RoleMembers] ([Role_Id], [Member_Id]) VALUES (2, 1)

