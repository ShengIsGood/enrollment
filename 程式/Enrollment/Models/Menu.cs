﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace Enrollment.Models
{
    public class Menu:BackendBase
    {
        [Key]
        [Display(Name = "編號")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }


        [Required(ErrorMessage = "群組名稱必填")]
        [MaxLength(100)]
        [Display(Name = "功能名稱")]
        public string Subject { get; set; }

        [Required(ErrorMessage = "權限代號必填")]
        [MaxLength(10)]
        [Display(Name = "權限代號")]
        public string PrivilegeCode { get; set; }

        
        [Display(Name = "是否有子功能")]
        public BooleanType HaveDetails { get; set; }

        [Display(Name = "上層標題")]
        public int? TopMenuId { get; set; }

        [JsonIgnore]
        [ForeignKey("TopMenuId")]
        public virtual Menu Menus { get; set; }


        [Display(Name = "是否為外掛功能")]
        public BooleanType IsPlugIn { get; set; }

        [MaxLength(100)]
        [Display(Name = "控制器名稱")]
        public string ControllerName { get; set; }

        [MaxLength(100)]
        [Display(Name = "功能(Action)")]
        public string ActionName { get; set; }

        [MaxLength(200)]
        [Display(Name = "網址")]
        public string Url { get; set; }

        [MaxLength(200)]
        [Display(Name = "外掛登入網址")]
        public string LoginUrl { get; set; }

        [Display(Name = "是否啟用")]
        public BooleanType IsWork { get; set; }


        [Display(Name = "排序")]
        public int SortNum { get; set; }


        public virtual ICollection<Menu> MenuDetails{ get; set; }
        public virtual ICollection<RoleMenu> RoleMenus { get; set; }
    }
}