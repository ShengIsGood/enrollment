﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace Enrollment.Models
{
    public class Device : BackendBase
    {
        [Key]
        [Display(Name = "編號")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required(ErrorMessage = "設備編號必填")]
        [MaxLength(20)]
        [Display(Name = "設備編號")]
        public string DeviceId { get; set; }

        //ForeignKey
        [Display(Name = "類別")]
        public int? DeviceCategoryId { get; set; }

        [JsonIgnore]
        [ForeignKey("DeviceCategoryId")]
        public virtual DeviceCategory DeviceCategory { get; set; }


        [Required(ErrorMessage = "設備名稱必填")]
        [MaxLength(50)]
        [Display(Name = "設備名稱")]
        public string Subject { get; set; }


        [MaxLength(100)]
        [Display(Name = "設備型號")]
        public string DeviceModel { get; set; }

        [MaxLength(100)]
        [Display(Name = "設備品牌")]
        public string DeviceBrand { get; set; }

        [Display(Name = "價格")]
        public int Price { get; set; }

        [Display(Name = "設備狀態")]
        public DeviceStatus DeviceStatus { get; set; }

        [Display(Name = "租用狀態")]
        public DeviceRentStatus DeviceRentStatus { get; set; }

        
        [Display(Name = "設備介紹")]
        public string DeviceInfo { get; set; }


        [Display(Name = "保固期限")]
        public DateTime? WarrantyDate { get; set; }

        [Display(Name = "購買日期")]
        public DateTime BuyDate { get; set; }

        [Display(Name = "退役日期")]
        public DateTime? DecommissioningDate { get; set; }

        [MaxLength(200)]
        [Display(Name = "備註")]
        public string Memo { get; set; }


        public virtual ICollection<RentDeviceHistory> RentDeviceHistories { get; set; }
    }
}