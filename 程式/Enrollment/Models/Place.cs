﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace Enrollment.Models
{
    public class Place : BackendBase
    {
        [Key]
        [Display(Name = "編號")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [JsonProperty]
        public int Id { get; set; }

        [Required(ErrorMessage = "{0}必填")]
        [MaxLength(50)]
        [Display(Name = "編號")]
        [JsonProperty]
        public string PlaceNumber { get; set; }

        [Display(Name = "類別")]
        [JsonProperty]
        public Category category { get; set; }

        [Required(ErrorMessage = "{0}必填")]
        [MaxLength(50)]
        [Display(Name = "名稱")]
        [JsonProperty]
        public string PlaceName { get; set; }

        [MaxLength(50)]
        [Display(Name = "位置")]
        [JsonProperty]
        public string Location { get; set; }

        [Display(Name = "可容納人數")]
        public int Numberlimit { get; set; }

        [Display(Name = "啟用")]
        [JsonProperty]
        public EnableType Enable { get; set; }

        public virtual ICollection<ClassTeacher> ClassTeacher { get; set; }
    }
}