﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Enrollment.Models
{
    public class ComGroup : BackendBase
    {
        [Key]
        [Required(ErrorMessage = "{0}必填")]
        [Column (TypeName = "VARCHAR")]
        [Display(Name = "群組代號")]
        [StringLength(50)]
        public string GroupID { get; set; }


        [StringLength(100)]
        [Display(Name = "群組說明")]
        public string GroupName { get; set; }

        [Column(TypeName = "VARCHAR")]
        [StringLength(2)]
        public string GroupType { get; set; }

        [Column(TypeName = "VARCHAR")]
        [StringLength(2)]
        public string SysType { get; set; }

   


        public virtual ICollection<ComCode> ComCode { get; set; }
    }
}