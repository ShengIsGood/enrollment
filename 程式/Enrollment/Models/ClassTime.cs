﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace Enrollment.Models
{
    public class ClassTime : BackendBase
    {
        [Key]
        [Display(Name = "編號")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [JsonProperty]
        public int Id { get; set; }

        [Display(Name = "課表")]
        public int ClassTeacherId { get; set; }

        [ForeignKey("ClassTeacherId")]
        [JsonIgnore]
        public virtual ClassTeacher ClassTeacher { get; set; }

      
        [Display(Name = "日期")]
        public DateTime? Date { get; set; }

        [MaxLength(1)]
        [Display(Name = "星期幾")]
        public string Day { get; set; }

        [MaxLength(5)]
        [Display(Name = "上課時間")]
        public string StarTime { get; set; }

        [MaxLength(5)]
        [Display(Name = "下課時間")]
        public string EndTime { get; set; }


    
    }
}