﻿using System;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Net.Mail;
using System.Web.DataAccess;
//using Enrollment.Migrations;


namespace Enrollment.Models
{
    public class BackendContext : DbContext
    {
        public BackendContext()
            : base("name=EnrollmentConnection")
        {
        }
        //權限
        public DbSet<Unit> Units { get; set; } //單位
        public DbSet<Member> Members { get; set; }//管理者
        public DbSet<Role> Roles { get; set; } //角色
        public DbSet<ComGroup> ComGroups { get; set; } //下拉式選單(群組)
        public DbSet<ComCode> ComCodes { get; set; }//下拉式選單(item)

        public DbSet<Menu> Menu { get; set; }//功能選單
        public DbSet<RoleMenu> RoleMenu { get; set; }//選單角色
        public DbSet<course> course { get; set; }//課程管理
        public DbSet<Field> Field { get; set; }//領域
        public DbSet<Teacher> Teacher { get; set; }//教師
        public DbSet<CourseAssociation> CourseAssociation { get; set; }
        public DbSet<TeacherAssociation> TeacherAssociation { get; set; }


        public DbSet<Student> Student { get; set; }//選單角色

        public DbSet<Place> Place { get; set; }//場地
        public DbSet<ClassTeacher> ClassTeachers { get; set; }//課表
        public DbSet<ClassTime> ClassTimes { get; set; }//上課時間

        public DbSet<ClassStudents> ClassStudents { get; set; }//課程報名
        public DbSet<ClassHistory> ClassHistories { get; set; }//課程授課記錄
        public DbSet<Attendance>  Attendances{ get; set; }//出勤紀錄

        public DbSet<Score> Scores { get; set; }//評分

        public DbSet<License> Licenses { get; set; }//證照
        public DbSet<Device> Devices { get; set; }//設備
        public DbSet<DeviceCategory> DeviceCategories{ get; set; }//設備類別
        public DbSet<RentDeviceHistory> RentDeviceHistories { get; set; }//租用記錄
                                                                         
        //protected override void OnModelCreating(DbModelBuilder modelBuilder)
        //{
        //    modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        //}


        //protected override void OnModelCreating(DbModelBuilder modelBuilder)
        //{
        //    base.OnModelCreating(modelBuilder);
        //    modelBuilder.Entity<Test>().Property(o => o.LineNo)
        //        .HasPrecision(6, 0)
        //        .HasColumnType("numeric");
        //    modelBuilder.Entity<Test>().Property(o => o.TotalSKU)
        //        .HasPrecision(8, 0)
        //        .HasColumnType("numeric");
        //    modelBuilder.Entity<Test>().Property(o => o.Qty)
        //        .HasPrecision(8, 0)
        //        .HasColumnType("numeric");
        //}

    }
}
