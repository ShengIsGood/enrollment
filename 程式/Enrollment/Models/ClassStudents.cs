﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace Enrollment.Models
{
    public class ClassStudents: BackendBase
    {
        [Key]
        [Display(Name = "編號")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [JsonProperty]
        public int Id { get; set; }


        [Display(Name = "課程")]
        public int ClassTeacherId { get; set; }

        [ForeignKey("ClassTeacherId")]
        [JsonIgnore]
        public virtual ClassTeacher ClassTeacher { get; set; }

        [Display(Name = "學員")]
        public int StudentId { get; set; }

        [ForeignKey("StudentId")]
        [JsonIgnore]
        public virtual Student Student { get; set; }


        [Display(Name = "報名狀態")]
        public ApplyStatus ClassStatus { get; set; }


        [Display(Name = "繳費狀態")]
        public PaymentStatus PaymentStatus { get; set; }

        [Display(Name = "是否需要宿舍")]
        public EnableType NeedRoom { get; set; }
    }
}