﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;

namespace Enrollment.Models
{
    public class course : BackendBase
    {
        [Key]
        [Display(Name = "編號")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [JsonProperty]
        public int Id { get; set; }

        [MaxLength(20)]
        [Display(Name = "課程編號")]
        public string CourseNumber { get; set; }

        [MaxLength(20)]
        [Display(Name = "課程名稱")]
        public string CourseName { get; set; }

        [MaxLength(200)]
        [Display(Name = "課程綱要")]
        public string CourseSummary { get; set; }

        [MaxLength(20)]
        [Display(Name = "課程訓練項目")]
        public string TrainingProgram { get; set; }

        [Display(Name = "訓練內容")]
        public string TrainingContent { get; set; }

        [MaxLength(50)]
        [Display(Name = "上傳教材")]
        public string UploadMaterials { get; set; }

        [MaxLength(100)]
        [Display(Name = "備註")]
        public string Memo { get; set; }

        public virtual ICollection<CourseAssociation> CourseAssociation { get; set; }
        public virtual ICollection<ClassTeacher> ClassTeacher { get; set; }
    }







}
