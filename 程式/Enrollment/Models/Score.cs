﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace Enrollment.Models
{
    public class Score :BackendBase
    {
        [Key]
        [Display(Name = "編號")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [JsonProperty]
        public int Id { get; set; }


        [Display(Name = "上層編號")]
        public int? ScoreId { get; set; }

        [JsonIgnore]
        [ForeignKey("ScoreId")]
        public virtual Score ParentScoreId { get; set; }

        [Display(Name = "評分日期")]
        public int? ClassHistoryId { get; set; }

        [ForeignKey("ClassHistoryId")]
        [JsonIgnore]
        public virtual ClassHistory ClassHistory { get; set; }

        [Display(Name = "學員")]
        public int? StudentId { get; set; }

        [ForeignKey("StudentId")]
        [JsonIgnore]
        public virtual Student Student { get; set; }



        [MaxLength(50)]
        [Display(Name = "評分項目")]
        public string ScoreSubject { get; set; }

        
        [Display(Name = "分數")]
        public double? Fraction{ get; set; }





        public virtual ICollection<Score> ChildScores { get; set; }
    }
}