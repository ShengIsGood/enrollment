﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace Enrollment.Models
{
    public class License : BackendBase
    {
        [Key]
        [Display(Name = "編號")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [JsonProperty]
        public int Id { get; set; }

        [MaxLength(50)]
        [Display(Name = "證照編號")]
        public string LicenseId { get; set; }

        [MaxLength(50)]
        [Display(Name = "類別")]
        public string LicenseCategory { get; set; }

        [MaxLength(50)]
        [Display(Name = "發證單位")]
        public string LicenseUnit { get; set; }

        [MaxLength(50)]
        [Display(Name = "證照中文名稱")]
        public string ChineseName { get; set; }

        [MaxLength(50)]
        [Display(Name = "證照英文名稱")]
        public string EnglishName { get; set; }

        [Display(Name = "發照日期")]
        public DateTime? IssueDate { get; set; }

        [Display(Name = "有效日期")]
        public DateTime? EffectiveDate { get; set; }

        [Display(Name = "老師")]
        public int? TeacherFK { get; set; }

        [ForeignKey("TeacherFK ")]
        [JsonIgnore]
        public virtual Teacher Teacher { get; set; }


        [Display(Name = "學生")]
        public int? StudentFK { get; set; }

        [ForeignKey("StudentFK")]
        [JsonIgnore]
        public virtual Student Student { get; set; }


    }
}