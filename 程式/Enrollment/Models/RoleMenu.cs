﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace Enrollment.Models
{
    public class RoleMenu : BackendBase
    {

        [Key]
        [Display(Name = "編號")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Display(Name = "角色編號")]
        public int RoleId { get; set; }

        [ForeignKey("RoleId")]
        [JsonIgnore]
        public virtual Role Role { get; set; }


        [Display(Name = "選單編號")]
        public int MenuId { get; set; }

        [ForeignKey("MenuId")]
        [JsonIgnore]
        public virtual Menu Menu { get; set; }

    }
}