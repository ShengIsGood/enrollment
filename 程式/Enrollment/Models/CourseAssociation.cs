﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace Enrollment.Models
{
    public class CourseAssociation:BackendBase
    {
        [Key]
        [Display(Name = "編號")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [JsonProperty]
        public int Id { get; set; }
   

        [Display(Name = "課程")]
        public int CourseFK { get; set; }

        [ForeignKey("CourseFK")] [JsonIgnore] public virtual course Course { get; set; }

 

        [Display(Name = "領域")]
        public int FieldFK { get; set; }

        [ForeignKey("FieldFK")] [JsonIgnore] public virtual Field Field { get; set; }
    }
}