﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace Enrollment.Models
{
    public class ClassHistory : BackendBase
    {
        [Key]
        [Display(Name = "編號")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [JsonProperty]
        public int Id { get; set; }


        [Display(Name = "課程")]
        public int ClassTeacherId { get; set; }

        [ForeignKey("ClassTeacherId")]
        [JsonIgnore]
        public virtual ClassTeacher ClassTeacher { get; set; }

        [Display(Name = "上課日期")]
        public DateTime ClassDate { get; set; }

        [MaxLength(1)]
        [Display(Name = "星期幾")]
        public string Day { get; set; }

        [MaxLength(5)]
        [Display(Name = "上課時間")]
        public string StarTime { get; set; }

        [MaxLength(5)]
        [Display(Name = "下課時間")]
        public string EndTime { get; set; }

        [StringLength(1000)]
        [Display(Name = "今日課程內容")]
        public string CourseContent{ get; set; }

        [StringLength(5)]
        [Display(Name = "實際上課時間")]
        public string ClassStartTimes { get; set; }

        [StringLength(5)]
        [Display(Name = "實際下課時間")]
        public string ClassEndTimes { get; set; }

        [StringLength(1000)]
        [Display(Name = "備註")]
        public string Memo { get; set; }


        public virtual ICollection<Score> Scores { get; set; }
        public virtual ICollection<RentDeviceHistory>RentDeviceHistories { get; set; }
    }
}