﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace Enrollment.Models
{
    public enum GenderType
    {
        男,
        女,
        不指定
    }

    public enum TargetType
    {
        另開視窗,
        本地開啟
    }
    public enum EnableType
    {
        否,
        是
    }


    public enum BooleanType
    {
        是,
        否
    }

    public enum ProcessState
    {
        搶修中,
        已排除
    }

    public enum PumpingState
    {
        關閉,
        開啟,
        故障
    }

    public enum EmployeeStatus
    {
        在職,
        離職,
        留職停薪
    }

    public enum InsertLevel
    {
        A,
        B,
        C,
        D,
        E,
        F,
        G
    }

    public enum CheckStatus
    {
        待審,
        通過,
        駁回
    }

    public enum DispatchCategory
    {
        取樣派工,
        測試派工
    }
    public enum WorkCategory
    {
        單工,
        多工
    }
    public enum DeliveryMethod
    {
        手動排工,
        智慧排工
    }

    public enum IsWorking
    {
        未工作,
        正在工作
    }


    public enum RoomType
    {
        宿舍,
        教室,
        訓練場地
    }



    public enum Category
    {
        教室,
        訓練場地,
        宿舍
    }

    public enum ClassCycle
    {
        每週,
        平日每天,
        特定日期
    }
    public enum ClassStatus
    {
       報名中,
      確定開課,
       已額滿,
       人數不足不開課,
       取消課程,
       已達最低開課人數
    }

    public enum ApplyStatus
    {
        錄取,
        取消報名,
        備取,
        課程已取消,
        人數不足取消課程,
        報名已截止
    }

    public enum PaymentStatus
    {
        未繳費,
        已繳費,
        退費中,
        已退費
    }
    public enum AttendStatus
    {
        出席,
        事假,
        病假,
        公假,
        生理假,
        育嬰假,
        曠課,
        
    }

    public enum DeviceStatus
    {
        正常,
        維護,
        保養,
        送修,
        故障,
        報廢
    }
    public enum DeviceRentStatus
    {
        未租用,
        已租用
    }

    public enum DeviceUse
    {
       課程需求,
       個人需求,
       例行維護,
       送修保養,
       其他

    }
    
}