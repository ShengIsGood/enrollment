﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace Enrollment.Models
{
    public class DeviceCategory : BackendBase
    {
        [Key]
        [Display(Name = "編號")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }


        //ForeignKey
        [Display(Name = "上層編號")]
        public int? ParentId { get; set; }

        [JsonIgnore]
        [ForeignKey("ParentId")]
        public virtual DeviceCategory ParentCategory { get; set; }



        [Required(ErrorMessage = "類別名稱必填")]
        [MaxLength(200)]
        [Display(Name = "類別名稱")]
        public string Subject { get; set; }

    

        [Required(ErrorMessage = "排序必填")]
        [Display(Name = "排序")]
        public int SortNum { get; set; }



        [Display(Name = "類別路徑")]
        [NotMapped]
        public string CategoryPath
        {
            get
            {
                string categoryPath = "";
                categoryPath = this.Subject;
                DeviceCategory parentClass = this.ParentCategory;
                while (parentClass != null)
                {
                    categoryPath = parentClass.Subject + "->" + categoryPath;
                    parentClass = parentClass.ParentCategory;
                }

                return categoryPath;
            }

        }

        [Display(Name = "層級")]
        [NotMapped]
        public int Level
        {
            get
            {
                int level = 1;
                DeviceCategory parentClass = this.ParentCategory;
                while (parentClass != null)
                {
                    level++;
                    parentClass = parentClass.ParentCategory;
                }

                return level;
            }

        }


        public virtual ICollection<DeviceCategory> ChildCategory { get; set; }

        public virtual ICollection<Device> Devices { get; set; }

    }
}