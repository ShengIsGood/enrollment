﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace Enrollment.Models
{
    public class Attendance : BackendBase
    {
        [Key]
        [Display(Name = "編號")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [JsonProperty]
        public int Id { get; set; }


        [Display(Name = "課程")]
        public int ClassTeacherId { get; set; }

        [ForeignKey("ClassTeacherId")]
        [JsonIgnore]
        public virtual ClassTeacher ClassTeacher { get; set; }

        [Display(Name = "學員")]
        public int StudentId { get; set; }

        [ForeignKey("StudentId")]
        [JsonIgnore]
        public virtual Student Student { get; set; }

        [Display(Name = "上課日期")]
        public DateTime ClassDate { get; set; }

        [MaxLength(1)]
        [Display(Name = "星期幾")]
        public string Day { get; set; }

        [MaxLength(5)]
        [Display(Name = "上課時間")]
        public string StarTime { get; set; }

        [MaxLength(5)]
        [Display(Name = "下課時間")]
        public string EndTime { get; set; }


        [Display(Name = "出席狀態")]
        public AttendStatus? AttendStatus { get; set; }

        [Display(Name = "是否遲到")]
        public EnableType IsLate { get; set; }

        [StringLength(5)]
        [Display(Name = "到課時間")]
        public string RealTime { get; set; }

        [Display(Name = "是否早退")]
        public EnableType IsExcused { get; set; }

        [StringLength(5)]
        [Display(Name = "離開時間")]
        public string LeaveTime { get; set; }

        [StringLength(5)]
        [Display(Name = "請假開始時間")]
        public string AskForLeaveStartTime { get; set; }

        [StringLength(5)]
        [Display(Name = "請假結束時間")]
        public string AskForLeaveEndTime { get; set; }

        [StringLength(200)]
        [Display(Name = "備註")]
        public string Memo { get; set; }
    }
}