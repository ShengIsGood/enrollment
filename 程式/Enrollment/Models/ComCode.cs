﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace Enrollment.Models
{
    public class ComCode :BackendBase
    {


        [Key]
        [Display(Name = "編號")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [JsonProperty]
        public int Id { get; set; }


        [Column(TypeName = "VARCHAR")]
        [Required(ErrorMessage = "{0}必填")]
        [Display(Name = "參數代號")]
        [StringLength(50)]
        public string CodeID { get; set; }

        [Column(TypeName = "VARCHAR")]
        [Required(ErrorMessage = "{0}必填")]
        [StringLength(50)]
        public string GroupID { get; set; }

        [ForeignKey("GroupID")]
        [JsonIgnore]
        public virtual ComGroup ComGroup { get; set; }



        [Column(TypeName = "VARCHAR")]
        [Display(Name = "說明1")]
        [StringLength(100)]
        public string CodeName1 { get; set; }

        [Column(TypeName = "VARCHAR")]
        [Display(Name = "說明2")]
        [StringLength(100)]
        public string CodeName2 { get; set; }

        [Column(TypeName = "VARCHAR")]
        [Display(Name = "說明3")]
        [StringLength(100)]
        public string CodeName3 { get; set; }

        [Display(Name = "排序")]
        public int SortIndex { get; set; }



        [Display(Name = "啟用")]
        public EnableType Enabled { get; set; }


        [Display(Name = "備註")]
        [Column(TypeName = "Text")]
        public string Memo { get; set; }


     
 
    }
}