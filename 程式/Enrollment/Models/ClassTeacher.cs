﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace Enrollment.Models
{
    public class ClassTeacher : BackendBase
    {
        [Key]
        [Display(Name = "編號")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [JsonProperty]
        public int Id { get; set; }


        [Display(Name = "課程")]
        public int CourseId { get; set; }

        [ForeignKey("CourseId")]
        [JsonIgnore]
        public virtual course Course { get; set; }


        [Display(Name = "講師")]
        public int TeacherId { get; set; }

        [ForeignKey("TeacherId")]
        [JsonIgnore]
        public virtual Teacher Teacher { get; set; }





        [Display(Name = "場地")]
        public int PlaceId { get; set; }

        [ForeignKey("PlaceId")]
        [JsonIgnore]
        public virtual Place Place { get; set; }


        [Display(Name = "最低開課人數")]
        public int MinStudents { get; set; }

        [Display(Name = "最高開課人數")]
        public int MaxStudents { get; set; }

        [Display(Name = "提供宿舍")]
        public EnableType ProvideDormitory { get; set; }

        [StringLength(100)]
        [Display(Name = "宿舍")]
        public string DormRoom { get; set; }


        [Display(Name = "上課週期")]
        public ClassCycle ClassCycle { get; set; }

     

        [Display(Name = "開課日期")]
        public DateTime StarDateTime { get; set; }

        [Display(Name = "結束日期")]
        public DateTime EndDateTime { get; set; }


        [Display(Name = "公告開始日期")]
        public DateTime AnnouncementStarDateTime { get; set; }

        [Display(Name = "公告結束日期")]
        public DateTime AnnouncementEndDateTime { get; set; }


        [Display(Name = "課程狀態")]
        public ClassStatus ClassStatus { get; set; }



        public virtual ICollection<ClassTime> ClassTimes { get; set; }
        public virtual ICollection<ClassStudents> ClassStudentses { get; set; }
        public virtual ICollection<ClassHistory> ClassHistories { get; set; }
        public virtual ICollection<Attendance> Attendance { get; set; } 
    
    }
}