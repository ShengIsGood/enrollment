﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace Enrollment.Models
{
    public class Teacher:BackendBase
    {
        [Key]
        [Display(Name = "編號")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [JsonProperty]
        public int Id { get; set; }

        [Required(ErrorMessage = "{0}必填")]
        [MaxLength(50)]
        [Display(Name = "姓名")]
        public string Name { get; set; }

        [MaxLength(50)]
        [Display(Name = "照片")]
        public string Photo { get; set; }

        [MaxLength(50)]
        [Display(Name = "最高學歷")]
        public string Education { get; set; }

        [Required(ErrorMessage = "{0}必填")]
        [EmailAddress(ErrorMessage = "{0} 格式錯誤")]
        [MaxLength(200)]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "E-Mail")]
        public string Email { get; set; }

        [MaxLength(200)]
        [Display(Name = "經歷")]
        public string Experience { get; set; }

        [MaxLength(50)]
        [Display(Name = "履歷")]
        public string Resume { get; set; }

        public virtual ICollection<TeacherAssociation> TeacherAssociation { get; set; }
        public virtual ICollection<ClassTeacher> ClassTeacher { get; set; }
        public virtual ICollection<License> Licenses { get; set; }
    }
}