﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Enrollment.Models
{
    public class ClassDateList
    {
        public int Id { get; set; }
        public string Date { get; set; }
    }
}