﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace Enrollment.Models
{
    public class RentDeviceHistory:BackendBase
    {
        [Key]
        [Display(Name = "編號")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }


        //ForeignKey
        [Display(Name = "設備")]
        public int? DeviceId { get; set; }

        [JsonIgnore]
        [ForeignKey("DeviceId")]
        public virtual Device Device { get; set; }

        //ForeignKey
        [Display(Name = "課程")]
        public int? ClassHistoryId { get; set; }

        [JsonIgnore]
        [ForeignKey("ClassHistoryId")]
        public virtual ClassHistory ClassHistory { get; set; }

        [MaxLength(10)]
        [Display(Name = "借用人")]
        public string RentName { get; set; }

        [MaxLength(20)]
        [Display(Name = "借用人電話")]
        public string RentMobile { get; set; }

        [Display(Name = "設備用途")]
        public DeviceUse DeviceUse { get; set; }

         [MaxLength(200)]
        [Display(Name = "設備用途說明")]
        public string DeviceUseContent { get; set; }

        [MaxLength(10)]
        [Display(Name = "借出人")]
        public string Lender { get; set; }

        [Display(Name = "是否歸還")]
        public EnableType IsReturn{ get; set; }

        [Display(Name = "歸還日期")]
        public DateTime? ReturnDate { get; set; }
    }
}