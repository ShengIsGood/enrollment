﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace Enrollment.Models
{
    public class Field:BackendBase
    {
        [Key]
        [Display(Name = "編號")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [JsonProperty]
        public int Id { get; set; }

        [MaxLength(50)]
        [Display(Name = "領域名稱")]
        public string FieldName { get; set; }

        public virtual ICollection<TeacherAssociation> TeacherAssociation { get; set; }
        public virtual ICollection<CourseAssociation> CourseAssociation { get; set; }
    }
}